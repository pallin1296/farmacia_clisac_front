import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, Output, OnInit, ViewChild,EventEmitter,Input } from '@angular/core';
import { ConectorApiService } from '../services/conector-api.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public iniciarSesion: FormGroup
  submitted=false;
  detalleCorte={}
  frmNuevoCorte: FormGroup;
  frmActualizarCorte: FormGroup;

  @ViewChild('NuevoCorte') ModalNuevoCorte
  @ViewChild('ActualizarCorte') ModalActualizarCorte

  constructor(
    private modalService: NgbModal,
    private conector: ConectorApiService,
    private route: ActivatedRoute,
    private router: Router,
    private _builder: FormBuilder  
  ) { 
    this.iniciarSesion = this._builder.group({
      usuario: ['',[Validators.required]],
      password: ['',[Validators.required]]
    }); 

    this.frmNuevoCorte = this._builder.group({
      cantidadNC: ['',[Validators.required]]
    }) ;

    this.frmActualizarCorte = this._builder.group({
      cantidadAC: ['',[Validators.required]],
      comentario: ['']
    }); 

  }

  get fDataNC(){
    return this.frmNuevoCorte.controls  
  }

  get fDataAC(){
    return this.frmActualizarCorte.controls  
  }

  ngOnInit(): void {
    localStorage.removeItem("token_AY5634ED");
    localStorage.removeItem("permisos");

  }

  get fData(){
    return this.iniciarSesion.controls  
  }
  validarUsuario(){

    this.submitted = true

    if(
      this.fData.usuario.value==null ||
      this.fData.password.value==null
      ){
      console.info("Error con los datos");
      return false
    }else{
      return true
    }

  }

  iniciar(){

    if(this.validarUsuario()){
      let data={
        "username":this.fData.usuario.value,
        "password":this.fData.password.value
        
      }

      this.conector.iniciarSesion(data).subscribe(
        (e: any)=>{
          
          if(e.ok){
            if(JSON.parse(e.permisos).ventagenerarventas==1){

              this.conector.obtenerUltimoCorte().subscribe(

                (resultimocorte: any)=>{
                
                  if(resultimocorte.ok){
                    
                    if(resultimocorte.data.corteexistente){
                      if(resultimocorte.data.usuario==data.username){
  
                        localStorage.setItem('info_user',"{"+'"username":"'+e.username+'","role":"'+e.role+'"'+"}");
                        localStorage.setItem('token_AY5634ED',e.access_token);
                        localStorage.setItem('permisos',e.permisos);
            
                        this.router.navigate(['dashboard']);
  
                      }else{
                        this.detalleCorte={
                          usuario:data.username,
                          idcortecaja:resultimocorte.data.idcortecaja,
                          corte:resultimocorte.data.corte,
                          infouser:"{"+'"username":"'+e.username+'","role":"'+e.role+'"'+"}",
                          permisos:e.permisos,
                          token:e.access_token
                        }
                        this.abrirModalActualizarCorte()
                      }
                    }else{
                      this.detalleCorte={
                        usuario:data.username,
                        infouser:"{"+'"username":"'+e.username+'","role":"'+e.role+'"'+"}",
                        permisos:e.permisos,
                        token:e.access_token
                      }                    
                      this.abrirModalNuevoCorte()
  
                    }
  
                  }else{
  
                    this.conector.mensajeError(e.msg)
  
                  }
  
  
                }
  
              );

            }else{

              localStorage.setItem('info_user',"{"+'"username":"'+e.username+'","role":"'+e.role+'"'+"}");
              localStorage.setItem('token_AY5634ED',e.access_token);
              localStorage.setItem('permisos',e.permisos);
  
              this.router.navigate(['dashboard']);

            }
    
          }else{
            if(e.msg=="Bad Request. Invalid credentials"){
              this.conector.mensajeError("Errores posibles: "
              +"<br>- Usuario incorrecto."
              +"<br>- Contraseña incorrecta."
              +"<br>- El usuario no tiene permiso de acceder.");

            }else{
              this.conector.mensajeError(e.msg);

            }

          }
    
        }
      );

    }
    
  }

  abrirModalNuevoCorte(){
    this.modalService.open(this.ModalNuevoCorte, { windowClass: 'animated fadeInDown',size:'lg' });

  }

  abrirModalActualizarCorte(){
    this.modalService.open(this.ModalActualizarCorte, { windowClass: 'animated fadeInDown',size:'lg' });
  }

  cerrarModal(){
    this.modalService.dismissAll();
  }

  crearCorte(){

    let data={
      importeinicial:this.fDataNC.cantidadNC.value,
      username:this.detalleCorte['usuario']
    }

    this.conector.crearNuevoCorte(data).subscribe(
      (resNC:any)=>{
        if(resNC.ok){
          this.cerrarModal();

          localStorage.setItem('info_user',this.detalleCorte['infouser']);
          localStorage.setItem('token_AY5634ED',this.detalleCorte['token']);
          localStorage.setItem('permisos',this.detalleCorte['permisos']);

          this.router.navigate(['dashboard']);

        }else{
          this.conector.mensajeError(resNC.msg);
        }
      }
    );

  }

  actualizarCorte(){

    let data={
      comentario:this.fDataAC.comentario.value,
      importeinicial:this.fDataAC.cantidadAC.value,
      username:this.detalleCorte['usuario'],
      idcortecaja:this.detalleCorte['idcortecaja']
    }

    this.conector.actualizarYcrearNuevoCorte(data).subscribe(
      (resAC:any)=>{
        if(resAC.ok){
          this.conector.corte58mm(resAC.data);
          this.cerrarModal();
          
          localStorage.setItem('info_user',this.detalleCorte['infouser']);
          localStorage.setItem('token_AY5634ED',this.detalleCorte['token']);
          localStorage.setItem('permisos',this.detalleCorte['permisos']);

          this.router.navigate(['dashboard']);

        }else{
          this.conector.mensajeError(resAC.msg);
        }
      }
    );

  }

}
