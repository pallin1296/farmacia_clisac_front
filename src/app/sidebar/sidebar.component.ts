import { Component,OnInit } from '@angular/core';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/dashboard', title: 'Dashboard',  icon: 'pe-7s-graph', class: '' },
    { path: '/user', title: 'User Profile',  icon:'pe-7s-user', class: '' },
    { path: '/table', title: 'Table List',  icon:'pe-7s-note2', class: '' },
    { path: '/typography', title: 'Typography',  icon:'pe-7s-news-paper', class: '' },
    { path: '/icons', title: 'Icons',  icon:'pe-7s-science', class: '' },
    { path: '/maps', title: 'Maps',  icon:'pe-7s-map-marker', class: '' },
    { path: '/notifications', title: 'Notifications',  icon:'pe-7s-bell', class: '' },
    { path: '/upgrade', title: 'Upgrade to PRO',  icon:'pe-7s-rocket', class: 'active-pro' },
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {
  menuItems: any[];
  menu:any=[];
  permisos=JSON.parse(localStorage.getItem('permisos'));
  usuario=JSON.parse(localStorage.getItem('info_user'));
  constructor() { }

  ngOnInit() {
   
    if(this.permisos.ventapacienteventa==1 || this.permisos.ventapacienteventarealizada==1 || this.permisos.ventagenerarventas==1 
      || this.permisos.ventaventasgeneradas==1 || this.permisos.ventacortecaja==1){
        let menu={ path: '', title: 'VENTAS',  icon: 'pe-7s-cart', class: '',componenttitle:"venta"};
        let submenu=[];
        
        
        if(this.permisos.ventagenerarventas==1){
    
          submenu.push({ path: '/ventas', title: 'Generar Venta',  icon: '', class: ''});
            
        }

        if(this.permisos.ventaventasgeneradas==1){
    
          submenu.push({ path: '/ventas-generadas', title: 'Ventas Generadas',  icon: '', class: ''});
            
        }

        if(this.permisos.ventacortecaja==1){
    
          submenu.push({ path: '/corteCaja', title: 'Corte de Caja',  icon: '', class: ''});
            
        }

        if(this.permisos.ventacortecajagenerados==1){
    
          submenu.push({ path: '/cortes-de-caja-generadas', title: 'Cortes de Caja Generadas',  icon: '', class: ''});
            
        }

        if(this.permisos.ventapacienteventa==1){
    
          submenu.push({ path: '/venta-almacen-secundario', title: 'Paciente Venta',  icon: '', class: ''});
            
        }

        if(this.permisos.ventapacienteventarealizada==1){
    
          submenu.push({ path: '/ventas-realizadas-almacen-secundario', title: 'Paciente Ventas Realizadas',  icon: '', class: ''});
            
        }

        this.menu.push({
          menu:menu,
          submenu:submenu
        });
    }

    //modulo producto
    if(this.permisos.agregarproducto==1 || this.permisos.editarproducto==1 || this.permisos.estatusproducto==1 || 
      this.permisos.agregarcategoria==1 || this.permisos.editarcategoria==1 || this.permisos.estatuscategoria==1){
        let menu={ path: '', title: 'PRODUCTO',  icon: 'pe-7s-ticket', class: '',componenttitle:"producto"};
        let submenu=[];

        if(this.permisos.agregarcategoria==1 || this.permisos.editarcategoria==1 || this.permisos.estatuscategoria==1){
    
          submenu.push({ path: '/categoria', title: 'Categoria',  icon: '', class: ''});
            
        }

        if(this.permisos.agregarproducto==1 || this.permisos.editarproducto==1 || this.permisos.estatus.producto==1){
    
          submenu.push({ path: '/producto', title: 'Producto',  icon: '', class: ''});
            
        }

        this.menu.push({
          menu:menu,
          submenu:submenu
        });
    }

    // modulo usuarios
    if(this.permisos.agregarpaciente==1 || this.permisos.paciente==1 || 
      this.permisos.agregarempleado==1 || this.permisos.editarempleado==1 || this.permisos.estatusempleado==1 ||
      this.permisos.agregardoctor==1 || this.permisos.editardoctor==1 || this.permisos.estatusdoctor==1){
        let menu={ path: '', title: 'USUARIO',  icon: 'pe-7s-users', class: '',componenttitle:"usuario"};
        let submenu=[];
        if(this.permisos.agregarpaciente==1 || this.permisos.paciente==1){
    
          submenu.push({ path: '/empleado', title: 'Empleado',  icon: '', class: ''});
            
        }

        if(this.permisos.agregarempleado==1 || this.permisos.editarempleado==1 || this.permisos.estatusempleado==1){
    
          submenu.push({ path: '/paciente', title: 'Paciente',  icon: '', class: ''});
            
        }

        if(this.permisos.agregardoctor==1 || this.permisos.editardoctor==1 || this.permisos.estatusdoctor==1){
    
          submenu.push({ path: '/doctor', title: 'Doctor',  icon: '', class: ''});
            
        }


        this.menu.push({
          menu:menu,
          submenu:submenu
        });
    }

   //modulo almacen principal 
   if(this.permisos.crearalmacengenerarlista==1 || 
    this.permisos.estatuslistagenerada==1 || 
    this.permisos.agregaralmacenprincipal==1 || this.permisos.ajustaralmacenprincipal==1 || this.permisos.editaralmacenprincipal==1){
      let menu={ path: '', title: 'ALMACEN PRINCIPAL',  icon: 'pe-7s-box2', class: '',componenttitle:"almacenP"};
      let submenu=[];

      if(this.permisos.agregaralmacenprincipal==1 || this.permisos.ajustaralmacenprincipal==1 || this.permisos.editaralmacenprincipal==1){
        
        submenu.push({ path: '/almacenPrincipal', title: 'Productos de Almacen Principal',  icon: '', class: ''});
          
      }

      if(this.permisos.crearalmacengenerarlista==1){
  
        submenu.push({ path: '/generarListaAlmacen', title: 'Generar lista',  icon: '', class: ''});
          
      }

      if(this.permisos.estatuslistagenerada==1){
  
        submenu.push({ path: '/listasGeneradas', title: 'Listas Generadas',  icon: '', class: ''});
          
      }

      this.menu.push({
        menu:menu,
        submenu:submenu
      });
  }

     //modulo almacen secundario
     if(this.permisos.generargenerarlista==1 || 
      this.permisos.listageneradasalida==1 || 
      this.permisos.agregaralmacensecundario==1 || this.permisos.ajustaralmacensecundario==1 || this.permisos.editaralmacensecundario==1){
        let menu={ path: '', title: 'ALMACEN SECUNDARO',  icon: 'pe-7s-box1', class: '',componenttitle:"almacenS"};
        let submenu=[];

        if(this.permisos.agregaralmacen==1 || this.permisos.editaralmacen==1 || this.permisos.estatusalmacen==1){
          
          submenu.push({ path: '/almacenes-secundarios', title: 'Almacen Secundario',  icon: '', class: ''});
            
        }

        if(this.permisos.agregaralmacensecundario==1 || this.permisos.ajustaralmacensecundario==1 || this.permisos.editaralmacensecundario==1){
          
          submenu.push({ path: '/almacenSecundario', title: 'Productos de Almacen Secundario',  icon: '', class: ''});
            
        }        
        
        if(this.permisos.generargenerarlista==1 || this.permisos.pacientegenerarsalida==1 || this.permisos.doctorgenerarsalida==1 || this.permisos.tipogenerarsalida==1){
    
          submenu.push({ path: '/generarListaAlmacenSalida', title: 'Generar Salida',  icon: '', class: ''});
            
        }
  
        if(this.permisos.listageneradasalida==1){
    
          submenu.push({ path: '/listaSalidasAlmacenSecundario', title: 'Listas Generadas de Salida',  icon: '', class: ''});
            
        }
  
        this.menu.push({
          menu:menu,
          submenu:submenu
        });
    }

    //modulo configuracion
    if(this.permisos.agregartipocirugia==1 || this.permisos.editartipocirugia==1 || this.permisos.estatustipocirugia==1 ||
      this.permisos.agregarrole==1 || this.permisos.editarrole==1){
        let menu={ path: '', title: 'CONFIGURACION',  icon: 'pe-7s-config', class: '',componenttitle:"configuracion"};
        let submenu=[];
        if(this.permisos.agregartipocirugia==1 || this.permisos.editartipocirugia==1 || this.permisos.estatustipocirugia==1){
    
          submenu.push({ path: '/tipoCirugiaEvento', title: 'Tipo Cirugia o Evento',  icon: '', class: ''});
            
        }
        if(this.permisos.agregarrole==1 || this.permisos.editarrole==1){
    
          submenu.push({ path: '/roles', title: 'Role',  icon: '', class: ''});
            
        }

        this.menu.push({
          menu:menu,
          submenu:submenu
        });
    }
    //modulo sesion
    if(this.usuario!=null){
      let menu={ path: '', title: this.usuario.username,  icon: 'pe-7s-power', class: '',componenttitle:"salir"};
      let submenu=[];
      
      submenu.push({ path: '/login', title: 'Salir',  icon: '', class: ''});
          
      this.menu.push({
        menu:menu,
        submenu:submenu
      });
    }
    //console.info(this.menu);
    //this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.menuItems=this.menu;

  }
  isMobileMenu() {
      if ($(window).width() > 900) {
          return false;
      }
      return true;
  };
}
