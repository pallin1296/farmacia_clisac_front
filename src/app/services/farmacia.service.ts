import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, CanDeactivate, CanLoad, Route, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FarmaciaService implements CanActivate, CanActivateChild, CanDeactivate<unknown>, CanLoad {

  roleAs: any[]=[];

  constructor(private router: Router) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    let url: string = state.url;
    return this.checkUserLogin(next, url);
  }

  canActivateChild(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    return this.canActivate(next, state);
    ;
  }

  canDeactivate(
    component: unknown,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return true;
  }

  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
    return true;
  }

  isAuthenticated() {

    const token = localStorage.getItem('token_AY5634ED');

    return (token == null) ? false : true;

  }

  checkUserLogin(route: ActivatedRouteSnapshot, url: any): boolean {

    if (this.isAuthenticated()) {

      const userRole = this.getRole();

      let clave=this.validarUrl(url);

      if(clave!="sv" && userRole.indexOf(clave)==-1){
        this.router.navigate(['/login']);
        return false;
      }

      return true;

    }

    this.router.navigate(['/login']);

    return false;

  }

  getRole() {

    let permisos = JSON.parse(localStorage.getItem('permisos'));

    if(permisos.agregarpaciente==1 || permisos.paciente==1){
    
      this.roleAs.push("paciente");
        
    }

    if(permisos.agregarempleado==1 || permisos.editarempleado==1 || permisos.estatusempleado==1){

      this.roleAs.push("empleado");
        
    }

    if(permisos.agregardoctor==1 || permisos.editardoctor==1 || permisos.estatusdoctor==1){

      this.roleAs.push("doctor");
        
    }

    if(permisos.agregarcategoria==1 || permisos.editarcategoria==1 || permisos.estatuscategoria==1){
    
      this.roleAs.push("categoria");
        
    }

    if(permisos.agregarproducto==1 || permisos.editarproducto==1 || permisos.estatusproducto==1){

      this.roleAs.push("producto");
        
    }

    if(permisos.agregaralmacenprincipal==1 || permisos.ajustaralmacenprincipal==1 || permisos.editaralmacenprincipal==1){
        
      this.roleAs.push("almacenprincipal");
        
    }

    if(permisos.crearalmacengenerarlista==1){

      this.roleAs.push("generarlistaalmacen");
        
    }

    if(permisos.estatuslistagenerada==1){

      this.roleAs.push("listasgeneradas");
        
    }

    if(permisos.agregaralmacensecundario==1 || permisos.ajustaralmacensecundario==1 || permisos.editaralmacensecundario==1){
          
      this.roleAs.push("almacensecundario");
        
    }        
    
    if(permisos.generargenerarlista==1 || permisos.pacientegenerarsalida==1 || permisos.doctorgenerarsalida==1 || permisos.tipogenerarsalida==1){

      this.roleAs.push("generarlistaalmacensalida");
        
    }

    if(permisos.listageneradasalida==1){

      this.roleAs.push("listasalidaalmacensecundario");
        
    }

    if(permisos.agregartipocirugia==1 || permisos.editartipocirugia==1 || permisos.estatustipocirugia==1){
    
      this.roleAs.push("tipocirugiaevento");
        
    }
    if(permisos.agregaralmacen==1 || permisos.editaralmacen==1 || permisos.estatusalmacen==1){
    
      this.roleAs.push("almacenessecundarios");
        
    }
    if(permisos.ventapacienteventarealizada==1){
    
      this.roleAs.push("ventasrealizadasalmacensecundario");
        
    }
    if(permisos.ventapacienteventa==1){

      this.roleAs.push("ventaalmacensecundario");
        
    }

      this.roleAs.push("ventacortecajageneradas");
        
    if(permisos.listageneradasalida==1){

      this.roleAs.push("listasalidasalmacensecundario");
        
    }
    if(permisos.agregarrole==1 || permisos.editarrole==1){
    
      this.roleAs.push("roles");
        
    }

    return this.roleAs;
  }

  validarUrl(url){
    let rutaActiva=""
    if(url=="/dashboard"){
    
      rutaActiva="sv";
        
    }

    if(url=="/ventas"){

      rutaActiva="sv";
        
    }

    if(url=="/cortes-de-caja-generadas"){

      rutaActiva="sv";
        
    }

    if(url=="/corteCaja"){
        
      rutaActiva="sv";
        
    }

    if(url=="/ventas-generadas"){
        
      rutaActiva="sv";
        
    }

    if(url=="/mostrarVentaActual"){

      rutaActiva="sv";
        
    }

    if(url=="/mostrarVentaPendiente"){
    
      rutaActiva="sv";
        
    }

    if(url=="/consultarVenta"){

      rutaActiva="sv";
        
    }

    if(url=="/producto"){
        
      rutaActiva="producto";
        
    }

    if(url=="/categoria"){

      rutaActiva="categoria";
        
    }

    if(url=="/inventario"){

      rutaActiva="sv";
        
    }

    if(url=="/paciente"){
          
      rutaActiva="paciente";
        
    }        
    
    if(url=="/empleado"){

      rutaActiva="empleado";
        
    }

    if(url=="/roles"){

      rutaActiva="roles";
        
    }

    if(url=="/generarListaAlmacen"){
    
      rutaActiva="generarlistaalmacen";
        
    }
    if(url=="/listasGeneradas"){
    
      rutaActiva="listasgeneradas";
        
    }
    if(url=="/almacenPrincipal"){
    
      rutaActiva="almacenprincipal";
        
    }
    if(url=="/almacenSecundario"){
    
      rutaActiva="almacensecundario";
        
    }
    if(url=="/generarListaAlmacenSalida"){
    
      rutaActiva="generarlistaalmacensalida";
        
    }
    if(url=="/listaSalidasAlmacenSecundario"){
    
      rutaActiva="listasalidasalmacensecundario";
        
    }
    if(url=="/doctor"){
    
      rutaActiva="doctor";
        
    }
    if(url=="/tipoCirugiaEvento"){
    
      rutaActiva="tipocirugiaevento";
        
    }
    if(url=="/venta-almacen-secundario"){
    
      rutaActiva="ventaalmacensecundario";
        
    }

    if(url=="/ventas-realizadas-almacen-secundario"){
    
      rutaActiva="ventasrealizadasalmacensecundario";
        
    }

    if(url=="/almacenes-secundarios"){
    
      rutaActiva="almacenessecundarios";
        
    }

    return rutaActiva;

  }

}

