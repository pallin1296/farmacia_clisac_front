import { FarmaciaService } from './farmacia.service';
import { Router } from '@angular/router';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class InterceptorApiService implements HttpInterceptor {

  constructor(private router: Router, private auth: FarmaciaService) {

  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    // Token
    const token = localStorage.getItem('token_AY5634ED');

    // Lista de urls sin token
    const exceptions = [
      'auth',
      'public/corte-caja/obtener-corte-actual',
      'public/corte-caja/crear-nuevo-corte',
      'public/corte-caja/actualizar-crear-nuevo-corte'
    ];

    // valida las excepciones
    const found = exceptions.find(I => request.url.includes(I));

    /* si no esta en lista de exeptions */
    if (!found) {
      request = request.clone({
        setHeaders: {
          'Content-type': 'application/json',
          'Authorization': `JWT ${ token }`
        }
      });

    } else{
      request = request.clone({
        setHeaders: {
          'Content-type': 'application/json'
        }
      });

    }

    return next.handle(request).pipe(

      catchError(error => {

        if (error instanceof HttpErrorResponse && error.status === 403 || error.status === 401) {

          alert('error');

          this.router.navigateByUrl("/login");

        } else {
            console.info('ErrorHG', error);
            return throwError(error);
        }

      }), finalize( () => {

      })

    );

  }

}
