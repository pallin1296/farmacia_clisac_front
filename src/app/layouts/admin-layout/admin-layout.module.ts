import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LbdModule } from '../../lbd/lbd.module';
import { NguiMapModule} from '@ngui/map';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { AdminLayoutRoutes } from './admin-layout.routing';

import { HomeComponent } from '../../home/home.component';
import { UserComponent } from '../../user/user.component';
import { TablesComponent } from '../../tables/tables.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';

import { EliminarCategoriaComponent } from '../../content-clisac/categoria/eliminar-categoria/eliminar-categoria.component';
import { ModalGenerarCobroComponent } from '../../content-clisac/ventas/modal-generar-cobro/modal-generar-cobro.component';
import { ModalNuevaListaComponent } from '../../content-clisac/generar-lista-almacen/modal-nueva-lista/modal-nueva-lista.component';
import { AgregarProductoComponent } from '../../content-clisac/producto/agregar-producto/agregar-producto.component';
import { EditarProductoComponent } from '../../content-clisac/producto/editar-producto/editar-producto.component';
import { DetalleProductoComponent } from '../../content-clisac/producto/detalle-producto/detalle-producto.component';
import { CambiarPropietarioComponent } from '../../content-clisac/generar-lista-almacen/cambiar-propietario/cambiar-propietario.component';
import { AgregarCategoriaComponent } from '../../content-clisac/categoria/agregar-categoria/agregar-categoria.component';
import { EditarCategoriaComponent } from '../../content-clisac/categoria/editar-categoria/editar-categoria.component';
import { AgregarEmpleadoComponent } from '../../content-clisac/empleado/agregar-empleado/agregar-empleado.component';
import { EditarEmpleadoComponent } from '../../content-clisac/empleado/editar-empleado/editar-empleado.component';
import { DetalleEmpleadoComponent } from '../../content-clisac/empleado/detalle-empleado/detalle-empleado.component';
import { QuitarProductoComponent } from '../../content-clisac/generar-lista-almacen/quitar-producto/quitar-producto.component';
import { QuitarProductoAlmacenPrincipalComponent } from '../../content-clisac/ventas/quitar-producto-almacen-principal/quitar-producto-almacen-principal.component';
import { BuscarProductoComponent } from '../../content-clisac/generar-lista-almacen/buscar-producto/buscar-producto.component';
import { EliminarProductoComponent } from '../../content-clisac/producto/eliminar-producto/eliminar-producto.component';
import { AjustarProductoAlmacenPrincipalComponent } from '../../content-clisac/almacen-principal/ajustar-producto-almacen-principal/ajustar-producto-almacen-principal.component';
import { AgregarProductoAlmacenPrincipalComponent } from '../../content-clisac/almacen-principal/agregar-producto-almacen-principal/agregar-producto-almacen-principal.component';
import { EditarAlmacenPrincipalComponent } from '../../content-clisac/almacen-principal/editar-almacen-principal/editar-almacen-principal.component';
import { AceptarRechazarComponent } from '../../content-clisac/listas-creadas/aceptar-rechazar/aceptar-rechazar.component';
import { DetalleListaComponent } from '../../content-clisac/listas-creadas/detalle-lista/detalle-lista.component';
import { EditarAlmacenSecundarioComponent } from '../../content-clisac/almacen-secundario/editar-almacen-secundario/editar-almacen-secundario.component';
import { AjustarProductoAlmacenSecundarioComponent } from '../../content-clisac/almacen-secundario/ajustar-producto-almacen-secundario/ajustar-producto-almacen-secundario.component';
import { AgregarProductoAlmacenSecundarioComponent } from '../../content-clisac/almacen-secundario/agregar-producto-almacen-secundario/agregar-producto-almacen-secundario.component';
import { NuevaListaSalidaComponent } from '../../content-clisac/generar-lista-almacen-salida/nueva-lista-salida/nueva-lista-salida.component';
import { CambiarPropietarioSalidaComponent } from '../../content-clisac/generar-lista-almacen-salida/cambiar-propietario-salida/cambiar-propietario-salida.component';
import { BuscarProductoSalidaComponent } from '../../content-clisac/generar-lista-almacen-salida/buscar-producto-salida/buscar-producto-salida.component';
import { QuitarProductoSalidaComponent } from '../../content-clisac/generar-lista-almacen-salida/quitar-producto-salida/quitar-producto-salida.component';
import { AgregarTipoCirugiaComponent } from '../../content-clisac/modales-dinamicos/agregar-tipo-cirugia/agregar-tipo-cirugia.component';
import { AgregarPacienteComponent } from '../../content-clisac/modales-dinamicos/agregar-paciente/agregar-paciente.component';
import { AgregarDoctorComponent } from '../../content-clisac/modales-dinamicos/agregar-doctor/agregar-doctor.component';
import { EditarClienteComponent } from '../../content-clisac/cliente/editar-cliente/editar-cliente.component';
import { EditarDoctorComponent } from '../../content-clisac/doctor/editar-doctor/editar-doctor.component';
import { EliminarDoctorComponent } from '../../content-clisac/doctor/eliminar-doctor/eliminar-doctor.component';
import { EditarTipoCirugiaComponent } from '../../content-clisac/tipo-cirugia/editar-tipo-cirugia/editar-tipo-cirugia.component';
import { EliminarTipoCirugiaComponent } from '../../content-clisac/tipo-cirugia/eliminar-tipo-cirugia/eliminar-tipo-cirugia.component';
import { AgregarRoleComponent } from '../../content-clisac/role/agregar-role/agregar-role.component';
import { EditarRoleComponent } from '../../content-clisac/role/editar-role/editar-role.component';
import { EliminarEmpleadoComponent } from '../../content-clisac/empleado/eliminar-empleado/eliminar-empleado.component';
import { DetalleVentaAlmacenPacienteComponent } from '../../content-clisac/venta-almacen-secundario/detalle-venta-almacen-paciente/detalle-venta-almacen-paciente.component';
import { AgregarAlmacenSecundarioComponent } from '../../content-clisac/almacenes-secundarios/agregar-almacen-secundario/agregar-almacen-secundario.component';
import { EliminarAlmacenSecundarioComponent } from '../../content-clisac/almacenes-secundarios/eliminar-almacen-secundario/eliminar-almacen-secundario.component';

import { VentasComponent } from '../../content-clisac/ventas/ventas.component';
import { MostrarVentasActualesComponent } from '../../content-clisac/mostrar-ventas-actuales/mostrar-ventas-actuales.component';
import { MostrarVentasPendientesComponent } from '../../content-clisac/mostrar-ventas-pendientes/mostrar-ventas-pendientes.component';
import { ConsultarVentaComponent } from '../../content-clisac/consultar-venta/consultar-venta.component';
import { ProductoComponent } from '../../content-clisac/producto/producto.component';
import { CategoriaComponent } from '../../content-clisac/categoria/categoria.component';
import { InventarioComponent } from '../../content-clisac/inventario/inventario.component';
import { EmpleadoComponent } from '../../content-clisac/empleado/empleado.component';
import { ClienteComponent } from '../../content-clisac/cliente/cliente.component';
import { EmpresaComponent } from '../../content-clisac/empresa/empresa.component';
import { RoleComponent } from '../../content-clisac/role/role.component';
import { CorteCajaComponent } from '../../content-clisac/corte-caja/corte-caja.component';
import { CorteCajaGeneradosComponent } from '../../content-clisac/corte-caja-generados/corte-caja-generados.component';
import { GenerarListaAlmacenComponent } from '../../content-clisac/generar-lista-almacen/generar-lista-almacen.component';
import { ListasCreadasComponent } from '../../content-clisac/listas-creadas/listas-creadas.component';
import { AlmacenPrincipalComponent } from '../../content-clisac/almacen-principal/almacen-principal.component';
import { AlmacenSecundarioComponent } from '../../content-clisac/almacen-secundario/almacen-secundario.component';
import { GenerarListaAlmacenSalidaComponent } from '../../content-clisac/generar-lista-almacen-salida/generar-lista-almacen-salida.component';
import { SalidasAlmacenPrincipalComponent } from '../../content-clisac/salidas-almacen-principal/salidas-almacen-principal.component';
import { DoctorComponent } from '../../content-clisac/doctor/doctor.component';
import { TipoCirugiaComponent } from '../../content-clisac/tipo-cirugia/tipo-cirugia.component';
import { VentaAlmacenSecundarioComponent } from '../../content-clisac/venta-almacen-secundario/venta-almacen-secundario.component';
import { VentasRealizadasAlmacenSecundarioComponent } from '../../content-clisac/ventas-realizadas-almacen-secundario/ventas-realizadas-almacen-secundario.component';
import { FarmaciaDashboardComponent } from '../../content-clisac/farmacia-dashboard/farmacia-dashboard.component';
import { AlmacenesSecundariosComponent } from '../../content-clisac/almacenes-secundarios/almacenes-secundarios.component';
import { EditarAlmacenComponent } from '../../content-clisac/almacenes-secundarios/editar-almacen/editar-almacen.component';
import { ModalEntradaComponent } from '../../content-clisac/ventas/modal-entrada/modal-entrada.component';
import { ModalSalidaComponent } from '../../content-clisac/ventas/modal-salida/modal-salida.component';
import { VentasGeneradasComponent } from '../../content-clisac/ventas-generadas/ventas-generadas.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    NgxDatatableModule,
    NgSelectModule,
    ReactiveFormsModule,
    LbdModule,
    NguiMapModule.forRoot({apiUrl: 'https://maps.google.com/maps/api/js?key=YOUR_KEY_HERE'})
  ],
  declarations: [
    ModalNuevaListaComponent,
    ModalGenerarCobroComponent,
    GenerarListaAlmacenComponent,
    GenerarListaAlmacenSalidaComponent,
    HomeComponent,
    UserComponent,
    TablesComponent,
    TypographyComponent,
    IconsComponent,
    MapsComponent,
    NotificationsComponent,
    UpgradeComponent,
    VentasComponent,MostrarVentasActualesComponent,
    MostrarVentasPendientesComponent,
    ConsultarVentaComponent,
    ProductoComponent,
    CategoriaComponent,
    InventarioComponent,
    EmpleadoComponent,
    ClienteComponent,
    EmpresaComponent,
    RoleComponent,
    CorteCajaComponent,
    AgregarProductoComponent,
    EditarProductoComponent,
    DetalleProductoComponent,
    CambiarPropietarioComponent,
    AgregarCategoriaComponent,
    EditarCategoriaComponent,
    AgregarEmpleadoComponent,
    EditarEmpleadoComponent,
    DetalleEmpleadoComponent,
    QuitarProductoComponent,
    BuscarProductoComponent,
    ListasCreadasComponent,
    AceptarRechazarComponent,
    DetalleListaComponent,
    EliminarProductoComponent,
    AlmacenPrincipalComponent,
    AjustarProductoAlmacenPrincipalComponent,
    AgregarProductoAlmacenPrincipalComponent,
    EditarAlmacenPrincipalComponent,
    AlmacenSecundarioComponent,
    EditarAlmacenSecundarioComponent,
    AjustarProductoAlmacenSecundarioComponent,
    AgregarProductoAlmacenSecundarioComponent,
    EliminarCategoriaComponent,
    NuevaListaSalidaComponent,
    CambiarPropietarioSalidaComponent,
    BuscarProductoSalidaComponent,
    QuitarProductoSalidaComponent,
    AgregarTipoCirugiaComponent,
    AgregarPacienteComponent,
    AgregarDoctorComponent,
    SalidasAlmacenPrincipalComponent,
    DoctorComponent,
    TipoCirugiaComponent,
    EditarClienteComponent,
    EditarDoctorComponent,
    EliminarDoctorComponent,
    EditarTipoCirugiaComponent,
    EliminarTipoCirugiaComponent,
    AgregarRoleComponent,
    EditarRoleComponent,
    EliminarEmpleadoComponent,
    VentaAlmacenSecundarioComponent,
    VentasRealizadasAlmacenSecundarioComponent,
    DetalleVentaAlmacenPacienteComponent,
    FarmaciaDashboardComponent,
    AgregarAlmacenSecundarioComponent,
    AlmacenesSecundariosComponent,
    EditarAlmacenComponent,
    QuitarProductoAlmacenPrincipalComponent,
    EliminarAlmacenSecundarioComponent,
    ModalEntradaComponent,
    ModalSalidaComponent,
    VentasGeneradasComponent,
    CorteCajaGeneradosComponent
  ]
})

export class AdminLayoutModule {}
