import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MostrarVentasPendientesComponent } from './mostrar-ventas-pendientes.component';

describe('MostrarVentasPendientesComponent', () => {
  let component: MostrarVentasPendientesComponent;
  let fixture: ComponentFixture<MostrarVentasPendientesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MostrarVentasPendientesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MostrarVentasPendientesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
