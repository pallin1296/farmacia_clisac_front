import { Component, OnInit } from '@angular/core';
import { ConectorApiService } from '../../services/conector-api.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ViewChild } from '@angular/core';

@Component({
  selector: 'app-corte-caja-generados',
  templateUrl: './corte-caja-generados.component.html',
  styleUrls: ['./corte-caja-generados.component.css']
})
export class CorteCajaGeneradosComponent implements OnInit {
  contenido:any;
  detalleCorte={}
  @ViewChild('DetalleCorte') ModalDetalleCorte;
  public fechaInicial=this.conector.formatoFechaAnioMesDia(new Date(),"-");
  public fechaFinal=this.conector.formatoFechaAnioMesDia(new Date(),"-");

  constructor(
    private conector: ConectorApiService,
    private modalService: NgbModal
    ) { 

    this.obtenerListaCorteCaja();

  }

  ngOnInit(): void {
  }

  obtenerListaCorteCaja(){

    this.conector.cargandoDatos();
    let data={
      fechaInicial:this.fechaInicial,
      fechaFinal:this.fechaFinal
    }
    this.conector.obtenerCortesCaja(data).subscribe(
      (e: any)=>{
        if(e.ok){
  
          this.contenido=e.data;

          this.conector.cerrarCargandoDatos();
        }else{
          this.conector.cerrarCargandoDatos();
          this.conector.mensajeError(e.msg);
        }
  
      }
    );

  }

  imprimirTicket(datos){

    this.conector.corte58mm(datos);

  }

  openModalDetalleCorte(row){
    this.detalleCorte=row;
    this.modalService.open(this.ModalDetalleCorte, { windowClass: 'animated fadeInDown',size:'lg' });    
  }

  
  cambiarFecha(){

    this.obtenerListaCorteCaja();
  
}

}
