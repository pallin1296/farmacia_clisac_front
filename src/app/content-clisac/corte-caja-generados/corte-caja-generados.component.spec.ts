import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CorteCajaGeneradosComponent } from './corte-caja-generados.component';

describe('CorteCajaGeneradosComponent', () => {
  let component: CorteCajaGeneradosComponent;
  let fixture: ComponentFixture<CorteCajaGeneradosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CorteCajaGeneradosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CorteCajaGeneradosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
