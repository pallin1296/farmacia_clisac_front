import { Component, ComponentFactoryResolver, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { ConectorApiService } from '../../services/conector-api.service';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import swal from 'sweetalert2'
import * as pdfMake from 'pdfmake/build/pdfmake.js';
import * as pdfFonts from 'pdfmake/build/vfs_fonts.js';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import { ModalNuevaListaComponent } from './modal-nueva-lista/modal-nueva-lista.component';
import { CambiarPropietarioComponent } from './cambiar-propietario/cambiar-propietario.component';
import { BuscarProductoComponent } from './buscar-producto/buscar-producto.component';
import { QuitarProductoComponent } from './quitar-producto/quitar-producto.component';


@Component({
  selector: 'app-generar-lista-almacen',
  templateUrl: './generar-lista-almacen.component.html',
  styleUrls: ['./generar-lista-almacen.component.css']
})
export class GenerarListaAlmacenComponent implements OnInit {
  contenido:any;
  listaAlmacenProducto:object[];
  idAlmacen=0;
  idAlmacenSecundario=0;
  almacen="";
  fecha="";
  estatus="Sin Estatus.";
  permisos=JSON.parse(localStorage.getItem('permisos'));

  public envioModal:any

  codigo: string;
  cantidad: string;

  @ViewChild(ModalNuevaListaComponent) nuevaLista:ModalNuevaListaComponent;
  @ViewChild(CambiarPropietarioComponent) cambiarPropietario:CambiarPropietarioComponent;
  @ViewChild(QuitarProductoComponent) quitarProducto:QuitarProductoComponent;
  @ViewChild(BuscarProductoComponent) buscarProducto:BuscarProductoComponent;
  
  constructor (
    private conector: ConectorApiService
    ){
      this.envioModal={}
  }

  ngOnInit(): void {

    this.obtenerUltimaListaAlmacen();
  }


  finalizarLista(){

    swal.fire({
      title: '¿Desea finalizar la lista?',
      text: "",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Finalizar!'
    }).then((result) => {
      if (result.isConfirmed) {

        

        this.conector.finalizarLista({"idalmacen":this.idAlmacen}).subscribe(
          (e: any)=>{
            if(e.ok){
              this.fecha=e.data.fecha
              swal.fire({
                title: 'Aceptar la lista en este momento.',
                text: "",
                icon: 'success',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si',
                cancelButtonText: 'No'

              }).then((result) => {
                if (result.isConfirmed) {
                  let data={
                    "idAlmacenalmacen":e.data.idalmacenalmacen,
                    "idAlmacen":e.data.idalmacen,
                    estatus:"Aceptado"
                  }  
                  this.conector.cambiarEstatus(data).subscribe(
                    (f: any)=>{
                      if(f.ok){

                        this.estatus="Aceptado"
                        this.crearPdfLista();  

                      }else{
                        this.estatus="Sin Estatus."
                        this.crearPdfLista();    

                      }
              
                    }
                  );
                         
                }else{
                  this.estatus="Sin Estatus."
                  this.crearPdfLista();    
                }
              })
            }
    
          }
        );
               
      }
    })

  }

  
  async obtenerUltimaListaAlmacen(){
    this.conector.cargandoDatos();

    this.conector.obtenerUltimaLista().subscribe(
      
      (e: any)=>{
        if(e.ok){

          if(e.data.datosAlmacen!=null){

            this.idAlmacen=e.data.datosAlmacen.idAlmacen;
            this.idAlmacenSecundario=e.data.datosAlmacen.idAlmacenSecundario;
            this.fecha=e.data.datosAlmacen.fecha;
            this.almacen=e.data.datosAlmacen.almacen;
            this.contenido=e.data.lista;   
  
          }
          this.conector.cerrarCargandoDatos();

        }else{
          this.conector.cerrarCargandoDatos();
          //this.conector.mensajeError(e.msg);

        }


      }
    );

  }

  async respuestaBusqueda(data){

    data.idalmacen=this.idAlmacen
    
    this.agregarProducto(data)

  }

  async scanearProducto(){
   
    let codigo = this.codigo.trim();
    let cantidad = (document.getElementById("cantidad") as HTMLInputElement).value;
    let id=this.idAlmacen;

    
    let data=({
      "codigo":codigo,
      "cantidad":cantidad,
      "idalmacen":id
    });

    this.agregarProducto(data);

  }

  agregarProducto(data) {
    
    if(data.codigo=="" || data.cantidad=="" || data.idalmacen==0){
           
      return swal.fire({
        title: 'No se genero escaner',
        html: "Faltan datos importantes."
      });
     
    }
    if(data.cantidad=="0"){
           
      return swal.fire({
        title: 'No se genero escaner',
        html: "Debe ingresar una cantidad valida."
      }); 
  }

    this.conector.agregarProductoALista(data).subscribe(
      (e: any)=>{
        if(!e.ok){
          
          (document.getElementById("codigo") as HTMLInputElement).value="";
          return swal.fire({
            title: 'No se genero escaner',
            html: e.msg
          });
          
        }else{

          this.obtenerUltimaListaAlmacen();
          
        }

        (document.getElementById("codigo") as HTMLInputElement).value="";
        (document.getElementById("codigo") as HTMLInputElement).focus()
      }
    );

  }  

crearPdfLista(){
let data=[];

this.contenido.forEach(element => {
  
  data.push([element.producto,element.cantidad]);

});

  const docDefinition = {
    pageSize: 'A4',
    content: [
      {
        text: `LISTA DE INSUMOS - AREA ${this.almacen}`,
        style: 'header'
      },
      {
        columns: [
          {
            width: '100%',
            text: `Fecha: ${this.conector.obtenerFechaLetra(this.fecha)}`,
            style: 'subheader'
          }        
        ],
        margin: [0, 0, 0, 5]
      }, 
      {
        columns: [
          {
            width: '100%',
            text: `Estatus: `+this.estatus,
            style: 'subheader'
          }          
        ],
        margin: [0, 0, 0, 5]
      },       
      {
        table: {
          widths: ['70%', '30%'],
          headerRows: 0,
          body: [
            [{ text: 'PRODUCTO', style: '' }, { text: 'CANTIDAD', style: '' }],
            ...data
          ]
        },
        margin: [0, 0, 0, 20]
      },
      {
        columns: [
          {
            width: '50%',
            text: `___________________________________`,
            alignment: 'center'
          },
          {
            width: '50%',
            text: `___________________________________`,
            alignment: 'center'
          }
        ],
        margin: [0, 90, 0, 0]
      },
      {
        columns: [
          {
            width: '50%',
            text: `ENCARGADO DE ALMACEN`,
            alignment: 'center'
          },
          {
            width: '50%',
            text: `ENCARGADO DE ${this.almacen}`,
            alignment: 'center'
          }
        ],
        margin: [0,5, 0, 0]
      }
    ],
    styles: {
      header: {
        fontSize: 22,
        bold: true,
        alignment: 'center',
        margin: [0, 0, 0, 30]
      },
      subheader: {
        fontSize: 12,
        bold: true
      }
    }
  };

  pdfMake.createPdf(docDefinition).open({ callback: this.reiniciar() });

}

mostrarListaAlmacen(){
    
  this.nuevaLista.open();
}

modalQuitarProducto(row){

  this.envioModal=row;
    
  this.quitarProducto.open();
}

modalBuscarProducto(){
    
  this.buscarProducto.open();
}

cambiarDePropietario(){
    this.envioModal={
      "idAlmacen":this.idAlmacen,
      "idAlmacenSecundario":this.idAlmacenSecundario
    }

  this.cambiarPropietario.open();
}

reiniciar(){
  this.almacen="";
  this.idAlmacen=0;
  this.idAlmacenSecundario=0;
  this.fecha="";
  this.estatus="Sin Estatus."
  this.contenido=[];
}



}
