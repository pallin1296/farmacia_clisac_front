import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { ConectorApiService } from '../../../services/conector-api.service';

@Component({
  selector: 'app-quitar-producto',
  templateUrl: './quitar-producto.component.html',
  styleUrls: ['./quitar-producto.component.css']
})
export class QuitarProductoComponent implements OnInit {
  
  frmProd: FormGroup;
  titulo="Quitar Producto.";
  cantidadProducto=1;
  cantidadActualizada=1;
  
  @Input() data;
  @Output() actualizar: EventEmitter<any> = new EventEmitter();
  @ViewChild("ModalQuitarProducto") defaultModel: NgbModal;

  constructor(private modalService: NgbModal,
    private conector: ConectorApiService,
    private formBuilder: FormBuilder) {
      this.frmProd = this.formBuilder.group({
        cantidad: [null, Validators.required]
      });        
     }

  ngOnInit(): void {
    this.data={}

  }

  ngOnChanges(){
    this.cantidadProducto=this.data.cantidad;
    this.cantidadActualizada=this.data.cantidad;

    this.frmProd.get("cantidad").setValue(this.data.cantidad);

  }

  sumar(){

    this.cantidadActualizada=Number(this.cantidadActualizada)+1;

    if(this.cantidadActualizada<=this.cantidadProducto){
      this.frmProd.get("cantidad").setValue(this.cantidadActualizada);
    }else{
      this.cantidadActualizada=this.cantidadProducto;
    }

  }
  restar(){

    this.cantidadActualizada=Number(this.cantidadActualizada)-1;

    if(this.cantidadActualizada>=1){
      this.frmProd.get("cantidad").setValue(this.cantidadActualizada);
    }else{
      this.cantidadActualizada=1;
    }

  }

  open() {
    this.modalService.open(this.defaultModel, { windowClass: 'animated fadeInDown',backdrop: 'static',size: 'sm', keyboard: false, centered: true });
  }

  close() {
    this.actualizar.emit();
    this.modalService.dismissAll();
  }

  quitarProducto(){
    this.data.cantidad=this.frmProd.get("cantidad").value;
    
    this.conector.quitarProductoLista(this.data).subscribe(
      (e: any)=>{
        if(e.ok){

          this.frmProd.get("cantidad").setValue(null);
          this.close()

        }else{
          this.conector.mensajeError(e.msg);
        }

      }
    );

  }

}
