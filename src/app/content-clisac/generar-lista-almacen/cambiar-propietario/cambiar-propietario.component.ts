import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { ConectorApiService } from '../../../services/conector-api.service';

@Component({
  selector: 'app-cambiar-propietario',
  templateUrl: './cambiar-propietario.component.html',
  styleUrls: ['./cambiar-propietario.component.css']
})
export class CambiarPropietarioComponent implements OnInit {
  titulo="Cambiar de Almacen";
  contenido:any;
  frmAlm: FormGroup;

  @Input() data;
  @ViewChild("cambiarPropietario") defaultModel: NgbModal;
  @Output() actualizar: EventEmitter<any> = new EventEmitter();

  constructor(private modalService: NgbModal,
    private conector: ConectorApiService,
    private formBuilder: FormBuilder) {
      this.frmAlm = this.formBuilder.group({
        almacen: [null, Validators.required]
      });  

    }
    
  ngOnInit():void {
    this.data={}
   
    this.listarAlmacen();
  }

  ngOnChanges(){
    this.listarAlmacen();
    
      this.frmAlm.get("almacen").setValue(this.data.idAlmacenSecundario);

  }

  listarAlmacen(){
    this.conector.obtenerTodosAlmacenesActivos().subscribe(
      (e: any)=>{
        if(e.ok){

            this.contenido=e.data;   
  
        }


      }
    );

  }  
  open() {
    this.modalService.open(this.defaultModel, { windowClass: 'animated fadeInDown',backdrop: 'static',size: 'lg', keyboard: false, centered: true });
  }

  close() {
    this.actualizar.emit();
    this.modalService.dismissAll();
  }

  guardarCambio(){

    let almacen = this.frmAlm.get("almacen").value

this.conector.cambiarPropietario({"idalmacen":almacen,"idalmacenalmacen":this.data.idAlmacen}).subscribe(
      (e: any)=>{
        if(e.ok){

          this.frmAlm.get("almacen").setValue(null);
          this.close()

        }else{
          this.conector.mensajeError(e.msg);
        }

      }
    );

  }

}
