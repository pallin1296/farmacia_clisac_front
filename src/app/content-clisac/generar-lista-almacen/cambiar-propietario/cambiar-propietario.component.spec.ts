import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CambiarPropietarioComponent } from './cambiar-propietario.component';

describe('CambiarPropietarioComponent', () => {
  let component: CambiarPropietarioComponent;
  let fixture: ComponentFixture<CambiarPropietarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CambiarPropietarioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CambiarPropietarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
