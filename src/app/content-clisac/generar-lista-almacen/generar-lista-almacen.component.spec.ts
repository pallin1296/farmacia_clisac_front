import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerarListaAlmacenComponent } from './generar-lista-almacen.component';

describe('GenerarListaAlmacenComponent', () => {
  let component: GenerarListaAlmacenComponent;
  let fixture: ComponentFixture<GenerarListaAlmacenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GenerarListaAlmacenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerarListaAlmacenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
