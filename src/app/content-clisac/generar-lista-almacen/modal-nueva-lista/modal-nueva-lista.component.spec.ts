import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalNuevaListaComponent } from './modal-nueva-lista.component';

describe('ModalNuevaListaComponent', () => {
  let component: ModalNuevaListaComponent;
  let fixture: ComponentFixture<ModalNuevaListaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalNuevaListaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalNuevaListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
