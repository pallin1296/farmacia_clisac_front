import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { ConectorApiService } from '../../../services/conector-api.service';
@Component({
  selector: 'app-modal-nueva-lista',
  templateUrl: './modal-nueva-lista.component.html',
  styleUrls: ['./modal-nueva-lista.component.css']
})
export class ModalNuevaListaComponent implements OnInit {
  
  frmAlm: FormGroup;
  contenido:any;
  activo=false;
  titulo="Generar Nueva Lista";

  @ViewChild("ModalListaAlmacen") defaultModel: NgbModal;
  @Output() actualizar: EventEmitter<any> = new EventEmitter();

  constructor(private modalService: NgbModal,
    private conector: ConectorApiService,
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.listarAlmacen();
    this.frmAlm = this.formBuilder.group({
      almacen: [null, Validators.required]
    });      
  }

  open() {
    this.modalService.open(this.defaultModel, { windowClass: 'animated fadeInDown',backdrop: 'static',size: 'lg', keyboard: false, centered: true });
  }

  close() {
    this.actualizar.emit();
    this.modalService.dismissAll();
  }

  boton(){
    this.activo=!this.activo;

    if(this.activo){
      this.titulo="Guardar almacen";
    }else{
      this.titulo="Generar Nueva Lista";
    }
  }

  listarAlmacen(){
    this.conector.obtenerTodosAlmacenesActivos().subscribe(
      (e: any)=>{
        if(e.ok){

            this.contenido=e.data;   
  
        }


      }
    );

  }
  
  guardarListaAlmacen(){
    let almacen = this.frmAlm.get("almacen").value

    this.conector.crearListaAlmacen({"idalmacen":almacen}).subscribe(
      (e: any)=>{
        if(e.ok){
          this.frmAlm.get("almacen").setValue(null);

          this.close()

        }

      }
    );

  }
  guardarAlmacen(){
    let almacen = (document.getElementById("agregarAlmacen") as HTMLInputElement).value;

    this.conector.agregarAlmacen({"almacen":almacen}).subscribe(
      (e: any)=>{
        if(e.ok){
          (document.getElementById("agregarAlmacen") as HTMLInputElement).value="";

          this.listarAlmacen();

        }

      }
    );

  }

}
