import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { ConectorApiService } from '../../../services/conector-api.service';

@Component({
  selector: 'app-buscar-producto',
  templateUrl: './buscar-producto.component.html',
  styleUrls: ['./buscar-producto.component.css']
})
export class BuscarProductoComponent implements OnInit {
 
  frmProd: FormGroup;
  titulo="Buscar producto";
  contenido:any;
  @Output() producto: EventEmitter<any> = new EventEmitter();

  @ViewChild("ModalBuscarProducto") defaultModel: NgbModal;

  constructor(private modalService: NgbModal,
    private conector: ConectorApiService,
    private formBuilder: FormBuilder) {
      this.frmProd = this.formBuilder.group({
        producto: [null, Validators.required],
        cantidad: [null, Validators.required]

      });   
           
     }

  ngOnInit(): void {
    this.listarProducto();
    this.frmProd.get("cantidad").setValue(1)
  }

  ngOnChages(){
    this.frmProd.get("cantidad").setValue(1)
    this.frmProd.get("producto").setValue(null)


  }

  open() {
    this.modalService.open(this.defaultModel, { windowClass: 'animated fadeInDown',backdrop: 'static',size: 'lg', keyboard: false, centered: true });
    this.frmProd.get("cantidad").setValue(1)
    this.frmProd.get("producto").setValue(null)

  }

  close() {
    this.modalService.dismissAll();
  }
  
  listarProducto(){
    this.conector.obtenerTodosProductos().subscribe(
      (e: any)=>{
        if(e.ok){

            this.contenido=e.data;   
  
        }

      }
    );

  }

  elegirProducto(){

    let data=({

      "codigo":this.frmProd.get("producto").value,
      "cantidad":this.frmProd.get("cantidad").value

    })

    if(this.frmProd.get("producto").value!=null && this.frmProd.get("cantidad").value){
      this.producto.emit(data);

      this.close();
    }
 

  }
  
}
