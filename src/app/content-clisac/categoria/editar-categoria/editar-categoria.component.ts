import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, Output, OnInit, ViewChild,EventEmitter,Input } from '@angular/core';
import { ConectorApiService } from '../../../services/conector-api.service';

@Component({
  selector: 'app-editar-categoria',
  templateUrl: './editar-categoria.component.html',
  styleUrls: ['./editar-categoria.component.css']
})
export class EditarCategoriaComponent implements OnInit {
  @Input() data
  @Input() edit:any[];
  submitted=false;
  errorCodigo="";
  detalleCategoria={}
  @Output() actualizar: EventEmitter<any> = new EventEmitter();
  @ViewChild('EditarCategoria') modal
  public EditarRegistro: FormGroup

  constructor(
    private _modal: NgbModal,
    private conector: ConectorApiService,
    private _builder: FormBuilder    
  ) { 
    this.data = {}
    this.EditarRegistro = this._builder.group({
      categoria: ['',[Validators.required]]
    })

  }

  ngOnInit(): void {

    this.enviarDatos();

}

ngOnChanges(){
  this.enviarDatos();
        
}
enviarDatos(){
  this.fData.categoria.setValue(this.data.categoria); 

}
limpiarInput(){

  this.submitted=false;
  this.errorCodigo="";
  this.fData.categoria.setValue(null);
}

validarCategoria(){

  this.submitted = true

  if(
    this.fData.categoria.value==null
    ){
    console.info("Error con los datos");
    return false
  }else{
    return true
  }

}

get fData(){
  return this.EditarRegistro.controls  
}
  close() {
    this.actualizar.emit();
    this._modal.dismissAll();
  }

  show(){
    this._modal.open(this.modal, { windowClass: 'animated fadeInDown',backdrop: 'static',size: 'lg', keyboard: false, centered: true })
  }

  editarCategoria(){

    if(this.validarCategoria()){
      
      let data={
        "idcategoria":this.data.idcategoria,
        "categoria":this.fData.categoria.value
      }

     this.conector.editarCategoria(data).subscribe(
        (e: any)=>{
          if(e.ok){
    
            this.limpiarInput();
            this.conector.mensajeExitoso(e.msg);
            this.close();
    
          }else{
            this.conector.mensajeError(e.msg);
          }
    
        }
      );    
    
    }
    
    }

}
