import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConectorApiService } from '../../../services/conector-api.service';

@Component({
  selector: 'app-eliminar-categoria',
  templateUrl: './eliminar-categoria.component.html',
  styleUrls: ['./eliminar-categoria.component.css']
})
export class EliminarCategoriaComponent implements OnInit {
  
  titulo="";
  @Input() data;
  @Output() actualizar: EventEmitter<any> = new EventEmitter();
  @ViewChild("ModalEliminarCategoria") defaultModel: NgbModal;

  constructor(private modalService: NgbModal,
    private conector: ConectorApiService) {    
      this.data={}
      
  }

  ngOnInit(): void {
  }

  ngOnChanges(){

    if(this.data.alta==1){
      this.titulo="Bajar Categoria.";

    }
    if(this.data.alta==2){
      this.titulo="Alta Categoria.";

    } 
    
  }

  open() {
    this.modalService.open(this.defaultModel, { windowClass: 'animated fadeInDown',backdrop: 'static',size: 'sm', keyboard: false, centered: true });
  }

  close() {
    this.actualizar.emit();
    this.modalService.dismissAll();
  }

  eliminarCategoria(){
    let estatus={}

    if(this.data.alta==1){
      estatus={
        "idcategoria":this.data.idcategoria,
        "alta":2
      }
    }

    if(this.data.alta==2){
      estatus={
        "idcategoria":this.data.idcategoria,
        "alta":1
      }
    }
    
    this.conector.estatusCategoria(estatus).subscribe(
      (e: any)=>{
        if(e.ok){
          this.conector.mensajeExitoso(e.msg);
          this.close()

        }else{
          this.conector.mensajeError(e.msg);
        }

      }
    );

  }  

}
