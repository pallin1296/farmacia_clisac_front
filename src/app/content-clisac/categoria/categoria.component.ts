import { Component, OnInit, ViewChild } from '@angular/core';
import { ConectorApiService } from '../../services/conector-api.service';
import { AgregarCategoriaComponent } from './agregar-categoria/agregar-categoria.component'
import { EditarCategoriaComponent } from './editar-categoria/editar-categoria.component'
import { EliminarCategoriaComponent } from './eliminar-categoria/eliminar-categoria.component'

@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.component.html',
  styleUrls: ['./categoria.component.css']
})
export class CategoriaComponent implements OnInit {
  @ViewChild(EditarCategoriaComponent) editarCatoria: EditarCategoriaComponent
  @ViewChild(EliminarCategoriaComponent) eliminarCategoria: EliminarCategoriaComponent
  @ViewChild(AgregarCategoriaComponent) crearCategoria: AgregarCategoriaComponent
  
  contenido:any;
  master:any;

  public busqueda;
  public envioModalEditar:any;
  public envioModalEstatus:any;
  public opciones;  
  permisos=JSON.parse(localStorage.getItem('permisos'));

  constructor(
    private conector: ConectorApiService
  ) {
    this.opciones="Todo"
    this.busqueda="";
    this.envioModalEditar={}
    this.envioModalEstatus={}
    
    this.actualizarListaCategoria();

   }

   ngOnInit(): void {

  }

ngOnChanges(){
  this.actualizarListaCategoria();
}

openCrear(){
  this.crearCategoria.show();
}

openEditar(row){
  this.envioModalEditar={
    "idcategoria":row.idcategoria,
    "categoria":row.categoria
  }    
  this.editarCatoria.show();
}

estatusCategoria(row){
  this.envioModalEstatus={
    "idcategoria":row.idcategoria,
    "alta":row.alta
  }    
  this.eliminarCategoria.open();
}

async actualizarListaCategoria(){
  this.conector.cargandoDatos();

  this.conector.obtenerCategoria().subscribe(
    (e: any)=>{
      if(e.ok){

        this.contenido=e.data
        this.master=e.data

        this.categoriaBusqueda();
        this.conector.cerrarCargandoDatos();

      }else{
        this.conector.cerrarCargandoDatos();
        this.conector.mensajeError(e.msg);
      }

    }
  );
}

categoriaBusqueda(){

  this.contenido=[]

  if(this.opciones==undefined || this.opciones=='Todo'){
    if(this.busqueda!=""){

    this.master.forEach((x) => {

        if(x.categoria?.toLowerCase()?.includes(this.busqueda?.toLowerCase())){
          this.contenido.push(x)
        }
  
    });
  }else{
    this.contenido=this.master;
  }

  }else{

    if(this.opciones=='Activos'){
     
      this.master.forEach((x) => {

        if(this.busqueda!=""){
          if(x.categoria?.toLowerCase()?.includes(this.busqueda?.toLowerCase()) && x.alta==1){
            this.contenido.push(x)
          }
        }else{
          if(x.alta==1){
            this.contenido.push(x)
          }
        }
    
      });

    }

    if(this.opciones=='Inactivos'){
     
      this.master.forEach((x) => {
        
        if(this.busqueda!=""){
          if(x.categoria?.toLowerCase()?.includes(this.busqueda?.toLowerCase()) && x.alta==2){
            this.contenido.push(x)
          }
        }else{
          if(x.alta==2){
            this.contenido.push(x)
          }
        }
    
      });

    }

  }

}

}
