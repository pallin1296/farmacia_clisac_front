import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, Output, OnInit, ViewChild,EventEmitter,Input } from '@angular/core';
import { ConectorApiService } from '../../../services/conector-api.service';

@Component({
  selector: 'app-agregar-categoria',
  templateUrl: './agregar-categoria.component.html',
  styleUrls: ['./agregar-categoria.component.css']
})
export class AgregarCategoriaComponent implements OnInit {
  
  @Input() data
  @Output() actualizar: EventEmitter<any> = new EventEmitter();
  @ViewChild('NuevaCategoria') modal
  public NuevoRegistro: FormGroup
  submitted=false;
  errorCodigo="";

  constructor(
    private _modal: NgbModal,
    private conector: ConectorApiService,
    private _builder: FormBuilder    
  ) { 
    this.data = {}
    this.NuevoRegistro = this._builder.group({
      categoria: ['',[Validators.required]]
    })    
  }

  ngOnInit(): void {
  }

  close() {
    this.actualizar.emit();
    this._modal.dismissAll();
  }

  show(){
    this._modal.open(this.modal, { windowClass: 'animated fadeInDown',backdrop: 'static',size: 'lg', keyboard: false, centered: true })
  }

  get fData(){
    return this.NuevoRegistro.controls  
  }

  limpiarInput(){

    this.submitted=false;
    this.fData.categoria.setValue(null); 

  }

  validarCategoria(){

    this.submitted = true

    if(
      this.fData.categoria.value==null
      ){
      console.info("Error con los datos");
      return false
    }else{
      return true
    }

  }

  guardarCategoria(){

    if(this.validarCategoria()){
      
      let data={
        "categoria":this.fData.categoria.value
      }
    
      this.conector.guardarCategoria(data).subscribe(
        (e: any)=>{
          if(e.ok){
    
            this.limpiarInput();
            this.actualizar.emit();
            this.conector.mensajeExitoso(e.msg);
          }else{
            this.conector.mensajeError(e.msg);
          }
    
        }
      );
    
    
    }
    
    }

}
