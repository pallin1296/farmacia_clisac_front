import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConectorApiService } from '../../../services/conector-api.service';
@Component({
  selector: 'app-eliminar-empleado',
  templateUrl: './eliminar-empleado.component.html',
  styleUrls: ['./eliminar-empleado.component.css']
})
export class EliminarEmpleadoComponent implements OnInit {

  titulo="";
  @Input() data;
  @Output() actualizar: EventEmitter<any> = new EventEmitter();
  @ViewChild("ModalEliminarEmpleado") defaultModel: NgbModal;

  constructor(private modalService: NgbModal,
    private conector: ConectorApiService) {    
      this.data={}
      
  }

  ngOnInit(): void {
  }

  ngOnChanges(){

    if(this.data.alta==1){
      this.titulo="Bajar Empleado.";

    }
    if(this.data.alta==2){
      this.titulo="Alta Empleado.";

    } 
    
  }

  open() {
    this.modalService.open(this.defaultModel, { windowClass: 'animated fadeInDown',backdrop: 'static',size: 'sm', keyboard: false, centered: true });
  }

  close() {
    this.actualizar.emit();
    this.modalService.dismissAll();
  }

  eliminarEmpleado(){
    let estatus={}

    if(this.data.alta==1){
      estatus={
        "idempleado":this.data.idempleado,
        "alta":2
      }
    }

    if(this.data.alta==2){
      estatus={
        "idempleado":this.data.idempleado,
        "alta":1
      }
    }
    
    this.conector.estatusEmpleado(estatus).subscribe(
      (e: any)=>{
        if(e.ok){
          this.conector.mensajeExitoso(e.msg);
          this.close()

        }else{
          this.conector.mensajeError(e.msg);
        }

      }
    );

  }  

}
