import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, Output, OnInit, ViewChild,EventEmitter,Input } from '@angular/core';
import { ConectorApiService } from '../../../services/conector-api.service';

@Component({
  selector: 'app-editar-empleado',
  templateUrl: './editar-empleado.component.html',
  styleUrls: ['./editar-empleado.component.css']
})
export class EditarEmpleadoComponent implements OnInit {

  @Input() data
  submitted=false;
  errorUsuario="";
  detalleEmpleado={}
  valorRole:string;
  selectedRole=null;
  valorVistaRole=false;
  public roles: any[]
  @Output() actualizar: EventEmitter<any> = new EventEmitter();
  @ViewChild('EditarEmpleado') modal
  public EditarRegistro: FormGroup

  constructor(
    private _modal: NgbModal,
    private conector: ConectorApiService,
    private _builder: FormBuilder
  ) {
    this.data = {}
    this.EditarRegistro = this._builder.group({
      nombre: ['',[Validators.required]],
      domicilio: ['',[Validators.required]],
      usuario: ['',[Validators.required]],
      password: ['',[Validators.required]],
      role: ['']
    })

   }

   ngOnInit(): void {
    if(this.data.id!=undefined){
      this.valorRole=this.data['idrole'];
     this.obtenerRoles();
     this.enviarDatos();
    }
  }
  ngOnChanges(){
    if(this.data.id!=undefined){
    this.valorRole=this.data['idrole'];
    this.obtenerRoles();
    this.enviarDatos();
    }
  }

  enviarDatos(){

    this.fData.nombre.setValue(this.data['nombre']); 
    this.fData.domicilio.setValue(this.data['domicilio']);
    this.fData.usuario.setValue(this.data['usuario']);
    this.fData.password.setValue(this.data['password']);

  }  
  
  close() {
    this.actualizar.emit();
    this._modal.dismissAll();
  }

  show(){
    this._modal.open(this.modal, { windowClass: 'animated fadeInDown',backdrop: 'static',size: 'lg', keyboard: false, centered: true })
  }

  get fData(){
    return this.EditarRegistro.controls  
  }
 
  limpiarInput(){

    this.submitted=false;
    this.errorUsuario="";
    this.fData.nombre.setValue(null); 
    this.fData.domicilio.setValue(null);
    this.fData.usuario.setValue(null);
    this.fData.password.setValue(null);

  }

  validarEmpleado(){

    this.submitted = true

    if(
      this.fData.nombre.value==null ||
      this.fData.domicilio.value==null ||
      this.fData.usuario.value==null ||
      this.fData.password.value==null ||
      this.fData.role.value==undefined
      ){
      console.info("Error con los datos");
      return false
    }else{
      return true
    }

  }

  editarEmpleado(){

    if(this.validarEmpleado()){
      
      let data={
        "nombre":this.fData.nombre.value,
        "domicilio":this.fData.domicilio.value,
        "usuario":this.fData.usuario.value,
        "password":this.fData.password.value,
        "idrole":this.fData.role.value,
        "idusuario":this.data.id
      }

      this.conector.editarEmpleado(data).subscribe(
        (e: any)=>{
          if(e.ok){
    
            this.limpiarInput();
            this.conector.mensajeExitoso(e.msg);
            this.close();
    
          }else{

            if(e.msg=="Usuario no disponible."){
              this.errorUsuario=e.msg;
            }else{
              this.conector.mensajeError(e.msg);
            }
          }
    
        }
      );
    
    
    }
    
    }
    obtenerRoles(){
      this.conector.obtenerRoles().subscribe(
        (e: any)=>{
          if(e.ok){
    
            this.roles=e.data

            this.obtenerOpciones();

          }else{
            this.conector.mensajeError(e.msg);
          }
    
        }
      );
    
    }

    crear(){
      this.obtenerOpciones();
      }
    
      async obtenerOpciones(){
    
        if(this.valorRole!=null && this.valorRole!=undefined){

          this.roles.forEach(element => {
    
            if(element.idrole==this.valorRole){
      
              this.selectedRole=element;
      
            }
            
          });
    
        }else{
          this.selectedRole=null;
        }

      }
    
      mostrarValoresRol(){
        
        if(this.valorVistaRole){
          this.valorVistaRole=false;
        }else{
          this.valorVistaRole=true;
        }
    
      }

}
