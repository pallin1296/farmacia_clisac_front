import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, Output, OnInit, ViewChild,EventEmitter,Input } from '@angular/core';
import { ConectorApiService } from '../../../services/conector-api.service';

@Component({
  selector: 'app-agregar-empleado',
  templateUrl: './agregar-empleado.component.html',
  styleUrls: ['./agregar-empleado.component.css']
})
export class AgregarEmpleadoComponent implements OnInit {
  
  @Input() data
  submitted=false;
  errorUsuario="";
  valorRole:string;
  selectedRole=null;
  valorVistaRole=false;
  public roles: any[]
  @Output() actualizar: EventEmitter<any> = new EventEmitter();
  @ViewChild('NuevoEmpleado') modal
  public NuevoRegistro: FormGroup

  constructor(
    private _modal: NgbModal,
    private conector: ConectorApiService,
    private _builder: FormBuilder
  ) {
    this.data = {}
    this.NuevoRegistro = this._builder.group({
      nombre: ['',[Validators.required]],
      domicilio: ['',[Validators.required]],
      usuario: ['',[Validators.required]],
      password: ['',[Validators.required]],
      role: ['']
    })

   }

  ngOnInit(): void {
    this.obtenerRoles();
    this.limpiarInput();
  }

  ngOnChanges(){
    this.obtenerRoles();
    this.limpiarInput();
  }

  get fData(){
    return this.NuevoRegistro.controls  
  }

  limpiarInput(){

    this.submitted=false;
    this.errorUsuario="";
    this.fData.nombre.setValue(null); 
    this.fData.domicilio.setValue(null);
    this.fData.usuario.setValue(null);
    this.fData.password.setValue(null);

  }

  validarEmpleado(){

    this.submitted = true

    if(
      this.fData.nombre.value==null ||
      this.fData.domicilio.value==null ||
      this.fData.usuario.value==null ||
      this.fData.password.value==null ||
      this.fData.role.value==undefined
      ){
      console.info("Error con los datos");
      return false
    }else{
      return true
    }

  }

  guardarEmpleado(){

    if(this.validarEmpleado()){
      
      let data={
        "nombre":this.fData.nombre.value,
        "domicilio":this.fData.domicilio.value,
        "usuario":this.fData.usuario.value,
        "password":this.fData.password.value,
        "idrole":this.fData.role.value
      }
    
      this.conector.agregarEmpleado(data).subscribe(
        (e: any)=>{
          if(e.ok){
    
            this.limpiarInput();
            this.conector.mensajeExitoso(e.msg);
            this.actualizar.emit();
    
          }else{
            console.log(e.msg);
            if(e.msg=="Usuario no disponible."){
              this.errorUsuario=e.msg;
            }else{
              this.conector.mensajeError(e.msg);
            }
          }
    
        }
      );
    
    
    }
    
    }

  close() {
    this.actualizar.emit();
    this._modal.dismissAll();
  }

  show(){
    this._modal.open(this.modal, { windowClass: 'animated fadeInDown',backdrop: 'static',size: 'xl', keyboard: false, centered: false })
  }

  obtenerRoles(){
    this.conector.obtenerRoles().subscribe(
      (e: any)=>{
        if(e.ok){
  
          this.roles=e.data
  
        }else{
          this.conector.mensajeError(e.msg);
        }
  
      }
    );
  
  }

  crear(){
  this.obtenerOpciones();
  }

  async obtenerOpciones(){

    if(this.valorRole!=null && this.valorRole!=undefined){
      this.roles.forEach(element => {

        if(element.idrole==this.valorRole){
  
          this.selectedRole=element;
  
        }
        
      });

    }else{
      this.selectedRole=null;
    }

  }

  mostrarValoresRol(){
    
    if(this.valorVistaRole){
      this.valorVistaRole=false;
    }else{
      this.valorVistaRole=true;
    }

  }

}
