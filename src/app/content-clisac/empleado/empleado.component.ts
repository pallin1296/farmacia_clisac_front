import { Component, OnInit, ViewChild } from '@angular/core';
import { AgregarEmpleadoComponent } from './agregar-empleado/agregar-empleado.component'
import { EditarEmpleadoComponent } from './editar-empleado/editar-empleado.component'
import { DetalleEmpleadoComponent } from './detalle-empleado/detalle-empleado.component'
import { EliminarEmpleadoComponent } from './eliminar-empleado/eliminar-empleado.component'
import { FormBuilder } from '@angular/forms';
import { ConectorApiService } from '../../services/conector-api.service';

@Component({
  selector: 'app-empleado',
  templateUrl: './empleado.component.html',
  styleUrls: ['./empleado.component.css']
})
export class EmpleadoComponent implements OnInit {

  @ViewChild(AgregarEmpleadoComponent) crearEmpleado: AgregarEmpleadoComponent
  @ViewChild(EditarEmpleadoComponent) editarEmpleado: EditarEmpleadoComponent
  @ViewChild(DetalleEmpleadoComponent) detalleEmpleado: DetalleEmpleadoComponent
  @ViewChild(EliminarEmpleadoComponent) eliminarEmpleado: EliminarEmpleadoComponent
  
  master:any;
  contenido:any;

  public opciones;  
  public busqueda;
  public envioModalAgregar:any;
  public envioModalDetalle:any;
  public envioModalEditar:any;
  public envioModalEstatus:any;
  
  permisos=JSON.parse(localStorage.getItem('permisos'));

  constructor(private conector: ConectorApiService, private fb: FormBuilder) {
    this.opciones="Todo"
    this.busqueda="";

    this.envioModalDetalle={}
    this.envioModalEditar={}
    this.envioModalEstatus={}
    this.envioModalAgregar={}
    
    this.actualizarListaEmpleado();
 
  }

   ngOnInit(): void {

  }

ngOnChanges(){
  this.actualizarListaEmpleado();
}

openCrear(){
  this.crearEmpleado.show();
}

openEditar(row){
  this.envioModalEditar=row;
  this.editarEmpleado.show();
}

openDetalle(row){
  this.envioModalDetalle=row

  this.detalleEmpleado.show();
}

async actualizarListaEmpleado(){
  this.conector.cargandoDatos();

  this.conector.obtenerEmpleados().subscribe(
    (e: any)=>{
      if(e.ok){

        this.contenido=e.data
        this.master=e.data
        this.empleadoBusqueda();
        this.conector.cerrarCargandoDatos();

      }else{
        this.conector.cerrarCargandoDatos();
        this.conector.mensajeError(e.msg);

      }

    }
  );
}

estatusEmpleado(row){
  console.info(row);
  this.envioModalEstatus={
    "idempleado":row.id,
    "alta":row.alta
  }    
  this.eliminarEmpleado.open();
}

empleadoBusqueda(){

  this.contenido=[]

  if(this.opciones==undefined || this.opciones=='Todo'){
    if(this.busqueda!=""){

    this.master.forEach((x) => {

        if(x.nombre?.toLowerCase()?.includes(this.busqueda?.toLowerCase())){
          this.contenido.push(x)
        }
  
    });
  }else{
    this.contenido=this.master;
  }

  }else{

    if(this.opciones=='Activos'){
     
      this.master.forEach((x) => {

        if(this.busqueda!=""){
          if(x.nombre?.toLowerCase()?.includes(this.busqueda?.toLowerCase()) && x.alta==1){
            this.contenido.push(x)
          }
        }else{
          if(x.alta==1){
            this.contenido.push(x)
          }
        }
    
      });

    }

    if(this.opciones=='Inactivos'){
     
      this.master.forEach((x) => {
        
        if(this.busqueda!=""){
          if(x.nombre?.toLowerCase()?.includes(this.busqueda?.toLowerCase()) && x.alta==2){
            this.contenido.push(x)
          }
        }else{
          if(x.alta==2){
            this.contenido.push(x)
          }
        }
    
      });

    }

  }

}

}
