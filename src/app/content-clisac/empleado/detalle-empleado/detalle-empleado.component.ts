import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, Output, OnInit, ViewChild,EventEmitter,Input,ChangeDetectorRef } from '@angular/core';
import { ConectorApiService } from '../../../services/conector-api.service';

@Component({
  selector: 'app-detalle-empleado',
  templateUrl: './detalle-empleado.component.html',
  styleUrls: ['./detalle-empleado.component.css']
})
export class DetalleEmpleadoComponent implements OnInit {
  
  @Input() data
  submitted=false;
  errorCodigo="";
  @ViewChild('DetalleEmpleado') modal: NgbModal;
  valorRole:string;
  selectedRole=null;
  valorVistaRole=false;
  valorVistaPassword=false;
  password="*********";
  public roles: any[]
  constructor(
    private _modal: NgbModal,
    private conector: ConectorApiService,
    private cdref: ChangeDetectorRef
  ) {
    this.data = {}
   }

   ngOnInit(){

    this.obtenerRoles();

  }
 ngOnChanges(){
  if(this.data.id!=undefined){
   this.obtenerOpciones();
  }
 }

 ngAfterContentChecked() {
  this.cdref.detectChanges();    
   }

  close() {

    this._modal.dismissAll();
  }

  show(){
    this._modal.open(this.modal, { windowClass: 'animated fadeInDown',backdrop: 'static',size: 'lg', keyboard: false, centered: true })
  }

  obtenerRoles(){
    this.conector.obtenerRoles().subscribe(
      (e: any)=>{
        if(e.ok){
  
          this.roles=e.data
  
        }else{
          this.conector.mensajeError(e.msg);
        }
  
      }
    );
  
  }

obtenerOpciones(){

      this.roles.forEach(element => {
  
          if(element.idrole==this.data.idrole){
    
            this.selectedRole=element;
    
          }
          
        });
    
    }
  
    mostrarValoresRol(){
      
      if(this.valorVistaRole){
        this.valorVistaRole=false;
      }else{
        this.valorVistaRole=true;
      }
  
    }

    mostrarValoresPassword(){
      
      if(this.valorVistaPassword){
        this.valorVistaPassword=false;
        this.password="*********";
      }else{
        this.valorVistaPassword=true;
        this.password=this.data.password;
      }
  
    }

}
