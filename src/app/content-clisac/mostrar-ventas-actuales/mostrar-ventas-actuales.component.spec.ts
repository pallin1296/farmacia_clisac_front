import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MostrarVentasActualesComponent } from './mostrar-ventas-actuales.component';

describe('MostrarVentasActualesComponent', () => {
  let component: MostrarVentasActualesComponent;
  let fixture: ComponentFixture<MostrarVentasActualesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MostrarVentasActualesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MostrarVentasActualesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
