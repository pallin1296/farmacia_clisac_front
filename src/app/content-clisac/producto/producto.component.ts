import { Component, OnInit, ViewChild } from '@angular/core';
import { AgregarProductoComponent } from './agregar-producto/agregar-producto.component'
import { EditarProductoComponent } from './editar-producto/editar-producto.component'
import { DetalleProductoComponent } from './detalle-producto/detalle-producto.component'
import { EliminarProductoComponent } from './eliminar-producto/eliminar-producto.component'
import { FormBuilder } from '@angular/forms';
import { ConectorApiService } from '../../services/conector-api.service';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {

  @ViewChild(AgregarProductoComponent) crearProducto: AgregarProductoComponent
  @ViewChild(EditarProductoComponent) editarProducto: EditarProductoComponent
  @ViewChild(DetalleProductoComponent) detalleProducto: DetalleProductoComponent
  @ViewChild(EliminarProductoComponent) eliminarProducto: EliminarProductoComponent

  contenido:any=[];
  public envioModalDetalle:any;
  public envioModalEditar:any;
  public envioModalEstatus:any;
  public busqueda:any;
  public opciones:any;
  master:any=[];

  permisos=JSON.parse(localStorage.getItem('permisos'));
  
  constructor(private conector: ConectorApiService, private fb: FormBuilder) {
    this.opciones="Todo"
    this.busqueda="";

    this.envioModalDetalle={}
    this.envioModalEditar={}
    this.envioModalEstatus={}
   
    this.actualizarListaProducto();

   }

  ngOnInit(): void {

  }

ngOnChanges(){
  this.actualizarListaProducto();
}

  openCrear(){
    this.crearProducto.show();
  }

  openEditar(row){
    this.envioModalEditar={
      "idproducto":row.idproducto
    }    
    this.editarProducto.show();
  }

  openDetalle(row){
    this.envioModalDetalle={
      "idproducto":row.idproducto
    }

    this.detalleProducto.show();
  }

  async actualizarListaProducto(){
    this.conector.cargandoDatos();

    this.conector.obtenerProductos().subscribe(
      (e: any)=>{
        if(e.ok){
  
          this.contenido=e.data;
          this.master=e.data;

          this.productoBusqueda();
          this.conector.cerrarCargandoDatos();
        }else{
          this.conector.cerrarCargandoDatos();
          this.conector.mensajeError(e.msg);

        }
  
      }
    );
  }

  estatusProducto(row){
    this.envioModalEstatus={
      "idproducto":row.idproducto,
      "alta":row.alta
    }    
    this.eliminarProducto.open();
  }

  productoBusqueda(){

    this.contenido=[]
  
    if(this.opciones==undefined || this.opciones=='Todo'){
      if(this.busqueda!=""){
  
      this.master.forEach((x) => {
  
          if(x.producto?.toLowerCase()?.includes(this.busqueda?.toLowerCase())){
            this.contenido.push(x)
          }
    
      });
    }else{
      this.contenido=this.master;
    }
  
    }else{
  
      if(this.opciones=='Activos'){
       
        this.master.forEach((x) => {
  
          if(this.busqueda!=""){
            if(x.producto?.toLowerCase()?.includes(this.busqueda?.toLowerCase()) && x.alta==1){
              this.contenido.push(x)
            }
          }else{
            if(x.alta==1){
              this.contenido.push(x)
            }
          }
      
        });
  
      }
  
      if(this.opciones=='Inactivos'){
       
        this.master.forEach((x) => {
          
          if(this.busqueda!=""){
            if(x.producto?.toLowerCase()?.includes(this.busqueda?.toLowerCase()) && x.alta==2){
              this.contenido.push(x)
            }
          }else{
            if(x.alta==2){
              this.contenido.push(x)
            }
          }
      
        });
  
      }
  
    }
  
  }


}
