import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, Output, OnInit, ViewChild,EventEmitter,Input } from '@angular/core';
import { ConectorApiService } from '../../../services/conector-api.service';


@Component({
  selector: 'app-detalle-producto',
  templateUrl: './detalle-producto.component.html',
  styleUrls: ['./detalle-producto.component.css']
})
export class DetalleProductoComponent implements OnInit {
  @ViewChild('DetalleProducto') modal
  @Input() data;
  @Output() actualizar: EventEmitter<any> = new EventEmitter();
  detalleProducto={}
  constructor(
    private _modal: NgbModal,
    private conector: ConectorApiService   
  ) { 

    this.data={}

  }

  ngOnChanges(){
    if(this.data.idproducto!=undefined){
      this.obtenerDetalle();
    }
  }

  ngOnInit(): void {
    if(this.data.idproducto!=undefined){

    this.obtenerDetalle();
    
    }
  }

  close() {
    this._modal.dismissAll();
  }

  show(){
    this._modal.open(this.modal, { windowClass: 'animated fadeInDown',size: 'lg' })
  }

  obtenerDetalle(){

    this.conector.obtenerProductoDetalle(this.data).subscribe(
      (e: any)=>{
        if(e.ok){

          this.detalleProducto=e.data;

        }else{
          this.conector.mensajeError(e.msg);
        }

      }
    );    
  }

}
