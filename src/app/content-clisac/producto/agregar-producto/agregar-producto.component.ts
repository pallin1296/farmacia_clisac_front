import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, Output, OnInit, ViewChild,EventEmitter,Input } from '@angular/core';
import { ConectorApiService } from '../../../services/conector-api.service';
import swal from 'sweetalert2'

@Component({
  selector: 'app-agregar-producto',
  templateUrl: './agregar-producto.component.html',
  styleUrls: ['./agregar-producto.component.css']
})
export class AgregarProductoComponent implements OnInit {
  
  togglers: boolean = false;
  @Input() data
  @Input() edit:any[];
  submitted=false;
  NoAplica=false;
  errorCodigo="";
  codigo: string;
  listaProductos=[]
  @Output() actualizar: EventEmitter<any> = new EventEmitter();
  @ViewChild('NuevoProducto') modal
  public NuevoRegistro: FormGroup
  public administraciones: any[]

  folios: any = [];
  public categorias: any[]

  constructor(
    private _modal: NgbModal,
    private conector: ConectorApiService,
    private _builder: FormBuilder
  ) {
    this.data = {}
    this.NuevoRegistro = this._builder.group({
      codigo: ['',[Validators.required]],
      producto: ['',[Validators.required]],
      precioProv: ['',[Validators.required]],
      precioPub: ['',[Validators.required]],
      MinimoAlmacen: ['',[Validators.required]],
      precioCli: ['',[Validators.required]],
      existente: ['',[Validators.required]],
      caducidad: ['',[Validators.required]],
      lote: ['',[Validators.required]],
      administracion: [''],
      categoria: [''],
      pieza: [true],
    })

   }


  ngOnInit(): void {
    this.listaProductos=[]
    this.obtenerCategoria();
    this.crearAdministraciones();
    this.limpiarInput();

  }

  ngOnChanges(){
this.listaProductos=[]
this.obtenerCategoria();
this.crearAdministraciones();
this.limpiarInput();

  }
  limpiarInput(){

    this.submitted=false;
    this.errorCodigo="";
    this.fData.codigo.setValue(null); 
    this.fData.producto.setValue(null);
    this.fData.MinimoAlmacen.setValue("0");
    this.fData.existente.setValue("0");
    this.fData.precioProv.setValue("0");
    this.fData.precioCli.setValue("0");
    this.fData.precioPub.setValue("0");
    this.fData.lote.setValue(null);
    this.fData.caducidad.setValue(null);
    this.listaProductos=[];
  }

  close() {
    this.actualizar.emit();
    this._modal.dismissAll();
  }

  show(){
    this._modal.open(this.modal, { windowClass: 'animated fadeInDown',backdrop: 'static',size: 'lg', keyboard: false, centered: true })
  }

  get fData(){
    return this.NuevoRegistro.controls  
  }
 
  validarProducto(){

    this.submitted = true

    if(
      (!this.NoAplica &&
      ((this.fData.codigo.value==null || this.fData.codigo.value=="") || 
      (this.fData.precioProv.value==null || this.fData.precioProv.value=="") ||
      (this.fData.producto.value==null || this.fData.producto.value=="") ||
      (this.fData.precioCli.value==null || this.fData.precioCli.value=="") ||
      (this.fData.precioPub.value==null || this.fData.precioPub.value=="") ||
      (this.fData.categoria.value==undefined || this.fData.categoria.value=="") ||
      (this.fData.MinimoAlmacen.value==null || this.fData.MinimoAlmacen.value=="") ||
      (this.fData.existente.value==null || this.fData.existente.value=="") ||
      (this.fData.administracion.value==undefined || this.fData.administracion.value=="") ||
      (this.fData.lote.value==null || this.fData.lote.value=="") ||
      (this.fData.caducidad.value==null || this.fData.caducidad.value=="")))
      ||
      (this.NoAplica &&
        ((this.fData.codigo.value==null || this.fData.codigo.value=="") || 
        (this.fData.precioProv.value==null || this.fData.precioProv.value=="") ||
        (this.fData.producto.value==null || this.fData.producto.value=="") ||
        (this.fData.pieza.value==false && this.listaProductos.length==0) ||
        (this.fData.pieza.value==true && (this.fData.MinimoAlmacen.value==null || this.fData.MinimoAlmacen.value=="")) ||
        (this.fData.pieza.value==true && (this.fData.existente.value==null || this.fData.existente.value=="")) ||        
        (this.fData.precioCli.value==null || this.fData.precioCli.value=="") ||
        (this.fData.precioPub.value==null || this.fData.precioPub.value=="") ||
        (this.fData.categoria.value==undefined || this.fData.categoria.value=="")))      
      ){
      console.info("Error con los datos");
      return false
    }else{
      return true
    }

  }

guardarProducto(){

if(this.validarProducto()){
  let data={}
  if(!this.NoAplica){

    data={
      "codigo":this.fData.codigo.value,
      "producto":this.fData.producto.value,
      "precioProveedor":this.fData.precioProv.value,
      "precioCliente":this.fData.precioCli.value,
      "precioPublico":this.fData.precioPub.value,
      "idCategoria":this.fData.categoria.value,
      "pieza":this.fData.pieza.value,
      "minimoAlmacen":this.fData.MinimoAlmacen.value,
      "existente":this.fData.existente.value,
      "caducidad":this.fData.caducidad.value,
      "lote":this.fData.lote.value,
      "tipoadministracion":this.fData.administracion.value
    }

  }
  if(this.NoAplica){

    data={
      "codigo":this.fData.codigo.value,
      "producto":this.fData.producto.value,
      "precioProveedor":this.fData.precioProv.value,
      "precioCliente":this.fData.precioCli.value,
      "precioPublico":this.fData.precioPub.value,
      "idCategoria":this.fData.categoria.value,
      "pieza":this.fData.pieza.value,
      "minimoAlmacen":this.fData.MinimoAlmacen.value,
      "existente":this.fData.existente.value,
      "caducidad":null,
      "lote":null,
      "tipoadministracion":null,
      "listaProducto":this.listaProductos
    }
    
  }

  this.conector.guardarProducto(data).subscribe(
    (e: any)=>{
      if(e.ok){

        this.limpiarInput();
        this.conector.mensajeExitoso(e.msg);
        this.actualizar.emit();

      }else{
        if(e.msg=="Codigo existente."){
          this.errorCodigo=e.msg;
        }else{
          this.conector.mensajeError(e.msg);
        }
      }

    }
  );


}

}

obtenerCategoria(){
  this.conector.obtenerCategoriaActivas().subscribe(
    (e: any)=>{
      if(e.ok){

        this.categorias=e.data

      }else{
        this.conector.mensajeError(e.msg);
      }

    }
  );

}

crearAdministraciones(){

  this.administraciones=[
    {
      "idadministracion":"No aplica",
      "administracion":"No aplica"
    },
    {
      "idadministracion":"oral",
      "administracion":"Vía oral"
    },
    {
      "idadministracion":"intramuscular",
      "administracion":"Vía intramuscular"
    },
    {
      "idadministracion":"intravenosa",
      "administracion":"Vía intravenosa"
    },
    {
      "idadministracion":"subcutanea",
      "administracion":"Vía subcutánea"
    },
    {
      "idadministracion":"inhalatoria",
      "administracion":"Vía inhalatoria"
    },
    {
      "idadministracion":"transdermica",
      "administracion":"vía transdérmica"
    },
    {
      "idadministracion":"nasal",
      "administracion":"Vía nasal"
    },
    {
      "idadministracion":"oftalmica",
      "administracion":"Vía oftálmica"
    },
    {
      "idadministracion":"otica",
      "administracion":"Vía ótica"
    },
    {
      "idadministracion":"topica",
      "administracion":"Vía tópica"
    },
    {
      "idadministracion":"rectal",
      "administracion":"Vía rectal"
    },
    {
      "idadministracion":"vaginal",
      "administracion":"Vía vaginal"
    }
  ]

 }

 mostrarNoAplica(){
    
  if(this.NoAplica){
    this.NoAplica=false;
  }else{
    this.NoAplica=true;
  }

}

keyPress(){
if(this.fData.caducidad.value!=null && this.fData.caducidad.value!=""){

  if(this.fData.caducidad.value.length==2){
    this.fData.caducidad.setValue(this.fData.caducidad.value+"/")
 }
}

}

crearPaquete(){

if(!this.fData.pieza.value){
  this.NoAplica=true;
  this.listaProductos=[];
  this.fData.MinimoAlmacen.setValue("0");
  this.fData.existente.setValue("0");
  this.fData.precioProv.setValue("0");
  this.fData.precioCli.setValue("0");
  this.fData.precioPub.setValue("0");
}
  
}

async scanearProducto(){
   
  let codigo = this.codigo.trim();
  let cantidad = (document.getElementById("cantidad") as HTMLInputElement).value;
  
  let data=({
    "codigo":codigo,
    "cantidad":cantidad
  });

  this.agregarProducto(data);

}

agregarProducto(data) {
  
  if(data.codigo=="" || data.cantidad==""){
         
    return swal.fire({
      title: 'No se genero escaner',
      html: "Faltan datos importantes."
    });
   
  }
  if(data.cantidad=="0"){
         
    return swal.fire({
      title: 'No se genero escaner',
      html: "Debe ingresar una cantidad valida."
    }); 
}

  this.conector.seleccionarProducto(data).subscribe(
    (e: any)=>{

      if(e.ok){

        const indiceElemento = this.listaProductos.findIndex(el => el.idProducto == e.data.idProducto );

        if(indiceElemento!=-1){

          let newTodos =this.listaProductos;
          newTodos[indiceElemento] = {...newTodos[indiceElemento], cantidad: Number(newTodos[indiceElemento].cantidad)+Number(data.cantidad)}
          this.listaProductos=newTodos;

        }else{

          this.listaProductos.push({
            "num":this.listaProductos.length,
            "idProducto":e.data.idProducto,
            "producto":e.data.producto,
            "cantidad":data.cantidad,
            "precioProveedor":e.data.precioProveedor,
            "precioCliente":e.data.precioCliente,
            "precioPublico":e.data.precioPublico
          });

        }

        let totalPrecioCliente=0;
        let totalPrecioProveedor=0;
        let totalPrecioPublico=0;
        
        this.listaProductos.forEach(element => {
          
          totalPrecioCliente=Number(totalPrecioCliente)+(Number(element.cantidad)*Number(element.precioCliente));
          totalPrecioProveedor=Number(totalPrecioProveedor)+(Number(element.cantidad)*Number(element.precioProveedor));
          totalPrecioPublico=Number(totalPrecioPublico)+(Number(element.cantidad)*Number(element.precioPublico));

        });

        this.fData.precioProv.setValue(totalPrecioProveedor);
        this.fData.precioCli.setValue(totalPrecioCliente);
        this.fData.precioPub.setValue(totalPrecioPublico);      
      }

      this.listaProductos.sort((unaMascota, otraMascota) => otraMascota.num - unaMascota.num);


      this.codigo="";

    }
  );

} 

quitarProducto(id){

  var indice = this.listaProductos.findIndex(el => el.idProducto == id );
  this.listaProductos.splice(indice, 1);
  
}

}
