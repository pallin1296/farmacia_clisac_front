import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConectorApiService } from '../../../services/conector-api.service';

@Component({
  selector: 'app-eliminar-producto',
  templateUrl: './eliminar-producto.component.html',
  styleUrls: ['./eliminar-producto.component.css']
})
export class EliminarProductoComponent implements OnInit {
  titulo="";
  @Input() data;
  @Output() actualizar: EventEmitter<any> = new EventEmitter();
  @ViewChild("ModalEliminarProducto") defaultModel: NgbModal;

  constructor(private modalService: NgbModal,
    private conector: ConectorApiService) {    
      this.data={}
      
  }

  ngOnInit(): void {
  }

  ngOnChanges(){

    if(this.data.alta==1){
      this.titulo="Bajar Producto del Stock.";

    }
    if(this.data.alta==2){
      this.titulo="Devolver Producto al Stock.";

    } 
    
  }

  open() {
    this.modalService.open(this.defaultModel, { windowClass: 'animated fadeInDown',backdrop: 'static',size: 'sm', keyboard: false, centered: true });
  }

  close() {
    this.actualizar.emit();
    this.modalService.dismissAll();
  }

  eliminarProducto(){
    let estatus={}

    if(this.data.alta==1){
      estatus={
        "idproducto":this.data.idproducto,
        "alta":2
      }
    }

    if(this.data.alta==2){
      estatus={
        "idproducto":this.data.idproducto,
        "alta":1
      }
    }
    
    this.conector.estatusProducto(estatus).subscribe(
      (e: any)=>{
        if(e.ok){
          this.conector.mensajeExitoso(e.msg);
          this.close()

        }else{
          this.conector.mensajeError(e.msg);
        }

      }
    );

  }

}
