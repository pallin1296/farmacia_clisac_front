import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConectorApiService } from '../../../services/conector-api.service';

@Component({
  selector: 'app-eliminar-doctor',
  templateUrl: './eliminar-doctor.component.html',
  styleUrls: ['./eliminar-doctor.component.css']
})
export class EliminarDoctorComponent implements OnInit {

  titulo="";
  @Input() data;
  @Output() actualizar: EventEmitter<any> = new EventEmitter();
  @ViewChild("ModalEliminarDoctor") defaultModel: NgbModal;

  constructor(private modalService: NgbModal,
    private conector: ConectorApiService) {    
      this.data={}
      
  }

  ngOnInit(): void {
  }

  ngOnChanges(){

    if(this.data.alta==1){
      this.titulo="Bajar Doctor.";

    }
    if(this.data.alta==2){
      this.titulo="Alta Doctor.";

    } 
    
  }

  open() {
    this.modalService.open(this.defaultModel, { windowClass: 'animated fadeInDown',backdrop: 'static',size: 'sm', keyboard: false, centered: true });
  }

  close() {
    this.actualizar.emit();
    this.modalService.dismissAll();
  }

  eliminarDoctor(){
    let estatus={}

    if(this.data.alta==1){
      estatus={
        "idDoctor":this.data.idDoctor,
        "alta":2
      }
    }

    if(this.data.alta==2){
      estatus={
        "idDoctor":this.data.idDoctor,
        "alta":1
      }
    }
    
    this.conector.estatusDoctor(estatus).subscribe(
      (e: any)=>{
        if(e.ok){
          this.conector.mensajeExitoso(e.msg);
          this.close()

        }else{
          this.conector.mensajeError(e.msg);
        }

      }
    );

  }

}
