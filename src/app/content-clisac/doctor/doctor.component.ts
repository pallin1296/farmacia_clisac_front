import { Component, OnInit, ViewChild } from '@angular/core';
import { ConectorApiService } from '../../services/conector-api.service';
import { AgregarDoctorComponent } from '../modales-dinamicos/agregar-doctor/agregar-doctor.component';
import { EditarDoctorComponent } from '../doctor/editar-doctor/editar-doctor.component';
import { EliminarDoctorComponent } from '../doctor/eliminar-doctor/eliminar-doctor.component';


@Component({
  selector: 'app-doctor',
  templateUrl: './doctor.component.html',
  styleUrls: ['./doctor.component.css']
})
export class DoctorComponent implements OnInit {

  @ViewChild(AgregarDoctorComponent) agregarDoctor:AgregarDoctorComponent;
  @ViewChild(EditarDoctorComponent) editarDoctor:EditarDoctorComponent;
  @ViewChild(EliminarDoctorComponent) eliminarDoctor:EliminarDoctorComponent;

  contenido:any;
  master:any;
  public envioModalEditar:any;
  public envioModalEstatus:any;
  public opciones;
  public busqueda;

  permisos=JSON.parse(localStorage.getItem('permisos'));
  
  constructor(
    private conector: ConectorApiService
  ) {
    this.opciones="Todo";
    this.busqueda="";
    this.envioModalEditar={}
    this.envioModalEstatus={}
    this.actualizarListaDoctor();

   }

   ngOnInit(): void {

  }

  crearDoctor(){
    this.agregarDoctor.show();
  }
  openEditar(row){
    this.envioModalEditar=row;
    this.editarDoctor.show();
  }
  async actualizarListaDoctor(){
    this.conector.cargandoDatos();

    this.conector.obtenerTodosDoctores().subscribe(
      (e: any)=>{
        if(e.ok){

            this.contenido=e.data;   
            this.master=e.data;
            this.conector.cerrarCargandoDatos();

          }else{
            this.conector.cerrarCargandoDatos();
            this.conector.mensajeError(e.msg);

          }

      }
    );

  }

  estatusDoctor(row){
    this.envioModalEstatus={
      "idDoctor":row.idDoctor,
      "alta":row.alta
    }    
    this.eliminarDoctor.open();
  }

  doctorBusqueda(){

    this.contenido=[]
  
    if(this.opciones==undefined || this.opciones=='Todo'){
      if(this.busqueda!=""){
  
      this.master.forEach((x) => {
  
          if(x.doctor?.toLowerCase()?.includes(this.busqueda?.toLowerCase())){
            this.contenido.push(x)
          }
    
      });
    }else{
      this.contenido=this.master;
    }
  
    }else{
  
      if(this.opciones=='Activos'){
       
        this.master.forEach((x) => {
  
          if(this.busqueda!=""){
            if(x.doctor?.toLowerCase()?.includes(this.busqueda?.toLowerCase()) && x.alta==1){
              this.contenido.push(x)
            }
          }else{
            if(x.alta==1){
              this.contenido.push(x)
            }
          }
      
        });
  
      }
  
      if(this.opciones=='Inactivos'){
       
        this.master.forEach((x) => {
          
          if(this.busqueda!=""){
            if(x.doctor?.toLowerCase()?.includes(this.busqueda?.toLowerCase()) && x.alta==2){
              this.contenido.push(x)
            }
          }else{
            if(x.alta==2){
              this.contenido.push(x)
            }
          }
      
        });
  
      }
  
    }
  
  }

}
