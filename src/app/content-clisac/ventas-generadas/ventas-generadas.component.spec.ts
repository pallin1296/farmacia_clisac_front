import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VentasGeneradasComponent } from './ventas-generadas.component';

describe('VentasGeneradasComponent', () => {
  let component: VentasGeneradasComponent;
  let fixture: ComponentFixture<VentasGeneradasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VentasGeneradasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VentasGeneradasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
