import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ConectorApiService } from '../../services/conector-api.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-ventas-generadas',
  templateUrl: './ventas-generadas.component.html',
  styleUrls: ['./ventas-generadas.component.css']
})
export class VentasGeneradasComponent implements OnInit {
  
  public fechaInicial=this.conector.formatoFechaAnioMesDia(new Date(),"-");
  public fechaFinal=this.conector.formatoFechaAnioMesDia(new Date(),"-");
  contenido=[]
  detalleVenta={}

  @ViewChild('DetalleVenta') ModalDetalleVenta

  constructor(
    private modalService: NgbModal,
    private conector: ConectorApiService, 
    private fb: FormBuilder) {
    
    this.obtenerVentas();
   }

  ngOnInit(): void {
  }

  obtenerVentas(){
    let data={
      inicio:this.fechaInicial,
      fin:this.fechaFinal
    }
    this.conector.cargandoDatos();

    this.conector.obtenerTodasLasVentas(data).subscribe(
      (e: any)=>{
        if(e.ok){

          this.conector.cerrarCargandoDatos();
          this.contenido=e.data
          console.info(this.contenido);

        }else{
          this.conector.cerrarCargandoDatos();
        }


      }
    );

  }

  cambiarFecha(){

    this.obtenerVentas();
  
}

imprimirTicket(row){
  this.conector.ticket58mm(row);
}

abrirModalDetalleVenta(row){
  this.detalleVenta=row;
  this.modalService.open(this.ModalDetalleVenta, { windowClass: 'animated fadeInDown',size:'lg' });
}

}
