import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, Output, OnInit, ViewChild,EventEmitter,Input } from '@angular/core';
import { ConectorApiService } from '../../../services/conector-api.service';
@Component({
  selector: 'app-editar-tipo-cirugia',
  templateUrl: './editar-tipo-cirugia.component.html',
  styleUrls: ['./editar-tipo-cirugia.component.css']
})
export class EditarTipoCirugiaComponent implements OnInit {

  @Input() data
  @Input() edit:any[];
  submitted=false;
  @Output() actualizar: EventEmitter<any> = new EventEmitter();
  @ViewChild('EditarTipoCirugia') modal
  public NuevoRegistro: FormGroup

  constructor(
    private _modal: NgbModal,
    private conector: ConectorApiService,
    private _builder: FormBuilder
  ) {
    this.data = {}
    this.NuevoRegistro = this._builder.group({
      tipoCirugia: ['',[Validators.required]]
    })

   }

   get fData(){
    return this.NuevoRegistro.controls  
  }

  limpiarInput(){

    this.submitted=false;
    this.fData.tipoCirugia.setValue(""); 

  }

  validarTipoCirugia(){

    this.submitted = true

    if(this.fData.tipoCirugia.value==null || this.fData.tipoCirugia.value==""){
      return false
    }else{
      return true
    }

    }

    ngOnInit(): void {
    }
  
    ngOnChanges(){
      this.enviarDatos();
            
    }
    enviarDatos(){
      this.fData.tipoCirugia.setValue(this.data.tipoCirugia); 
  
    }

  close() {
    this.actualizar.emit();
    this._modal.dismissAll();
  }

  show(){
    this._modal.open(this.modal, { windowClass: 'animated fadeInDown',backdrop: 'static',size: 'lg', keyboard: false, centered: true })
  }

  editarTipoCirugia(){

    if(this.validarTipoCirugia()){
      let data={}

      data={
        "tipoCirugia":this.fData.tipoCirugia.value,
        "idTipoCirugia":this.data.idTipoCirugia
      }

      this.conector.editarTipoCirugia(data).subscribe(
        (e: any)=>{
          if(e.ok){
    
            this.limpiarInput();
            this.conector.mensajeExitoso(e.msg);
            this.close();
    
          }else{
            this.conector.mensajeError(e.msg);
          }
    
        }
      );


    }

    }


}
