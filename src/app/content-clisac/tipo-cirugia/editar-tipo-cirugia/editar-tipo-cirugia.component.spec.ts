import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarTipoCirugiaComponent } from './editar-tipo-cirugia.component';

describe('EditarTipoCirugiaComponent', () => {
  let component: EditarTipoCirugiaComponent;
  let fixture: ComponentFixture<EditarTipoCirugiaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditarTipoCirugiaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarTipoCirugiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
