import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EliminarTipoCirugiaComponent } from './eliminar-tipo-cirugia.component';

describe('EliminarTipoCirugiaComponent', () => {
  let component: EliminarTipoCirugiaComponent;
  let fixture: ComponentFixture<EliminarTipoCirugiaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EliminarTipoCirugiaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EliminarTipoCirugiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
