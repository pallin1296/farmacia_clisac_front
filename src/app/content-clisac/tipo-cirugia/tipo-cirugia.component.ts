import { Component, OnInit, ViewChild } from '@angular/core';
import { ConectorApiService } from '../../services/conector-api.service';
import { AgregarTipoCirugiaComponent } from '../modales-dinamicos/agregar-tipo-cirugia/agregar-tipo-cirugia.component';
import { EditarTipoCirugiaComponent } from '../tipo-cirugia/editar-tipo-cirugia/editar-tipo-cirugia.component';

@Component({
  selector: 'app-tipo-cirugia',
  templateUrl: './tipo-cirugia.component.html',
  styleUrls: ['./tipo-cirugia.component.css']
})
export class TipoCirugiaComponent implements OnInit {

  @ViewChild(AgregarTipoCirugiaComponent) agregarCirugia:AgregarTipoCirugiaComponent;
  @ViewChild(EditarTipoCirugiaComponent) editarTipoCirugia:EditarTipoCirugiaComponent;

  contenido:any;
  master:any;
  public envioModalEditar:any;
  public busqueda;
  permisos=JSON.parse(localStorage.getItem('permisos'));

  constructor(
    private conector: ConectorApiService
  ) {
    this.envioModalEditar={}
    this.actualizarListaTipoCirugia();

   }

   ngOnInit(): void {

  }

  crearTipoCirugia(){
    this.agregarCirugia.show();
  }
  openEditar(row){
    this.envioModalEditar=row;
    this.editarTipoCirugia.show();
  }
  async actualizarListaTipoCirugia(){
    this.conector.cargandoDatos();

    this.conector.obtenerTodosTiposCirugia().subscribe(
      (e: any)=>{
        if(e.ok){

            this.contenido=e.data;
            this.master=e.data;   
            this.conector.cerrarCargandoDatos();

        }else{
          this.conector.cerrarCargandoDatos();

        }


      }
    );

  }

  tipoCirugiaBusqueda(){

    this.contenido=[]
    this.master.forEach((x) => {

      if(x.tipoCirugia?.toLowerCase()?.includes(this.busqueda?.toLowerCase())){
        this.contenido.push(x)
      }

    });

    if(this.busqueda=="" || this.busqueda==null){
      this.contenido=this.master;
    }

  }

}
