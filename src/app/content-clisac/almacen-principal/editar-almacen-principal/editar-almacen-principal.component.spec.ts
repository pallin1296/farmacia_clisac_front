import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarAlmacenPrincipalComponent } from './editar-almacen-principal.component';

describe('EditarAlmacenPrincipalComponent', () => {
  let component: EditarAlmacenPrincipalComponent;
  let fixture: ComponentFixture<EditarAlmacenPrincipalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditarAlmacenPrincipalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarAlmacenPrincipalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
