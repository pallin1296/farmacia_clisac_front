import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConectorApiService } from '../../../services/conector-api.service';

@Component({
  selector: 'app-ajustar-producto-almacen-principal',
  templateUrl: './ajustar-producto-almacen-principal.component.html',
  styleUrls: ['./ajustar-producto-almacen-principal.component.css']
})
export class AjustarProductoAlmacenPrincipalComponent implements OnInit {
  
  titulo="";
  @Input() data;
  @Output() actualizar: EventEmitter<any> = new EventEmitter();
  @ViewChild("ModalAjustarProducto") defaultModel: NgbModal;
  buscarNombre=false;
  codigo: string;
  cantidad:string;
  producto="";
  contenido:any;

  existente="";
  obtenido=false;

  constructor(private modalService: NgbModal,
    private conector: ConectorApiService) {    
      this.data={}    
   }

   async scanearProducto(){
     
    this.codigo = this.codigo.trim();
    this.agregarProducto();

  }

  agregarProducto(){

    this.consultarProducto({"codigo":this.codigo});
    
  }

  ngOnInit(): void {
    this.listarProducto();
    this.buscarNombre=false;
    this.limpiar();
  }

  ngOnChanges(){
    this.buscarNombre=false;
    this.limpiar();
  }

  limpiar(){
    this.codigo="";
    this.cantidad="";
    this.obtenido=false;
    
  }

  open() {
    this.modalService.open(this.defaultModel, { windowClass: 'animated fadeInDown',backdrop: 'static',size: 'lg', keyboard: false, centered: true });
    this.limpiar();
  }

  close() {
    this.actualizar.emit();
    this.modalService.dismissAll();
  }

  consultarProducto(data){

    this.conector.consultarProductoAlmacenPrincipal(data).subscribe(
      (e: any)=>{
        if(e.ok){
  
          this.producto=e.data.producto;
          this.existente=e.data.existente;
          this.obtenido=true;
      
          (document.getElementById("codigo") as HTMLInputElement).value="";
          
        }else{
          this.conector.mensajeError(e.msg);
          (document.getElementById("codigo") as HTMLInputElement).value="";
          (document.getElementById("cantidad") as HTMLInputElement).value="";          
          this.obtenido=false;

        }
  
      }
    );

  }

  ajustarCantidadProducto(){

    let cantidad=(document.getElementById("cantidad") as HTMLInputElement).value;
    
    let data={
      "cantidad":cantidad,
      "codigo":this.codigo
    };
   
    this.conector.ajustarProductoAlmacenPrincipal(data).subscribe(
      (e: any)=>{
        if(e.ok){
        
          (document.getElementById("cantidad") as HTMLInputElement).value="";
          this.actualizar.emit();
          this.agregarProducto();

        }
  
      }
    );

  }

  cambiarNombre(){
    this.buscarNombre=!this.buscarNombre;
  }

  listarProducto(){
    this.conector.obtenerTodosProductos().subscribe(
      (e: any)=>{
        if(e.ok){

            this.contenido=e.data;   
  
        }

      }
    );

  }

}
