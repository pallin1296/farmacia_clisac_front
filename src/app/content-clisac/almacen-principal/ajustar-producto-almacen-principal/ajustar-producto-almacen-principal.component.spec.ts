import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AjustarProductoAlmacenPrincipalComponent } from './ajustar-producto-almacen-principal.component';

describe('AjustarProductoAlmacenPrincipalComponent', () => {
  let component: AjustarProductoAlmacenPrincipalComponent;
  let fixture: ComponentFixture<AjustarProductoAlmacenPrincipalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AjustarProductoAlmacenPrincipalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AjustarProductoAlmacenPrincipalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
