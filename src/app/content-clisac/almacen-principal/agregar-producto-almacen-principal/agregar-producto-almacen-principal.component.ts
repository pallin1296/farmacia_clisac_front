import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConectorApiService } from '../../../services/conector-api.service';

@Component({
  selector: 'app-agregar-producto-almacen-principal',
  templateUrl: './agregar-producto-almacen-principal.component.html',
  styleUrls: ['./agregar-producto-almacen-principal.component.css']
})
export class AgregarProductoAlmacenPrincipalComponent implements OnInit {

  titulo = "";
  @Input() data;
  @Output() actualizar: EventEmitter<any> = new EventEmitter();
  @ViewChild("ModalAgregarProducto") defaultModel: NgbModal;
  codigo: string;
  cantidad: string;
  producto = "";
  existente = "";
  obtenido = false;
  buscarNombre=false;
  contenido:any;
  constructor(private modalService: NgbModal,
    private conector: ConectorApiService) {
    this.data = {}
  }

  ngOnInit(): void {
    this.listarProducto();
    this.limpiar();
  }

  ngOnChanges() {
    this.limpiar();
  }

  limpiar() {
    this.codigo = "";
    this.cantidad = "";
    this.obtenido = false;

  }

  async scanearProducto() {
    this.codigo = this.codigo.trim();

    this.agregarProducto();

  }

  agregarProducto() {

    this.consultarProducto({ "codigo": this.codigo });

  }

  open() {
    this.modalService.open(this.defaultModel, { windowClass: 'animated fadeInDown', backdrop: 'static', size: 'lg', keyboard: false, centered: true });
    this.limpiar();
  }

  close() {
    this.modalService.dismissAll();
  }

  consultarProducto(data) {

    this.conector.consultarProductoAlmacenPrincipal(data).subscribe(
      (e: any) => {
        if (e.ok) {

          this.producto = e.data.producto;
          this.existente = e.data.existente;
          this.obtenido = true;

          (document.getElementById("codigo") as HTMLInputElement).value = "";

        } else {
          this.conector.mensajeError(e.msg);
          this.obtenido = false;

        }

      }
    );

  }

  agregarCantidadProducto() {

    let cantidad = (document.getElementById("cantidad") as HTMLInputElement).value;

    let data = {
      "cantidad": cantidad,
      "codigo": this.codigo
    };

    this.conector.agregarProductoAlmacenPrincipal(data).subscribe(
      (e: any) => {
        if (e.ok) {

          (document.getElementById("cantidad") as HTMLInputElement).value = "";
          this.actualizar.emit();
          this.agregarProducto();

        }

      }
    );

  }

  cambiarNombre(){
    this.buscarNombre=!this.buscarNombre;
  }
  listarProducto(){
    this.conector.obtenerTodosProductos().subscribe(
      (e: any)=>{
        if(e.ok){

            this.contenido=e.data;   
  
        }

      }
    );

  }

}
