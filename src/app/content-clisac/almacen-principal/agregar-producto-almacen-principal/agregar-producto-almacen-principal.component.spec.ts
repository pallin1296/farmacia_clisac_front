import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarProductoAlmacenPrincipalComponent } from './agregar-producto-almacen-principal.component';

describe('AgregarProductoAlmacenPrincipalComponent', () => {
  let component: AgregarProductoAlmacenPrincipalComponent;
  let fixture: ComponentFixture<AgregarProductoAlmacenPrincipalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgregarProductoAlmacenPrincipalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarProductoAlmacenPrincipalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
