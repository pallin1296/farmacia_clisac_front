import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlmacenPrincipalComponent } from './almacen-principal.component';

describe('AlmacenPrincipalComponent', () => {
  let component: AlmacenPrincipalComponent;
  let fixture: ComponentFixture<AlmacenPrincipalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AlmacenPrincipalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlmacenPrincipalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
