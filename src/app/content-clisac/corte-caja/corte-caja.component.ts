import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ConectorApiService } from '../../services/conector-api.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup,Validators,FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-corte-caja',
  templateUrl: './corte-caja.component.html',
  styleUrls: ['./corte-caja.component.css']
})
export class CorteCajaComponent implements OnInit {
  detalleCorte={
    "importeinicial":0,
    "entrada":0,
    "salida":0,
    "devolucion":0,
    "ventaefectivo":0,
    "ventatarjeta":0,
    "ganancia":0,
    "corteesperado":0
  }
  titulo=""
  informacionCorte:any

  @ViewChild('InformacionDevolucion') ModalInformacionDevolucion
  @ViewChild('InformacionVenta') ModalInformacionVenta
  @ViewChild('InformacionMovimiento') ModalInformacionMovimiento
  @ViewChild('GenerarCorte') ModalGenerarCorte
 
  frmCorte: FormGroup;

  constructor(
    private router: Router,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private conector: ConectorApiService    
  ) { 

    this.obtenerCorte()

  }

  ngOnInit(): void {

    this.frmCorte = this.formBuilder.group({
      cantidad: [null, Validators.required],
      comentario: ['']
    });  

  }

  get fDataCorte(){
    return this.frmCorte.controls  
  }

  obtenerCorte() {

    this.conector.obtenerInformacionCorte().subscribe(
      (e: any) => {
        if(e.ok){

          this.detalleCorte=e.data

        }else{
          this.conector.mensajeError(e.msg)
        }
      });

  }

  informacionDevolucion(){
    let data={
      idCorte:this.detalleCorte['idcortecaja']
    }

    this.titulo="Información de Salidas por Devolución"

    this.conector.obtenerDevolucionesCaja(data).subscribe(
      (e: any) => {
        if(e.ok){

          this.informacionCorte=e.data
          
          if(this.informacionCorte.length>0){
            this.abrirModalInformacionDevolucion();
          }else{
            this.conector.mensajeErroneo("No hay información");
          }

        }else{
          this.conector.mensajeError(e.msg)
        }
      });

  }

  informacionMovimientos(movimiento){
    let data={
      idCorte:this.detalleCorte['idcortecaja'],
      movimiento:movimiento
    }

    if(movimiento=='entrada'){
      this.titulo="Informacion de Entradas"
    }

    if(movimiento=='salida'){
      this.titulo="Información de Salidas"
    }

    this.conector.obtenerMovimientosCaja(data).subscribe(
      (e: any) => {
        if(e.ok){

          this.informacionCorte=e.data

          if(this.informacionCorte.length>0){
            this.abrirModalInformacionMovimiento();
          }else{
            this.conector.mensajeErroneo("No hay información");
          }

        }else{
          this.conector.mensajeError(e.msg)
        }
      });

  }

  informacionVentas(movimiento){
    let data={
      idCorte:this.detalleCorte['idcortecaja'],
      movimiento:movimiento
    }

    if(movimiento=='efectivo'){
      this.titulo="Información de Entradas en Efectivo"
    }

    if(movimiento=='tarjeta' || movimiento=='transferencia'){
      this.titulo="Información de Entradas por Tarjeta Y Transferencias"
    }

    this.conector.obtenerVentasCaja(data).subscribe(
      (e: any) => {
        if(e.ok){

          this.informacionCorte=e.data

          if(this.informacionCorte.length>0){
            this.abrirModalInformacionVenta();
          }else{
            this.conector.mensajeErroneo("No hay información");
          }

        }else{
          this.conector.mensajeError(e.msg)
        }
      });

  }

  validarFormularioCaja(){

    if(this.fDataCorte.cantidad.value=="" || this.fDataCorte.cantidad.value==null){
      return false
    }else{
      return true
    }

  }

  finalizarCorte(){

    if(!this.validarFormularioCaja()){
      return
    }

    let data={
      idCorte:this.detalleCorte['idcortecaja'],
      cantidad:this.fDataCorte.cantidad.value,
      comentario:this.fDataCorte.comentario.value
    }

    this.conector.finalizarCorteCaja(data).subscribe(
      (e: any) => {
        if(e.ok){

          this.conector.corte58mm(e.data);
          this.cerrarSesion();

        }else{
          this.conector.mensajeError(e.msg)
        }
      });

  }

  abrirModalInformacionVenta(){
    this.modalService.open(this.ModalInformacionVenta, { windowClass: 'animated fadeInDown',size:'lg' });
  }

  abrirModalInformacionDevolucion(){
    this.modalService.open(this.ModalInformacionDevolucion, { windowClass: 'animated fadeInDown',size:'lg' });
  }

  abrirModalInformacionMovimiento(){
    this.modalService.open(this.ModalInformacionMovimiento, { windowClass: 'animated fadeInDown',size:'lg' });
  }

  abrirModalGenerarCorte(){
    this.modalService.open(this.ModalGenerarCorte, { windowClass: 'animated fadeInDown',size:'lg' });
  }

  cerrarSesion(){
    this.modalService.dismissAll();
    this.router.navigate(['/login']);
  }

}
