import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VentasRealizadasAlmacenSecundarioComponent } from './ventas-realizadas-almacen-secundario.component';

describe('VentasRealizadasAlmacenSecundarioComponent', () => {
  let component: VentasRealizadasAlmacenSecundarioComponent;
  let fixture: ComponentFixture<VentasRealizadasAlmacenSecundarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VentasRealizadasAlmacenSecundarioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VentasRealizadasAlmacenSecundarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
