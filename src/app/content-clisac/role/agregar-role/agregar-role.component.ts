import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, Output, OnInit, ViewChild,EventEmitter,Input } from '@angular/core';
import { ConectorApiService } from '../../../services/conector-api.service';

@Component({
  selector: 'app-agregar-role',
  templateUrl: './agregar-role.component.html',
  styleUrls: ['./agregar-role.component.css']
})
export class AgregarRoleComponent implements OnInit {

  @Output() actualizar: EventEmitter<any> = new EventEmitter();
  @ViewChild('NuevaRole') modal
  public NuevoRegistro: FormGroup
  almacenes:any;
  submitted=false;
  errorCodigo="";
  almacenElegido=true;
  public agregarproducto=true;
  public editarproducto=true;
  public estatusproducto=true;
  public agregarcategoria=true;
  public editarcategoria=true;
  public estatuscategoria=true;
  public agregarempleado=true;
  public editarempleado=true;
  public estatusempleado=true;
  public agregarpaciente=true;
  public editarpaciente=true;
  public agregardoctor=true;
  public editardoctor=true;
  public estatusdoctor=true;
  public agregaralmacenprincipal=true;
  public ajustaralmacenprincipal=true;
  public editaralmacenprincipal=true;
  public crearalmacengenerarlista=true;
  public generargenerarlista=true;
  public estatuslistagenerada=true;
  public agregaralmacensecundario=true;
  public ajustaralmacensecundario=true;
  public editaralmacensecundario=true;
  public tipogenerarsalida=true;
  public pacientegenerarsalida=true;
  public doctorgenerarsalida=true;
  public cancelarsalida=true;
  public listageneradasalida=true;
  public agregartipocirugia=true;
  public editartipocirugia=true;
  public estatustipocirugia=true;
  public agregarrole=true;
  public editarrole=true;
  public reportealmacenprincipal=true;
  public reportealmacensecundario=true;
  public ventapacienteventa=true;
  public ventapacienteventarealizada=true;
  public agregaralmacen=true;
  public editaralmacen=true;
  public estatusalmacen=true;
  public ventagenerarventas=true;
  public ventaventasgeneradas=true;
  public ventacortecaja=true;
  public ventacortecajagenerados=true;

  constructor(
    private _modal: NgbModal,
    private conector: ConectorApiService,
    private _builder: FormBuilder    
  ) { 

    this.almacenes=[]
    this.NuevoRegistro = this._builder.group({
      role: ['',[Validators.required]],
      almacen: [null]

    }) 
    this.listarAlmacen();

  }

  ngOnInit(): void {
  }

  close() {
    this.actualizar.emit();
    this._modal.dismissAll();
  }

  show(){
    this._modal.open(this.modal, { windowClass: 'animated fadeInDown',backdrop: 'static',size: 'xl', keyboard: false, centered: true })
  }

  get fData(){
    return this.NuevoRegistro.controls  
  }

  limpiarInput(){

    this.submitted=false;
    this.fData.role.setValue(null); 

  }

  validarRole(){

    this.submitted = true

    if(
      this.fData.role.value==null || this.fData.role.value==""
      ){
      console.info("Error con los datos");
      return false
    }else{
      return true
    }

  }

  guardarRole(){

    if(this.validarRole()){
      let agregarproducto=0;
      let editarproducto=0;
      let estatusproducto=0;
      let agregarcategoria=0;
      let editarcategoria=0;
      let estatuscategoria=0;
      let agregarempleado=0;
      let editarempleado=0;
      let estatusempleado=0;
      let agregarpaciente=0;
      let editarpaciente=0;
      let agregardoctor=0;
      let editardoctor=0;
      let estatusdoctor=0;
      let agregaralmacenprincipal=0;
      let ajustaralmacenprincipal=0;
      let editaralmacenprincipal=0;
      let crearalmacengenerarlista=0;
      let generargenerarlista=0;
      let estatuslistagenerada=0;
      let agregaralmacensecundario=0;
      let ajustaralmacensecundario=0;
      let editaralmacensecundario=0;
      let tipogenerarsalida=0;
      let pacientegenerarsalida=0;
      let doctorgenerarsalida=0;
      let cancelarsalida=0;
      let listageneradasalida=0;
      let agregartipocirugia=0;
      let editartipocirugia=0;
      let estatustipocirugia=0;
      let agregarrole=0;
      let editarrole=0; 
      let reportealmacenprincipal=0;
      let reportealmacensecundario=0;
      let ventapacienteventa=0;
      let ventapacienteventarealizada=0;
      let agregaralmacen=0;
      let editaralmacen=0;
      let estatusalmacen=0;
      let ventagenerarventas=0;
      let ventaventasgeneradas=0;
      let ventacortecaja=0;
      let ventacortecajagenerados=0;

      
      if(this.agregarproducto){
        agregarproducto=1;
      }
      if(this.editarproducto){
        editarproducto=1;
      }
      if(this.estatusproducto){
        estatusproducto=1;
      }
      if(this.agregarcategoria){
        agregarcategoria=1;
      }
      if(this.editarcategoria){
        editarcategoria=1;
      }
      if(this.estatuscategoria){
        estatuscategoria=1;
      }
      if(this.agregarempleado){
        agregarempleado=1;
      }
      if(this.editarempleado){
        editarempleado=1;
      }
      if(this.estatusempleado){
        estatusempleado=1;
      }
      if(this.agregarpaciente){
        agregarpaciente=1;
      }
      if(this.editarpaciente){
        editarpaciente=1;
      }
      if(this.agregardoctor){
        agregardoctor=1;
      }
      if(this.editardoctor){
        editardoctor=1;
      }
      if(this.estatusdoctor){
        estatusdoctor=1;
      }
      if(this.agregaralmacenprincipal){
        agregaralmacenprincipal=1;
      }
      if(this.ajustaralmacenprincipal){
        ajustaralmacenprincipal=1;
      }
      if(this.editaralmacenprincipal){
        editaralmacenprincipal=1;
      }
      if(this.crearalmacengenerarlista){
        crearalmacengenerarlista=1;
      }
      if(this.generargenerarlista){
        generargenerarlista=1;
      }
      if(this.estatuslistagenerada){
        estatuslistagenerada=1;
      }
      if(this.agregaralmacensecundario){
        agregaralmacensecundario=1;
      }
      if(this.ajustaralmacensecundario){
        ajustaralmacensecundario=1;
      }
      if(this.editaralmacensecundario){
        editaralmacensecundario=1;
      }
      if(this.tipogenerarsalida){
        tipogenerarsalida=1;
      }
      if(this.pacientegenerarsalida){
        pacientegenerarsalida=1;
      }
      if(this.doctorgenerarsalida){
        doctorgenerarsalida=1;
      }
      if(this.cancelarsalida){
        cancelarsalida=1;
      }
      if(this.listageneradasalida){
        listageneradasalida=1;
      }
      if(this.agregartipocirugia){
        agregartipocirugia=1;
      }
      if(this.editartipocirugia){
        editartipocirugia=1;
      }
      if(this.estatustipocirugia){
        estatustipocirugia=1;
      }
      if(this.agregarrole){
        agregarrole=1;
      }
      if(this.editarrole){
        editarrole=1;
      }
      if(this.reportealmacenprincipal){
        reportealmacenprincipal=1;
      }
      if(this.reportealmacensecundario){
        reportealmacensecundario=1;
      }
      if(this.ventapacienteventa){
        ventapacienteventa=1;
      }
      if(this.ventapacienteventarealizada){
        ventapacienteventarealizada=1;
      }
      if(this.agregaralmacen){
        agregaralmacen=1;
      }
      if(this.editaralmacen){
        editaralmacen=1;
      }
      if(this.estatusalmacen){
        estatusalmacen=1;
      }
      if(this.ventagenerarventas){
        ventagenerarventas=1;
      }
      if(this.ventaventasgeneradas){
        ventaventasgeneradas=1;
      }
      if(this.ventacortecaja){
        ventacortecaja=1;
      }
      if(this.ventacortecajagenerados){
        ventacortecajagenerados=1;
      }
      
      let data={
        "role":this.fData.role.value,
        "almacen":this.fData.almacen.value,
        "agregarproducto":agregarproducto,
        "editarproducto":editarproducto,
        "estatusproducto":estatusproducto,
        "agregarcategoria":agregarcategoria,
        "editarcategoria":  editarcategoria,
        "estatuscategoria": estatuscategoria,
        "agregarempleado": agregarempleado,
        "editarempleado": editarempleado,
        "estatusempleado": estatusempleado,
        "agregarpaciente": agregarpaciente,
        "editarpaciente": editarpaciente,
        "agregardoctor": agregardoctor,
        "editardoctor": editardoctor,
        "estatusdoctor": estatusdoctor,
        "agregaralmacenprincipal": agregaralmacenprincipal,
        "ajustaralmacenprincipal": ajustaralmacenprincipal,
        "editaralmacenprincipal": editaralmacenprincipal,
        "crearalmacengenerarlista": crearalmacengenerarlista,
        "generargenerarlista": generargenerarlista,
        "estatuslistagenerada": estatuslistagenerada,
        "agregaralmacensecundario": agregaralmacensecundario,
        "ajustaralmacensecundario": ajustaralmacensecundario,
        "editaralmacensecundario": editaralmacensecundario,
        "tipogenerarsalida": tipogenerarsalida,
        "pacientegenerarsalida": pacientegenerarsalida,
        "doctorgenerarsalida": doctorgenerarsalida,
        "cancelarsalida": cancelarsalida,
        "listageneradasalida": listageneradasalida,
        "agregartipocirugia": agregartipocirugia,
        "editartipocirugia": editartipocirugia,
        "estatustipocirugia": estatustipocirugia,
        "agregarrole": agregarrole,
        "editarrole": editarrole,
        "reportealmacenprincipal": reportealmacenprincipal,
        "reportealmacensecundario": reportealmacensecundario,
        "ventapacienteventa": ventapacienteventa,
        "ventapacienteventarealizada": ventapacienteventarealizada,
        "agregaralmacen":agregaralmacen,
        "editaralmacen":  editaralmacen,
        "estatusalmacen": estatusalmacen,
        "ventagenerarventas":ventagenerarventas,
        "ventaventasgeneradas":ventaventasgeneradas,
        "ventacortecaja":ventacortecaja,
        "ventacortecajagenerados":ventacortecajagenerados
      }

      this.conector.guardarRole(data).subscribe(
        (e: any)=>{
          if(e.ok){
    
            this.limpiarInput();
            this.conector.mensajeExitoso(e.msg);
            this.actualizar.emit();
    
          }else{
            this.conector.mensajeError(e.msg);
          }
    
        }
      );
    
    
    }
    
    }

    listarAlmacen(){
      this.conector.obtenerTodosAlmacenesActivos().subscribe(
        (e: any)=>{
          if(e.ok){
  
              this.almacenes=e.data;   

            }
  
  
        }
      );
  
    } 

    cambiarAlmacen(){

    if(this.fData.almacen.value!=null){
      this.almacenElegido=false;
      this.cambiarValores(false);
    }else{
      this.almacenElegido=true;
      this.cambiarValores(true);
    }

  }

  cambiarValores(valor){

    this.agregarproducto=valor;
    this.editarproducto=valor;
    this.estatusproducto=valor;
    this.agregarcategoria=valor;
    this.editarcategoria=valor;
    this.estatuscategoria=valor;
    this.agregarempleado=valor;
    this.editarempleado=valor;
    this.estatusempleado=valor;
    this.agregarpaciente=valor;
    this.editarpaciente=valor;
    this.agregardoctor=valor;
    this.editardoctor=valor;
    this.estatusdoctor=valor;
    this.agregaralmacenprincipal=valor;
    this.ajustaralmacenprincipal=valor;
    this.editaralmacenprincipal=valor;
    this.crearalmacengenerarlista=valor;
    this.generargenerarlista=valor;
    this.estatuslistagenerada=valor;
    this.agregartipocirugia=valor;
    this.editartipocirugia=valor;
    this.estatustipocirugia=valor;
    this.agregarrole=valor;
    this.editarrole=valor;
    this.reportealmacenprincipal=valor;
    this.ventapacienteventa=valor;
    this.ventapacienteventarealizada=valor;
    this.ventagenerarventas=valor;
    this.ventaventasgeneradas=valor;
    this.ventacortecaja=valor;
    this.ventacortecajagenerados=valor;
  }

  cambiarStatusSwitch(btnS){

    if(btnS=="generarVenta"){

      if(this.ventagenerarventas){
        this.ventacortecaja=true;
        this.ventagenerarventas=true;
      }else{
        this.ventacortecaja=false;
        this.ventagenerarventas=false;
      }

    }

    if(btnS=="corteCaja"){

      if(this.ventacortecaja){
        this.ventacortecaja=true;
        this.ventagenerarventas=true;
      }else{
        this.ventacortecaja=false;
        this.ventagenerarventas=false;
      }      
    }

  }

}
