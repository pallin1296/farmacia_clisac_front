import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarRoleComponent } from './agregar-role.component';

describe('AgregarRoleComponent', () => {
  let component: AgregarRoleComponent;
  let fixture: ComponentFixture<AgregarRoleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgregarRoleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
