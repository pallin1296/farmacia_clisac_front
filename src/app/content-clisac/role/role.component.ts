import { Component, OnInit, ViewChild } from '@angular/core';
import { ConectorApiService } from '../../services/conector-api.service';
import { AgregarRoleComponent } from './agregar-role/agregar-role.component'
import { EditarRoleComponent } from './editar-role/editar-role.component'
@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css']
})
export class RoleComponent implements OnInit {

  @ViewChild(EditarRoleComponent) editarRole: EditarRoleComponent
  @ViewChild(AgregarRoleComponent) crearRole: AgregarRoleComponent
  
  contenido:any;
  master:any;

  public busqueda;
  public envioModalEditar:any;
  public envioModalEstatus:any;
  
  permisos=JSON.parse(localStorage.getItem('permisos'));

  constructor(
    private conector: ConectorApiService
  ) {
    this.busqueda="";
    this.envioModalEditar={}
    this.envioModalEstatus={}
    this.actualizarListaRole();

   }

   ngOnInit(): void {

  }

ngOnChanges(){
  this.actualizarListaRole();
}

openCrear(){
  this.crearRole.show();
}

openEditar(row){
  this.envioModalEditar=row;
  this.editarRole.show();
}

async actualizarListaRole(){
  this.conector.cargandoDatos();

  this.conector.obtenerRole().subscribe(
    (e: any)=>{
      if(e.ok){

        this.contenido=e.data
        this.master=e.data;
        this.conector.cerrarCargandoDatos();
      }else{
        this.conector.mensajeError(e.msg);
        this.conector.cerrarCargandoDatos();
      }

    }
  );
}

roleBusqueda(){

  this.contenido=[]
  this.master.forEach((x) => {

    if(x.role?.toLowerCase()?.includes(this.busqueda?.toLowerCase())){
      this.contenido.push(x)
    }

  });

  if(this.busqueda=="" || this.busqueda==null){
    this.contenido=this.master;
  }

}

}
