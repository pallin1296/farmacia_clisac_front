import { Component, Output, OnInit, ViewChild, EventEmitter } from '@angular/core';
import { ConectorApiService } from '../../services/conector-api.service';
import { AceptarRechazarComponent } from './aceptar-rechazar/aceptar-rechazar.component';
import { DetalleListaComponent } from './detalle-lista/detalle-lista.component';
import * as pdfMake from 'pdfmake/build/pdfmake.js';
import * as pdfFonts from 'pdfmake/build/vfs_fonts.js';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-listas-creadas',
  templateUrl: './listas-creadas.component.html',
  styleUrls: ['./listas-creadas.component.css']
})
export class ListasCreadasComponent implements OnInit {

contenido:any;
envioModal:any;
envioModalAceptar:any;
estatus:any;
public fechaInicial;
public fechaFinal;
@Output() actualizar: EventEmitter<any> = new EventEmitter();
@ViewChild(AceptarRechazarComponent) aceptarLista:AceptarRechazarComponent;
@ViewChild(DetalleListaComponent) detalleLista:DetalleListaComponent;

permisos=JSON.parse(localStorage.getItem('permisos'));

  constructor (
    private conector: ConectorApiService
    ){
      this.estatus={
        "estatus":"Sin Estatus"
      }      
      this.envioModal={}
      this.envioModalAceptar={}
      let fecha=new Date();
      let mes="";
      let dia="";
      if((fecha.getMonth()+1)>=1 && (fecha.getMonth()+1)<=9){
          mes="0"+(fecha.getMonth()+1).toString();
      }else{
        mes=(fecha.getMonth()+1).toString();
      }
  
    if((fecha.getDate())>=1 && (fecha.getDate())<=9){
        dia="0"+(fecha.getDate()).toString();
    }else{
      dia=(fecha.getDate()).toString();
    }
  
      this.fechaInicial=fecha.getFullYear()+"-"+mes+"-"+dia;
      this.fechaFinal=fecha.getFullYear()+"-"+mes+"-"+dia;

      this.obtenerListasCreadas();
  }
  ngOnInit(): void {

  }
  cambiarFecha(){

      this.obtenerListasCreadas();
    
  }
async obtenerListasCreadas(){
  this.conector.cargandoDatos();
    this.estatus.fechaInicial=this.fechaInicial;
    this.estatus.fechaFinal=this.fechaFinal;

    this.conector.obtenerListasCreadas(this.estatus).subscribe(
      (e: any)=>{

        if(e.ok){

         this.contenido=e.data;
         this.conector.cerrarCargandoDatos();

        }else{
          this.conector.cerrarCargandoDatos();
          this.conector.mensajeError(e.msg);

        }

      }
    );

  }
  
  modalDetalleLista(row){

    this.envioModal={
      "idAlmacen":row.idalmacenalmacen
    };
    
    this.detalleLista.open();
  }

  modalAceptarLista(row){

    this.envioModalAceptar={
      "idAlmacenalmacen":row.idalmacenalmacen,
      "idAlmacen":row.idalmacen

    };

    this.aceptarLista.open();
  }

  listaAceptada(){
    this.estatus={
      "estatus":"Aceptado"
    }
    this.obtenerListasCreadas();

  }

  listaRechazada(){
    this.estatus={
      "estatus":"Rechazado"
    }
    this.obtenerListasCreadas();

  }

  reinciar(){
    this.estatus={
      "estatus":"Sin Estatus"
    }
    this.obtenerListasCreadas();
  }

imprimir(row){

  this.obtenerListaProducto(row);


}

obtenerListaProducto(row){
let dataInfo=[]
let listaProducto=[]
  this.conector.obtenerListaProducto({"idAlmacen":row.idalmacenalmacen}).subscribe(
    (e: any)=>{

      if(e.ok){

       e.data.forEach(element => {

       listaProducto.push([element.producto,element.cantidad]);

       });

       dataInfo=[{"info":row},{"lista":listaProducto}]
        
       this.crearPdfLista(dataInfo);

      }

    }
  );


}

crearPdfLista(data){
  let dataInfo=data[0];
  let dataLista=data[1].lista;
 
    const docDefinition = {
      pageSize: 'A4',
      content: [
        {
          text: `LISTA DE INSUMOS - AREA ${dataInfo.info.almacen}`,
          style: 'header'
        },
        {
          columns: [
            {
              width: '100%',
              text: `Fecha: ${this.conector.obtenerFechaLetra(dataInfo.info.fechaCreacion)}`,
              style: 'subheader'
            }        
          ],
          margin: [0, 0, 0, 5]
        }, 
        {
          columns: [
            {
              width: '100%',
              text: `Estatus: ${this.estatus.estatus}`,
              style: 'subheader'
            }          
          ],
          margin: [0, 0, 0, 5]
        },       
        {
          table: {
            widths: ['70%', '30%'],
            headerRows: 0,
            body: [
              [{ text: 'PRODUCTO', style: '' }, { text: 'CANTIDAD', style: '' }],
              ...dataLista
            ]
          },
          margin: [0, 0, 0, 20]
        },
        {
          columns: [
            {
              width: '50%',
              text: `___________________________________`,
              alignment: 'center'
            },
            {
              width: '50%',
              text: `___________________________________`,
              alignment: 'center'
            }
          ],
          margin: [0, 90, 0, 0]
        },
        {
          columns: [
            {
              width: '50%',
              text: `ENCARGADO DE ALMACEN`,
              alignment: 'center'
            },
            {
              width: '50%',
              text: `ENCARGADO DE ${dataInfo.info.almacen}`,
              alignment: 'center'
            }
          ],
          margin: [0,5, 0, 0]
        }
      ],
      styles: {
        header: {
          fontSize: 22,
          bold: true,
          alignment: 'center',
          margin: [0, 0, 0, 30]
        },
        subheader: {
          fontSize: 12,
          bold: true
        }
      }
    };
  
    pdfMake.createPdf(docDefinition).open();
  
  }


}
