import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { ConectorApiService } from '../../../services/conector-api.service';

@Component({
  selector: 'app-aceptar-rechazar',
  templateUrl: './aceptar-rechazar.component.html',
  styleUrls: ['./aceptar-rechazar.component.css']
})
export class AceptarRechazarComponent implements OnInit {

  titulo="Aceptacion/Rechazo de Lista";

  @Input() data;
  @Output() actualizar: EventEmitter<any> = new EventEmitter();
  @ViewChild("ModalAceptarLista") defaultModel: NgbModal;

  constructor(private modalService: NgbModal,
    private conector: ConectorApiService,
    private formBuilder: FormBuilder) {
    }

  ngOnInit(): void {
    this.data={}

  }

  ngOnChange(){
    console.info(this.data);
  }

  open() {
    this.modalService.open(this.defaultModel, { windowClass: 'animated fadeInDown',backdrop: 'static',size: 'lg', keyboard: false, centered: true });
  }

  close() {
    this.actualizar.emit();
    this.modalService.dismissAll();
  }

  aceptar(){
this.data.estatus="Aceptado";
this.cambiarEstatus();
}

  rechazar(){
    this.data.estatus="Rechazado";
    this.cambiarEstatus();
  }

cambiarEstatus(){

  this.conector.cambiarEstatus(this.data).subscribe(
    (e: any)=>{
      if(e.ok){
        this.conector.mensajeExitoso(e.msg);
        this.close();

      }else{
        this.conector.mensajeError(e.msg);
      }
  
    }
  );

}

}
