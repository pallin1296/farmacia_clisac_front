import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { ConectorApiService } from '../../../services/conector-api.service';

@Component({
  selector: 'app-detalle-lista',
  templateUrl: './detalle-lista.component.html',
  styleUrls: ['./detalle-lista.component.css']
})
export class DetalleListaComponent implements OnInit {
  titulo="Detalle Lista";
  contenido:any;
  @Input() data;
  @ViewChild("ModalDetalleLista") defaultModel: NgbModal;
  @Output() actualizar: EventEmitter<any> = new EventEmitter();

  constructor(private modalService: NgbModal,
    private conector: ConectorApiService,
    private formBuilder: FormBuilder) {
      this.data={}

    }

  ngOnInit(): void {
    this.obtenerListaProducto();
  }

  ngOnChanges(){
    this.obtenerListaProducto();
  }

  open() {
    this.modalService.open(this.defaultModel, { windowClass: 'animated fadeInDown',backdrop: 'static',size: 'lg', keyboard: false, centered: true });
  }

  close() {
    this.modalService.dismissAll();
  }

  obtenerListaProducto(){

    this.conector.obtenerListaProducto(this.data).subscribe(
      (e: any)=>{

        if(e.ok){

         this.contenido=e.data;

        }

      }
    );


  }

}
