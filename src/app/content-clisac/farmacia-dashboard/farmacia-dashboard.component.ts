import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-farmacia-dashboard',
  templateUrl: './farmacia-dashboard.component.html',
  styleUrls: ['./farmacia-dashboard.component.css']
})
export class FarmaciaDashboardComponent implements OnInit {
  
  permisos=JSON.parse(localStorage.getItem('permisos'));
  ventas=false;
  productos=false;
  usuario=false;
  almacenPrincipal=false;
  almacenSecundario=false;
  configuracion=false;

  constructor() {
    this.validarPermisos();
   }

  ngOnInit(): void {
  }

  validarPermisos(){

    if(this.permisos.ventapacienteventa==1 || this.permisos.ventapacienteventarealizada==1){
      this.ventas=true;
  }

  //modulo producto
  if(this.permisos.agregarproducto==1 || this.permisos.editarproducto==1 || this.permisos.estatusproducto==1 || 
    this.permisos.agregarcategoria==1 || this.permisos.editarcategoria==1 || this.permisos.estatuscategoria==1){
      
      this.productos=true;
      
  }

  // modulo usuarios
  if(this.permisos.agregarpaciente==1 || this.permisos.paciente==1 || 
    this.permisos.agregarempleado==1 || this.permisos.editarempleado==1 || this.permisos.estatusempleado==1 ||
    this.permisos.agregardoctor==1 || this.permisos.editardoctor==1 || this.permisos.estatusdoctor==1){
      
      this.usuario=true;

  }

 //modulo almacen principal 
 if(this.permisos.crearalmacengenerarlista==1 || 
  this.permisos.estatuslistagenerada==1 || 
  this.permisos.agregaralmacenprincipal==1 || this.permisos.ajustaralmacenprincipal==1 || this.permisos.editaralmacenprincipal==1){
    
    this.almacenPrincipal=true;

}

   //modulo almacen secundario
   if(this.permisos.generargenerarlista==1 || 
    this.permisos.listageneradasalida==1 || 
    this.permisos.agregaralmacensecundario==1 || this.permisos.ajustaralmacensecundario==1 || this.permisos.editaralmacensecundario==1){
      
      this.almacenSecundario=true;

  }

  //modulo configuracion
  if(this.permisos.agregartipocirugia==1 || this.permisos.editartipocirugia==1 || this.permisos.estatustipocirugia==1 ||
    this.permisos.agregarrole==1 || this.permisos.editarrole==1){
      
      this.configuracion=true;

  }

  }

}
