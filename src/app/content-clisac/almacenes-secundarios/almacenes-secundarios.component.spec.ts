import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlmacenesSecundariosComponent } from './almacenes-secundarios.component';

describe('AlmacenesSecundariosComponent', () => {
  let component: AlmacenesSecundariosComponent;
  let fixture: ComponentFixture<AlmacenesSecundariosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AlmacenesSecundariosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlmacenesSecundariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
