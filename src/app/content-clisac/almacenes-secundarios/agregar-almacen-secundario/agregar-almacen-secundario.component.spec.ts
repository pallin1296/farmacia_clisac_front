import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarAlmacenSecundarioComponent } from './agregar-almacen-secundario.component';

describe('AgregarAlmacenSecundarioComponent', () => {
  let component: AgregarAlmacenSecundarioComponent;
  let fixture: ComponentFixture<AgregarAlmacenSecundarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgregarAlmacenSecundarioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarAlmacenSecundarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
