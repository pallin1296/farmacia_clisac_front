import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, Output, OnInit, ViewChild,EventEmitter,Input } from '@angular/core';
import { ConectorApiService } from '../../../services/conector-api.service';

@Component({
  selector: 'app-agregar-almacen-secundario',
  templateUrl: './agregar-almacen-secundario.component.html',
  styleUrls: ['./agregar-almacen-secundario.component.css']
})
export class AgregarAlmacenSecundarioComponent implements OnInit {

  @Input() data
  @Output() actualizar: EventEmitter<any> = new EventEmitter();
  @ViewChild('NuevoAlmacenSecundario') modal
  public NuevoRegistro: FormGroup
  submitted=false;
  errorCodigo="";

  constructor(
    private _modal: NgbModal,
    private conector: ConectorApiService,
    private _builder: FormBuilder    
  ) { 
    this.data = {}
    this.NuevoRegistro = this._builder.group({
      almacenSecundario: ['',[Validators.required]]
    })    
  }

  ngOnInit(): void {
  }

  close() {
    this.actualizar.emit();
    this._modal.dismissAll();
  }

  show(){
    this._modal.open(this.modal, { windowClass: 'animated fadeInDown',backdrop: 'static',size: 'lg', keyboard: false, centered: true })
  }

  get fData(){
    return this.NuevoRegistro.controls  
  }

  limpiarInput(){

    this.submitted=false;
    this.fData.almacenSecundario.setValue(null); 

  }

  validarAlmacenSecundario(){

    this.submitted = true

    if(
      this.fData.almacenSecundario.value==null
      ){
      console.info("Error con los datos");
      return false
    }else{
      return true
    }

  }

  guardarAlmacenSecundario(){

    if(this.validarAlmacenSecundario()){
      
      let data={
        "almacen":this.fData.almacenSecundario.value
      }
    
      this.conector.agregarAlmacen(data).subscribe(
        (e: any)=>{
          if(e.ok){
    
            this.limpiarInput();
            this.actualizar.emit();
            this.conector.mensajeExitoso(e.msg);
          }else{
            this.conector.mensajeError(e.msg);
          }
    
        }
      );
    
    
    }
    
    }

}
