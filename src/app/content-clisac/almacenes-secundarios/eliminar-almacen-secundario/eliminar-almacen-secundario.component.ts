import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConectorApiService } from '../../../services/conector-api.service';
@Component({
  selector: 'app-eliminar-almacen-secundario',
  templateUrl: './eliminar-almacen-secundario.component.html',
  styleUrls: ['./eliminar-almacen-secundario.component.css']
})
export class EliminarAlmacenSecundarioComponent implements OnInit {

  titulo="";
  @Input() data;
  @Output() actualizar: EventEmitter<any> = new EventEmitter();
  @ViewChild("ModalEliminarAlmacenSecundario") defaultModel: NgbModal;

  constructor(private modalService: NgbModal,
    private conector: ConectorApiService) {    
      this.data={}
      
  }

  ngOnInit(): void {
  }

  ngOnChanges(){

    if(this.data.alta==1){
      this.titulo="Bajar Almacen Secundario.";

    }
    if(this.data.alta==2){
      this.titulo="Alta Almacen Secundario.";

    } 
    
  }

  open() {
    this.modalService.open(this.defaultModel, { windowClass: 'animated fadeInDown',backdrop: 'static',size: 'sm', keyboard: false, centered: true });
  }

  close() {
    this.actualizar.emit();
    this.modalService.dismissAll();
  }

  eliminarAlmacenSecundario(){
    let estatus={}

    if(this.data.alta==1){
      estatus={
        "idalmacen":this.data.idalmacen,
        "alta":2
      }
    }

    if(this.data.alta==2){
      estatus={
        "idalmacen":this.data.idalmacen,
        "alta":1
      }
    }
    
    this.conector.habilitarAlmacenSecundario(estatus).subscribe(
      (e: any)=>{
        if(e.ok){
          this.conector.mensajeExitoso(e.msg);
          this.close()

        }else{
          this.conector.mensajeError(e.msg);
        }

      }
    );

  }  

}
