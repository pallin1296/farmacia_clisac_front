import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EliminarAlmacenSecundarioComponent } from './eliminar-almacen-secundario.component';

describe('EliminarAlmacenSecundarioComponent', () => {
  let component: EliminarAlmacenSecundarioComponent;
  let fixture: ComponentFixture<EliminarAlmacenSecundarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EliminarAlmacenSecundarioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EliminarAlmacenSecundarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
