import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, Output, OnInit, ViewChild,EventEmitter,Input } from '@angular/core';
import { ConectorApiService } from '../../../services/conector-api.service';
@Component({
  selector: 'app-editar-almacen',
  templateUrl: './editar-almacen.component.html',
  styleUrls: ['./editar-almacen.component.css']
})
export class EditarAlmacenComponent implements OnInit {

  @Input() data
  @Input() edit:any[];
  submitted=false;
  errorCodigo="";
  detalleAlmacenSecundario={}
  @Output() actualizar: EventEmitter<any> = new EventEmitter();
  @ViewChild('EditarAlmacenSecundario') modal
  public EditarRegistro: FormGroup

  constructor(
    private _modal: NgbModal,
    private conector: ConectorApiService,
    private _builder: FormBuilder    
  ) { 
    this.data = {}
    this.EditarRegistro = this._builder.group({
      almacen: ['',[Validators.required]]
    })

  }

  ngOnInit(): void {

    this.enviarDatos();

}

ngOnChanges(){
  this.enviarDatos();
        
}
enviarDatos(){
  this.fData.almacen.setValue(this.data.almacen); 

}
limpiarInput(){

  this.submitted=false;
  this.errorCodigo="";
  this.fData.almacen.setValue(null);
}

validarAlmacenSecundairo(){

  this.submitted = true

  if(
    this.fData.almacen.value==null
    ){
    console.info("Error con los datos");
    return false
  }else{
    return true
  }

}

get fData(){
  return this.EditarRegistro.controls  
}
  close() {
    this.actualizar.emit();
    this._modal.dismissAll();
  }

  show(){
    this._modal.open(this.modal, { windowClass: 'animated fadeInDown',backdrop: 'static',size: 'lg', keyboard: false, centered: true })
  }

  editarAlmacenSecundario(){

    if(this.validarAlmacenSecundairo()){
      
      let data={
        "idalmacen":this.data.idalmacen,
        "almacen":this.fData.almacen.value
      }

     this.conector.editarAlmacenSecundario(data).subscribe(
        (e: any)=>{
          if(e.ok){
    
            this.limpiarInput();
            this.conector.mensajeExitoso(e.msg);
            this.close();
    
          }else{
            this.conector.mensajeError(e.msg);
          }
    
        }
      );    
    
    }
    
    }

}
