import { Component, OnInit,ViewChild } from '@angular/core';
import { ConectorApiService } from '../../services/conector-api.service';
import { FormBuilder } from '@angular/forms';
import { AgregarAlmacenSecundarioComponent } from './agregar-almacen-secundario/agregar-almacen-secundario.component'
import {EditarAlmacenComponent } from './editar-almacen/editar-almacen.component'
import {EliminarAlmacenSecundarioComponent } from './eliminar-almacen-secundario/eliminar-almacen-secundario.component'

@Component({
  selector: 'app-almacenes-secundarios',
  templateUrl: './almacenes-secundarios.component.html',
  styleUrls: ['./almacenes-secundarios.component.css']
})
export class AlmacenesSecundariosComponent implements OnInit {
  
  @ViewChild(AgregarAlmacenSecundarioComponent) crearAlmacenSecundario: AgregarAlmacenSecundarioComponent
  @ViewChild(EditarAlmacenComponent) editarAlmacenSecundario: EditarAlmacenComponent
  @ViewChild(EliminarAlmacenSecundarioComponent) eliminarAlmacenSecundario: EliminarAlmacenSecundarioComponent

  master:any;
  contenido:any;

  public opciones;  
  public busqueda;
  public envioModalAgregar:any;
  public envioModalDetalle:any;
  public envioModalEditar:any;
  public envioModalEstatus:any;
  
  permisos=JSON.parse(localStorage.getItem('permisos'));

  constructor(private conector: ConectorApiService, private fb: FormBuilder) {
    this.opciones="Todo"
    this.busqueda="";

    this.envioModalDetalle={}
    this.envioModalEditar={}
    this.envioModalEstatus={}
    this.envioModalAgregar={}
    
    this.actualizarListaAlmacenSecundario();
 
  }

   ngOnInit(): void {

  }

ngOnChanges(){
  this.actualizarListaAlmacenSecundario();
}

openCrear(){
  this.crearAlmacenSecundario.show();
}

openEditar(row){
  this.envioModalEditar=row;
  this.editarAlmacenSecundario.show();
}

/*openDetalle(row){
  this.envioModalDetalle=row

  this.detalleEmpleado.show();
}*/

async actualizarListaAlmacenSecundario(){
  this.conector.cargandoDatos();

  this.conector.obtenerTodosAlmacenes().subscribe(
    (e: any)=>{
      if(e.ok){

        this.contenido=e.data
        this.master=e.data
        console.info(this.contenido);
        this.almacenSecundarioBusqueda();
        this.conector.cerrarCargandoDatos();

      }else{
        this.conector.cerrarCargandoDatos();
        this.conector.mensajeError(e.msg);

      }

    }
  );
}

estatusAlmacenSecundario(row){
  console.info(row);
  this.envioModalEstatus={
    "idalmacen":row.idalmacen,
    "alta":row.alta
  }    
  this.eliminarAlmacenSecundario.open();
}

almacenSecundarioBusqueda(){

  this.contenido=[]

  if(this.opciones==undefined || this.opciones=='Todo'){
    if(this.busqueda!=""){

    this.master.forEach((x) => {

        if(x.almacen?.toLowerCase()?.includes(this.busqueda?.toLowerCase())){
          this.contenido.push(x)
        }
  
    });
  }else{
    this.contenido=this.master;
  }

  }else{

    if(this.opciones=='Activos'){
     
      this.master.forEach((x) => {

        if(this.busqueda!=""){
          if(x.almacen?.toLowerCase()?.includes(this.busqueda?.toLowerCase()) && x.alta==1){
            this.contenido.push(x)
          }
        }else{
          if(x.alta==1){
            this.contenido.push(x)
          }
        }
    
      });

    }

    if(this.opciones=='Inactivos'){
     
      this.master.forEach((x) => {
        
        if(this.busqueda!=""){
          if(x.almacen?.toLowerCase()?.includes(this.busqueda?.toLowerCase()) && x.alta==2){
            this.contenido.push(x)
          }
        }else{
          if(x.alta==2){
            this.contenido.push(x)
          }
        }
    
      });

    }

  }

}

}
