import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, Output, OnInit, ViewChild,EventEmitter,Input } from '@angular/core';
import { ConectorApiService } from '../../../services/conector-api.service';
@Component({
  selector: 'app-editar-cliente',
  templateUrl: './editar-cliente.component.html',
  styleUrls: ['./editar-cliente.component.css']
})
export class EditarClienteComponent implements OnInit {

  @Input() data
  @Input() edit:any[];
  submitted=false;
  @Output() actualizar: EventEmitter<any> = new EventEmitter();
  @ViewChild('EditarPaciente') modal
  public NuevoRegistro: FormGroup

  constructor(
    private _modal: NgbModal,
    private conector: ConectorApiService,
    private _builder: FormBuilder
  ) {
    this.data = {}
    this.NuevoRegistro = this._builder.group({
      paciente: ['',[Validators.required]],
      telefono: [''],
      edad: ['']
    })

   }

   get fData(){
    return this.NuevoRegistro.controls  
  }

  limpiarInput(){

    this.submitted=false;
    this.fData.paciente.setValue(""); 
    this.fData.telefono.setValue(""); 
    this.fData.edad.setValue(""); 

  }

  validarPaciente(){

    this.submitted = true

    if(this.fData.paciente.value==null || this.fData.paciente.value==""){
      return false
    }else{
      return true
    }

    }

    ngOnInit(): void {
    }
  
    ngOnChanges(){
      this.enviarDatos();
            
    }
    enviarDatos(){
      this.fData.paciente.setValue(this.data.paciente); 
      this.fData.telefono.setValue(this.data.telefono); 
      this.fData.edad.setValue(this.data.edad); 

    }

  close() {
    this.actualizar.emit();
    this._modal.dismissAll();
  }

  show(){
    this._modal.open(this.modal, { windowClass: 'animated fadeInDown',backdrop: 'static',size: 'lg', keyboard: false, centered: true })
  }

  editarPaciente(){

    if(this.validarPaciente()){
      let data={}

      data={
        "paciente":this.fData.paciente.value,
        "telefono":this.fData.telefono.value,
        "edad":this.fData.edad.value,
        "idPaciente":this.data.idPaciente
      }

      this.conector.modificarPaciente(data).subscribe(
        (e: any)=>{
          if(e.ok){
    
            this.limpiarInput();
            this.conector.mensajeExitoso(e.msg);
            this.close();
    
          }else{
            this.conector.mensajeError(e.msg);

          }
    
        }
      );


    }

    }

}
