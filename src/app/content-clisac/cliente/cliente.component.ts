import { Component, OnInit, ViewChild } from '@angular/core';
import { ConectorApiService } from '../../services/conector-api.service';
import { AgregarPacienteComponent } from '../modales-dinamicos/agregar-paciente/agregar-paciente.component';
import { EditarClienteComponent } from '../cliente/editar-cliente/editar-cliente.component';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {
 
  @ViewChild(AgregarPacienteComponent) agregarPaciente:AgregarPacienteComponent;
  @ViewChild(EditarClienteComponent) editarPaciente:EditarClienteComponent;

  contenido:any;
  master:any;

  public envioModalEditar:any;
  public busqueda;
  public 

  permisos=JSON.parse(localStorage.getItem('permisos'));

  constructor(
    private conector: ConectorApiService
  ) {
    this.envioModalEditar={}
    this.actualizarListaPaciente();

   }

   ngOnInit(): void {

  }

  crearPaciente(){
    this.agregarPaciente.show();
  }
  openEditar(row){
    this.envioModalEditar=row;
    this.editarPaciente.show();
  }
  async actualizarListaPaciente(){
    this.conector.cargandoDatos();

    this.conector.obtenerTodosPacientes().subscribe(
      (e: any)=>{
        if(e.ok){

            this.contenido=e.data; 
            this.master=e.data;  
            this.conector.cerrarCargandoDatos();
          }else{
            this.conector.cerrarCargandoDatos();
            this.conector.mensajeError(e.msg);

          }


      }
    );
  }

  pacienteBusqueda(){

    this.contenido=[]
    this.master.forEach((x) => {

      if(x.paciente?.toLowerCase()?.includes(this.busqueda?.toLowerCase())){
        this.contenido.push(x)
      }

    });

    if(this.busqueda=="" || this.busqueda==null){
      this.contenido=this.master;
    }

  }

}
