import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarTipoCirugiaComponent } from './agregar-tipo-cirugia.component';

describe('AgregarTipoCirugiaComponent', () => {
  let component: AgregarTipoCirugiaComponent;
  let fixture: ComponentFixture<AgregarTipoCirugiaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgregarTipoCirugiaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarTipoCirugiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
