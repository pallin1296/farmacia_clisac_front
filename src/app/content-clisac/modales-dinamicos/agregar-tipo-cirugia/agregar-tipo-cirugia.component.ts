import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, Output, OnInit, ViewChild,EventEmitter,Input } from '@angular/core';
import { ConectorApiService } from '../../../services/conector-api.service';
@Component({
  selector: 'app-agregar-tipo-cirugia',
  templateUrl: './agregar-tipo-cirugia.component.html',
  styleUrls: ['./agregar-tipo-cirugia.component.css']
})
export class AgregarTipoCirugiaComponent implements OnInit {

  @Input() data
  @Input() edit:any[];
  submitted=false;
  @Output() actualizar: EventEmitter<any> = new EventEmitter();
  @ViewChild('NuevoTipoCirugia') modal
  public NuevoRegistro: FormGroup

  constructor(
    private _modal: NgbModal,
    private conector: ConectorApiService,
    private _builder: FormBuilder
  ) {
    this.data = {}
    this.NuevoRegistro = this._builder.group({
      tipoCirugia: ['',[Validators.required]]
    })

   }

   get fData(){
    return this.NuevoRegistro.controls  
  }

  limpiarInput(){

    this.submitted=false;
    this.fData.tipoCirugia.setValue(""); 

  }

  validarTipoCirugia(){

    this.submitted = true

    if(this.fData.tipoCirugia.value==null || this.fData.tipoCirugia.value==""){
      return false
    }else{
      return true
    }

    }

  ngOnInit(): void {
  }

  close() {
    this.actualizar.emit();
    this._modal.dismissAll();
  }

  show(){
    this._modal.open(this.modal, { windowClass: 'animated fadeInDown',backdrop: 'static',size: 'lg', keyboard: false, centered: true })
  }

  guardarTipoCirugia(){

    if(this.validarTipoCirugia()){
      let data={}

      data={
        "tipoCirugia":this.fData.tipoCirugia.value
      }

      this.conector.guardarTipoCirugia(data).subscribe(
        (e: any)=>{
          if(e.ok){
    
            this.limpiarInput();
            this.conector.mensajeExitoso(e.msg);
            this.actualizar.emit();
    
          }else{
            this.conector.mensajeError(e.msg);

          }
    
        }
      );


    }

    }

}
