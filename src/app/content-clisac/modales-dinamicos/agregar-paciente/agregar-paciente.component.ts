import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, Output, OnInit, ViewChild,EventEmitter,Input } from '@angular/core';
import { ConectorApiService } from '../../../services/conector-api.service';
@Component({
  selector: 'app-agregar-paciente',
  templateUrl: './agregar-paciente.component.html',
  styleUrls: ['./agregar-paciente.component.css']
})
export class AgregarPacienteComponent implements OnInit {

  @Input() data
  @Input() edit:any[];
  submitted=false;
  @Output() actualizar: EventEmitter<any> = new EventEmitter();
  @ViewChild('NuevoPaciente') modal
  public NuevoRegistro: FormGroup

  constructor(
    private _modal: NgbModal,
    private conector: ConectorApiService,
    private _builder: FormBuilder
  ) {
    this.data = {}
    this.NuevoRegistro = this._builder.group({
      paciente: ['',[Validators.required]],
      telefono: [''],
      edad: ['']
    })

   }

   get fData(){
    return this.NuevoRegistro.controls  
  }

  limpiarInput(){

    this.submitted=false;
    this.fData.paciente.setValue(""); 
    this.fData.telefono.setValue(""); 
    this.fData.edad.setValue(""); 

  }

  validarPaciente(){

    this.submitted = true

    if(this.fData.paciente.value==null || this.fData.paciente.value==""){
      return false
    }else{
      return true
    }

    }

  ngOnInit(): void {
  }

  close() {
    this.actualizar.emit();
    this._modal.dismissAll();
  }

  show(){
    this._modal.open(this.modal, { windowClass: 'animated fadeInDown',backdrop: 'static',size: 'lg', keyboard: false, centered: true })
  }

  guardarPaciente(){

    if(this.validarPaciente()){
      let data={}

      data={
        "paciente":this.fData.paciente.value,
        "telefono":this.fData.telefono.value,
        "edad":this.fData.edad.value
      }

      this.conector.guardarPaciente(data).subscribe(
        (e: any)=>{
          if(e.ok){
    
            this.limpiarInput();
            this.conector.mensajeExitoso(e.msg);
            this.actualizar.emit();
    
          }else{
            this.conector.mensajeError(e.msg);

          }
    
        }
      );


    }

    }

}
