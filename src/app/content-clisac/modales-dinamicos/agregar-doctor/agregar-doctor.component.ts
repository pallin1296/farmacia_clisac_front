import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, Output, OnInit, ViewChild,EventEmitter,Input } from '@angular/core';
import { ConectorApiService } from '../../../services/conector-api.service';

@Component({
  selector: 'app-agregar-doctor',
  templateUrl: './agregar-doctor.component.html',
  styleUrls: ['./agregar-doctor.component.css']
})
export class AgregarDoctorComponent implements OnInit {
  
  @Input() data
  @Input() edit:any[];
  submitted=false;
  @Output() actualizar: EventEmitter<any> = new EventEmitter();
  @ViewChild('NuevoDoctor') modal
  public NuevoRegistro: FormGroup

  constructor(
    private _modal: NgbModal,
    private conector: ConectorApiService,
    private _builder: FormBuilder
  ) {
    this.data = {}
    this.NuevoRegistro = this._builder.group({
      doctor: ['',[Validators.required]],
      telefono: [''],
      interno: [true]
    })

   }

   get fData(){
    return this.NuevoRegistro.controls  
  }

  limpiarInput(){

    this.submitted=false;
    this.fData.doctor.setValue(""); 
    this.fData.telefono.setValue(""); 
    this.fData.interno.setValue(true); 

  }

  validarDoctor(){

    this.submitted = true

    if(this.fData.doctor.value==null || this.fData.doctor.value==""){
      return false
    }else{
      return true
    }

    }

  ngOnInit(): void {
  }

  close() {
    this.actualizar.emit();
    this._modal.dismissAll();
  }

  show(){
    this._modal.open(this.modal, { windowClass: 'animated fadeInDown',backdrop: 'static',size: 'lg', keyboard: false, centered: true })
  }

  guardarDoctor(){

    if(this.validarDoctor()){
      let data={}
      let interno=1;
      if(!this.fData.interno.value){
        interno=0
      }

      data={
        "doctor":this.fData.doctor.value,
        "telefono":this.fData.telefono.value,
        "interno":interno
      }

      this.conector.guardarDoctor(data).subscribe(
        (e: any)=>{
          if(e.ok){
    
            this.limpiarInput();
            this.conector.mensajeExitoso(e.msg);
            this.actualizar.emit();
    
          }else{
            this.conector.mensajeError(e.msg);

          }
    
        }
      );


    }

    }

}
