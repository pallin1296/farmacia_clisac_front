import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevaListaSalidaComponent } from './nueva-lista-salida.component';

describe('NuevaListaSalidaComponent', () => {
  let component: NuevaListaSalidaComponent;
  let fixture: ComponentFixture<NuevaListaSalidaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NuevaListaSalidaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevaListaSalidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
