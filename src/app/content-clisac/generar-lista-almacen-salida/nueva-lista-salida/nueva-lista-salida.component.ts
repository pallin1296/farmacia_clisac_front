import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { ConectorApiService } from '../../../services/conector-api.service';
@Component({
  selector: 'app-nueva-lista-salida',
  templateUrl: './nueva-lista-salida.component.html',
  styleUrls: ['./nueva-lista-salida.component.css']
})
export class NuevaListaSalidaComponent implements OnInit {
  
  frmAlm: FormGroup;
  contenido:any[]=[];
  activo=false;
  titulo="Generar Nueva Lista";

  @ViewChild("ListaAlmacenSalida") defaultModel: NgbModal;
  @Output() actualizar: EventEmitter<any> = new EventEmitter();

  constructor(private modalService: NgbModal,
    private conector: ConectorApiService,
    private formBuilder: FormBuilder) {

      
     }

    ngOnInit(): void {
      this.listarAlmacen();

      this.frmAlm = this.formBuilder.group({
        almacen: [null, Validators.required]
      });  
      
    }

  open() {
    this.modalService.open(this.defaultModel, { windowClass: 'animated fadeInDown',backdrop: 'static',size: 'lg', keyboard: false, centered: true });
  }

  close() {
    this.actualizar.emit();
    this.modalService.dismissAll();
  }
  listarAlmacen(){
    this.conector.obtenerTodosAlmacenes().subscribe(
      (e: any)=>{
        if(e.ok){

            this.contenido=e.data;   
          }


      }
    );

  }

}
