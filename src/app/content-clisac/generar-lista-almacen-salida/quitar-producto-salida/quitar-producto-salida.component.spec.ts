import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuitarProductoSalidaComponent } from './quitar-producto-salida.component';

describe('QuitarProductoSalidaComponent', () => {
  let component: QuitarProductoSalidaComponent;
  let fixture: ComponentFixture<QuitarProductoSalidaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuitarProductoSalidaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuitarProductoSalidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
