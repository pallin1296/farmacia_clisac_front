import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { ConectorApiService } from '../../../services/conector-api.service';

@Component({
  selector: 'app-cambiar-propietario-salida',
  templateUrl: './cambiar-propietario-salida.component.html',
  styleUrls: ['./cambiar-propietario-salida.component.css']
})
export class CambiarPropietarioSalidaComponent implements OnInit {
  titulo="Cambiar de Almacen";
  contenido:any;
  frmAlm: FormGroup;

  @Input() data;

  @ViewChild("cambiarPropietarioSalida") defaultModel: NgbModal;
  @Output() actualizar: EventEmitter<any> = new EventEmitter();

  constructor(private modalService: NgbModal,
    private conector: ConectorApiService,
    private formBuilder: FormBuilder) {
      this.frmAlm = this.formBuilder.group({
        almacen: [null, Validators.required]
      });  

    }

  ngOnInit():void {
    this.data={}
   
    this.listarAlmacen();
  }

  ngOnChanges(){
    this.listarAlmacen();
    
      this.frmAlm.get("almacen").setValue(this.data.idAlmacenSecundario);

  }

  listarAlmacen(){
    this.conector.obtenerTodosAlmacenes().subscribe(
      (e: any)=>{
        if(e.ok){

            this.contenido=e.data;   
  
        }


      }
    );

  }  
  open() {
    this.modalService.open(this.defaultModel, { windowClass: 'animated fadeInDown',backdrop: 'static',size: 'lg', keyboard: false, centered: true });
  }

  close() {
    this.actualizar.emit();
    this.modalService.dismissAll();
  }


}
