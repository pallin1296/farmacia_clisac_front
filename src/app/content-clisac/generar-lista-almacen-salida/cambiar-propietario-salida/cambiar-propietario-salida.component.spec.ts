import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CambiarPropietarioSalidaComponent } from './cambiar-propietario-salida.component';

describe('CambiarPropietarioSalidaComponent', () => {
  let component: CambiarPropietarioSalidaComponent;
  let fixture: ComponentFixture<CambiarPropietarioSalidaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CambiarPropietarioSalidaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CambiarPropietarioSalidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
