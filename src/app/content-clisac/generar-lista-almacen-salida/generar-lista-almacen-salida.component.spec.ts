import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerarListaAlmacenSalidaComponent } from './generar-lista-almacen-salida.component';

describe('GenerarListaAlmacenSalidaComponent', () => {
  let component: GenerarListaAlmacenSalidaComponent;
  let fixture: ComponentFixture<GenerarListaAlmacenSalidaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GenerarListaAlmacenSalidaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerarListaAlmacenSalidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
