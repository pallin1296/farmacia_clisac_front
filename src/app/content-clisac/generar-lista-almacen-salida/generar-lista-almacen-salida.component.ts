import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { ConectorApiService } from '../../services/conector-api.service';
import swal from 'sweetalert2'
import { AgregarTipoCirugiaComponent } from '../modales-dinamicos/agregar-tipo-cirugia/agregar-tipo-cirugia.component';
import { AgregarPacienteComponent } from '../modales-dinamicos/agregar-paciente/agregar-paciente.component';
import { AgregarDoctorComponent } from '../modales-dinamicos/agregar-doctor/agregar-doctor.component';
import { BuscarProductoComponent } from '../generar-lista-almacen/buscar-producto/buscar-producto.component';
import { QuitarProductoSalidaComponent } from './quitar-producto-salida/quitar-producto-salida.component';
import * as pdfMake from 'pdfmake/build/pdfmake.js';
import * as pdfFonts from 'pdfmake/build/vfs_fonts.js';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

//import { NuevaListaSalidaComponent } from './nueva-lista-salida/nueva-lista-salida.component';
//import { CambiarPropietarioSalidaComponent } from './cambiar-propietario-salida/cambiar-propietario-salida.component';

@Component({
  selector: 'app-generar-lista-almacen-salida',
  templateUrl: './generar-lista-almacen-salida.component.html',
  styleUrls: ['./generar-lista-almacen-salida.component.css']
})
export class GenerarListaAlmacenSalidaComponent implements OnInit {
  doctores:any[]=[];
  tipos:any[]=[];
  pacientes:any[]=[];
  almacenes:any;
  contenido:any[]=[];
  submitted=false;
  public envioModal:any
  permisos=JSON.parse(localStorage.getItem('permisos'));

  idAlmacen=0;
  idPaciente=0;

  codigo: string;
  cantidad: string;
  
  NuevoAlmacen: FormGroup;
  @ViewChild(AgregarTipoCirugiaComponent) agregarCirugia:AgregarTipoCirugiaComponent;
  @ViewChild(AgregarPacienteComponent) agregarPaciente:AgregarPacienteComponent;
  @ViewChild(AgregarDoctorComponent) agregarDoctor:AgregarDoctorComponent;
  @ViewChild(BuscarProductoComponent) buscarProducto:BuscarProductoComponent;
  @ViewChild(QuitarProductoSalidaComponent) quitarProducto:QuitarProductoSalidaComponent;

  constructor(private modalService: NgbModal,
    private conector: ConectorApiService,
    private formBuilder: FormBuilder) {
      this.NuevoAlmacen = this.formBuilder.group({
        almacen: [null, Validators.required],
        tipo: [null, Validators.required],
        paciente: [null, Validators.required],
        doctor: [null, Validators.required],
        enfermera: [null, Validators.required]
      });
      this.listarAlmacen();
      this.listarDoctores();
      this.listarPacientes();
      this.listarTipoCirugia();

    }

    get fData(){
      return this.NuevoAlmacen.controls  
    }

  ngOnInit(): void {

  }

  modalBuscarProducto(){
    
    this.buscarProducto.open();
  }

  listarAlmacen(){
    this.conector.cargandoDatos();
    this.conector.obtenerTodosAlmacenesActivos().subscribe(
      (e: any)=>{
        if(e.ok){

            if(this.permisos.almacen!="None"){
              let lista=[]
              e.data.forEach(element => {

                if(element.idalmacen==parseInt(this.permisos.almacen)){
                  lista.push(element);
                }
              });
              this.almacenes=lista;
              this.fData.almacen.setValue(parseInt(this.permisos.almacen));

              this.obtenerListaAlmacen();
            }else{

              this.almacenes=e.data;   

            }
            this.conector.cerrarCargandoDatos();


        }


      }
    );

  }
  listarDoctores(){
    this.conector.obtenerTodosDoctoresActivos().subscribe(
      (e: any)=>{
        if(e.ok){

            this.doctores=e.data;   

          }else{
            this.conector.mensajeError(e.msg);
          }

      }
    );

  }

  listarPacientes(){
    this.conector.obtenerTodosPacientes().subscribe(
      (e: any)=>{
        if(e.ok){

            this.pacientes=e.data;   
  
        }


      }
    );

  }

  listarTipoCirugia(){
    this.conector.obtenerTodosTiposCirugia().subscribe(
      (e: any)=>{
        if(e.ok){

            this.tipos=e.data;   
            this.conector.cerrarCargandoDatos();

        }else{
          this.conector.cerrarCargandoDatos();
        }


      }
    );

  }
  
  crearDoctor(){
    this.agregarDoctor.show();
  }

  modalQuitarProducto(row){

    this.envioModal=row;
    this.envioModal.idalmacen=this.fData.almacen.value
    this.quitarProducto.open();
  }

  crearPaciente(){
    this.agregarPaciente.show();
  }

  crearTipoCirugia(){
    this.agregarCirugia.show();
  }

  async actualizarListaDoctores(){

    this.listarDoctores();

  }

  async actualizarListaPacientes(){

    this.listarPacientes();

  }

  async actualizarListaTipoCirugia(){

    this.listarTipoCirugia();

  }

  guardarListaAlmacenSalida(){

    if(this.validarAlmacenPaciente()){


    let data={
      "idAlmacen":this.fData.almacen.value,
      "idTipoCirugia":this.fData.tipo.value,
      "idPaciente":this.fData.paciente.value,
      "idDoctor":this.fData.doctor.value,
      "enfermera":this.fData.enfermera.value,
    }
  
    this.conector.guardarAlmacenPaciente(data).subscribe(
      (e: any)=>{
        if(e.ok){
            this.idAlmacen=e.data.idAlmacenPaciente;
            this.submitted=false;
          }
  
      }
    );


    }

  }
  actualizarListaAlmacenSalida(){

    if(this.validarAlmacenPaciente()){


    let data={
      "idAlmacenPaciente":this.idAlmacen,
      "idTipoCirugia":this.fData.tipo.value,
      "idPaciente":this.fData.paciente.value,
      "idDoctor":this.fData.doctor.value,
      "enfermera":this.fData.enfermera.value,
    }
  
    this.conector.modificarAlmacenPaciente(data).subscribe(
      (e: any)=>{
        if(e.ok){
            console.info(e.msg);
          }
  
      }
    );


    }

  }
  validarAlmacenPaciente(){

    this.submitted = true

    if((this.fData.almacen.value==null || this.fData.almacen.value=="") ||
    (this.fData.tipo.value==null || this.fData.tipo.value=="") ||
    (this.fData.paciente.value==null || this.fData.paciente.value=="") ||
    (this.fData.doctor.value==null || this.fData.doctor.value=="") ||
    (this.fData.enfermera.value=="")){
      return false
    }else{
      return true
    }

    }

    obtenerListaAlmacen(){

      if((this.fData.almacen.value!=null && this.fData.almacen.value!="")){    

      this.conector.seleccionarAlmacenPaciente({"idAlmacen":this.fData.almacen.value,"idPaciente":this.fData.paciente.value}).subscribe(
        (e: any)=>{
          if(e.ok){

            this.fData.tipo.setValue(e.data.idTipoCirugia);
            this.fData.paciente.setValue(e.data.idPaciente);
            this.fData.doctor.setValue(e.data.idDoctor);
            this.fData.enfermera.setValue(e.data.enfermera);  
            this.idAlmacen=e.data.idAlmacenPaciente;

            this.obtenerUltimaListaAlmacenSalida();

            }else{

            this.fData.tipo.setValue(null);
            //this.fData.paciente.setValue(null);
            this.fData.doctor.setValue(null);
            this.fData.enfermera.setValue(null);  
            this.idAlmacen=0; 
            this.contenido=[]

            }
    
        }
      );

      }

    }

    async scanearProducto(){
   
      let codigo = this.codigo.trim();
      let cantidad = (document.getElementById("cantidad") as HTMLInputElement).value;
      let id=this.idAlmacen;
  
      
      let data=({
        "codigo":codigo,
        "cantidad":cantidad,
        "idAlmacenPaciente":this.idAlmacen,
        "idAlmacen":this.fData.almacen.value
      });
  
      this.agregarProducto(data);
  
    }

    agregarProducto(data) {
    
      if(data.codigo=="" || data.cantidad=="" || data.idAlmacen==0 || data.idAlmacenPaciente==0){
             
        return swal.fire({
          title: 'No se genero escaner',
          html: "Faltan datos importantes."
        });
       
      }
      if(data.cantidad=="0"){
             
        return swal.fire({
          title: 'No se genero escaner',
          html: "Debe ingresar una cantidad valida."
        }); 
    }
  
      this.conector.agregarProductoAListaPaciente(data).subscribe(
        (e: any)=>{
          if(!e.ok){
            
            (document.getElementById("codigo") as HTMLInputElement).value="";
            return swal.fire({
              title: 'No se genero escaner',
              html: e.msg
            });
            
          }else{
  
            this.obtenerUltimaListaAlmacenSalida();
            
          }
  
          (document.getElementById("codigo") as HTMLInputElement).value="";
          (document.getElementById("codigo") as HTMLInputElement).focus()
        }
      );
  
    }  

    async obtenerUltimaListaAlmacenSalida(){
    
      this.conector.obtenerListaAlmacenPaciente({"idAlmacenPaciente":this.idAlmacen}).subscribe(
        (e: any)=>{
          if(e.ok){
  
            this.contenido=e.data;
  
          }
  
  
        }
      );
  
    }

    async respuestaBusqueda(data){

      data.idAlmacenPaciente=this.idAlmacen
      data.idAlmacen=this.fData.almacen.value

      this.agregarProducto(data)
  
    }

    finalizarLista(){

      swal.fire({
        title: '¿Desea finalizar la lista?',
        text: "",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Finalizar!'
      }).then((result) => {
        if (result.isConfirmed) {

      this.conector.finalizarListaAlmacenSalida({"idAlmacenPaciente":this.idAlmacen}).subscribe(
        (e: any)=>{
          if(e.ok){
  
            this.crearPdfLista(e.data);
  
          }
  
  
        }
      );

    }
  })

    }

    cancelarLista(){

      swal.fire({
        title: '¿Desea Cancelar la lista?',
        text: "Al cancelar la lista, los productos se devolveran al almacen seleccionado.",
        icon: 'error',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si',
        cancelButtonText: 'No, continuar'
      }).then((result) => {
        if (result.isConfirmed) {

      this.conector.cancelarAlmacenSalida({"idAlmacenPaciente":this.idAlmacen}).subscribe(
        (e: any)=>{
          if(e.ok){
            this.fData.almacen.setValue(null);
            this.fData.tipo.setValue(null);
            this.fData.paciente.setValue(null);
            this.fData.doctor.setValue(null);
            this.fData.enfermera.setValue(null);  
            this.idAlmacen=0; 
            this.contenido=[]  
          }
  
  
        }
      );

    }
  })

    }

crearPdfLista(dataInfo){
  let data=[];
  
  dataInfo.listaAlmacen.forEach(element => {
    
    data.push([element.producto,element.cantidad]);
  
  });
  
    const docDefinition = {
      pageSize: 'A4',
      content: [
        {
          text: `LISTA DE MATERIAL Y MEDICAMENTO - AREA ${dataInfo.almacen}`,
          style: 'header',
          fontSize:16
        },
        {
          columns: [
            {
              width: '15%',
              text: `PACIENTE: `,
              alignment: 'left',
              bold:true,
              fontSize:10
            },
            {
              width: '45%',
              text: `${dataInfo.paciente} `,
              alignment: 'left',
              fontSize:10
            },
            {
              width: '10%',
              text: `EDAD: `,
              alignment: 'left',
              bold:true,
              fontSize:10
            },
            {
              width: '10%',
              text: `${dataInfo.edad} `,
              alignment: 'left',
              fontSize:10
            },
            {
              width: '10%',
              text: `TEL: `,
              alignment: 'left',
              bold:true,
              fontSize:10
            },
            {
              width: '10%',
              text: `${dataInfo.telefono} `,
              alignment: 'left',
              fontSize:10
            }
          ],
          margin: [0,5, 0, 0]
        },
        {
          columns: [
            {
              width: '15%',
              text: `DOCTOR: `,
              alignment: 'left',
              bold:true,
              fontSize:10
            },
            {
              width: '35%',
              text: `${dataInfo.doctor} `,
              alignment: 'left',
              fontSize:10
            },
            {
              width: '15%',
              text: `ENFERMERA: `,
              alignment: 'left',
              bold:true,
              fontSize:10
            },
            {
              width: '35%',
              text: `${dataInfo.enfermera} `,
              alignment: 'left',
              fontSize:10
            }
          ],
          margin: [0,5, 0, 0]
        },
        {
          columns: [
            {
              width: '20%',
              text: `TIPO CIRUGIA/EVENTO: `,
              alignment: 'left',
              bold:true,
              fontSize:8
            },
            {
              width: '30%',
              text: `${dataInfo.tipoCirugia} `,
              alignment: 'left',
              fontSize:8
            },
            {
              width: '7%',
              text: `EMISION: `,
              alignment: 'left',
              bold:true,
              fontSize:8
            },
            {
              width: '17%',
              text: `${this.conector.obtenerFechaLetra(dataInfo.fechaCreacion)} `,
              alignment: 'left',
              fontSize:7
            },
            {
              width: '9%',
              text: `IMPRESION: `,
              alignment: 'left',
              bold:true,
              fontSize:8
            },
            {
              width: '17%',
              text: `${this.conector.obtenerFechaLetra(new Date())} `,
              alignment: 'left',
              fontSize:7
            }
          ],
          margin: [0,5, 0, 10]
        },       
        {
          table: {
            widths: ['70%', '30%'],
            headerRows: 0,
            body: [
              [{ text: 'PRODUCTO', style: '' }, { text: 'CANTIDAD', style: '' }],
              ...data
            ]
          },
          margin: [0, 0, 0, 20]
        }
      ],
      styles: {
        header: {
          fontSize: 22,
          bold: true,
          alignment: 'center',
          margin: [0, 0, 0, 30]
        },
        subheader: {
          fontSize: 12,
          bold: true
        }
      }
    };
  
    pdfMake.createPdf(docDefinition).open({ callback: this.limpiarTodo() });
  
  }

  limpiarTodo(){
    this.fData.tipo.setValue(null);
    this.fData.paciente.setValue(null);
    this.fData.doctor.setValue(null);
    this.fData.enfermera.setValue(null);  
    this.idAlmacen=0; 
    this.contenido=[]

  }


}
