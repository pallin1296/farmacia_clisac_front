import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuscarProductoSalidaComponent } from './buscar-producto-salida.component';

describe('BuscarProductoSalidaComponent', () => {
  let component: BuscarProductoSalidaComponent;
  let fixture: ComponentFixture<BuscarProductoSalidaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuscarProductoSalidaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuscarProductoSalidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
