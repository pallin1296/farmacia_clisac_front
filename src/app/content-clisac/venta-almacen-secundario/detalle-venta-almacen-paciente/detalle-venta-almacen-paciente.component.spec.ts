import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleVentaAlmacenPacienteComponent } from './detalle-venta-almacen-paciente.component';

describe('DetalleVentaAlmacenPacienteComponent', () => {
  let component: DetalleVentaAlmacenPacienteComponent;
  let fixture: ComponentFixture<DetalleVentaAlmacenPacienteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetalleVentaAlmacenPacienteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleVentaAlmacenPacienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
