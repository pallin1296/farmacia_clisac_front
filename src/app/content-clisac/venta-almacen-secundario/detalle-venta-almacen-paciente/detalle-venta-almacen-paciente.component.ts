import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, Output, OnInit, ViewChild,EventEmitter,Input,ChangeDetectorRef } from '@angular/core';
import { ConectorApiService } from '../../../services/conector-api.service';

@Component({
  selector: 'app-detalle-venta-almacen-paciente',
  templateUrl: './detalle-venta-almacen-paciente.component.html',
  styleUrls: ['./detalle-venta-almacen-paciente.component.css']
})
export class DetalleVentaAlmacenPacienteComponent implements OnInit {
  @Input() data
  @ViewChild('DetalleVentaAlamcenSecundario') modal: NgbModal;
  constructor(
    private _modal: NgbModal,
    private conector: ConectorApiService,
    private cdref: ChangeDetectorRef
  ) {
    this.data = {}
   }

  ngOnInit(){

  }
 ngOnChanges(){

 }

  close() {

    this._modal.dismissAll();
  }

  show(){
    this._modal.open(this.modal, { windowClass: 'animated fadeInDown',backdrop: 'static',size: 'lg', keyboard: false, centered: true })
  }

}
