import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VentaAlmacenSecundarioComponent } from './venta-almacen-secundario.component';

describe('VentaAlmacenSecundarioComponent', () => {
  let component: VentaAlmacenSecundarioComponent;
  let fixture: ComponentFixture<VentaAlmacenSecundarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VentaAlmacenSecundarioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VentaAlmacenSecundarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
