import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalidasAlmacenPrincipalComponent } from './salidas-almacen-principal.component';

describe('SalidasAlmacenPrincipalComponent', () => {
  let component: SalidasAlmacenPrincipalComponent;
  let fixture: ComponentFixture<SalidasAlmacenPrincipalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalidasAlmacenPrincipalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalidasAlmacenPrincipalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
