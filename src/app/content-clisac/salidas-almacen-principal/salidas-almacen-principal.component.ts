import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ConectorApiService } from '../../services/conector-api.service';
import * as pdfMake from 'pdfmake/build/pdfmake.js';
import * as pdfFonts from 'pdfmake/build/vfs_fonts.js';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import swal from 'sweetalert2'

@Component({
  selector: 'app-salidas-almacen-principal',
  templateUrl: './salidas-almacen-principal.component.html',
  styleUrls: ['./salidas-almacen-principal.component.css']
})
export class SalidasAlmacenPrincipalComponent implements OnInit {
  
  master:any=[];
  contenido:any=[];
  almacenes:any=[];

  public busqueda:any;
  public almacen:any;
  public fechaInicial;
  public fechaFinal;
  permisos=JSON.parse(localStorage.getItem('permisos'));
  mostrarLista=false;
  constructor(private conector: ConectorApiService, private fb: FormBuilder) {
    let fecha=new Date();
    let mes="";
    let dia="";
    if((fecha.getMonth()+1)>=1 && (fecha.getMonth()+1)<=9){
        mes="0"+(fecha.getMonth()+1).toString();
    }else{
      mes=(fecha.getMonth()+1).toString();
    }

  if((fecha.getDate())>=1 && (fecha.getDate())<=9){
      dia="0"+(fecha.getDate()).toString();
  }else{
    dia=(fecha.getDate()).toString();
  }

    this.fechaInicial=fecha.getFullYear()+"-"+mes+"-"+dia;
    this.fechaFinal=fecha.getFullYear()+"-"+mes+"-"+dia;
    
    this.listarAlmacen();

  }

  ngOnInit(): void {
  }

  listarAlmacen(){
    this.conector.cargandoDatos();

    this.conector.obtenerTodosAlmacenes().subscribe(
      (e: any)=>{
        if(e.ok){
          if(this.permisos.almacen!="None"){
            let lista=[]
            this.mostrarLista=false;
            this.almacen=this.permisos.almacen;

            e.data.forEach(element => {

              if(element.idalmacen==parseInt(this.permisos.almacen)){
                lista.push(element);
              }
            });
            this.almacenes=lista;
            this.actualizarListaSalidas();

          }else{
            this.mostrarLista=true;
            this.almacenes=e.data;   

          }
          this.conector.cerrarCargandoDatos();


        }else{
          this.conector.cerrarCargandoDatos();
        }


      }
    );

  }
  
  cambiarFecha(){

    if(this.almacen!=null && this.almacen!=undefined && this.almacen!=""){

      this.actualizarListaSalidas();
    
    }

  }
  async actualizarListaSalidas(){

    if(this.almacen!=null && this.almacen!=undefined && this.almacen!=""){
    this.conector.obtenerListasAlmacenSalidas({idAlmacen:this.almacen,fechaInicial:this.fechaInicial,fechaFinal:this.fechaFinal}).subscribe(
      (e: any)=>{
        if(e.ok){
  
          this.contenido=e.data;
          this.master=e.data;
  
        }else{
          this.conector.mensajeError(e.msg);
        }
  
      }
    );
  }

  }

  pacienteBusqueda(){

    this.contenido=[]
    this.master.forEach((x) => {

      if(x.paciente?.toLowerCase()?.includes(this.busqueda?.toLowerCase())){
        this.contenido.push(x)
      }

    });

    if(this.busqueda=="" || this.busqueda==null){
      this.contenido=this.master;
    }

  }

  crearPdfLista(dataInfo){
    let data=[];
    
    dataInfo.listaAlmacen.forEach(element => {
      
      data.push([element.producto,element.cantidad]);
    
    });
    
      const docDefinition = {
        pageSize: 'A4',
        content: [
          {
            text: `LISTA DE MATERIAL Y MEDICAMENTO - AREA ${dataInfo.almacen}`,
            style: 'header',
            fontSize:16
          },
          {
            columns: [
              {
                width: '15%',
                text: `PACIENTE: `,
                alignment: 'left',
                bold:true,
                fontSize:10
              },
              {
                width: '45%',
                text: `${dataInfo.paciente} `,
                alignment: 'left',
                fontSize:10
              },
              {
                width: '10%',
                text: `EDAD: `,
                alignment: 'left',
                bold:true,
                fontSize:10
              },
              {
                width: '10%',
                text: `${dataInfo.edad} `,
                alignment: 'left',
                fontSize:10
              },
              {
                width: '8%',
                text: `TEL: `,
                alignment: 'left',
                bold:true,
                fontSize:10
              },
              {
                width: '12%',
                text: `${dataInfo.telefono} `,
                alignment: 'left',
                fontSize:10
              }
            ],
            margin: [0,5, 0, 0]
          },
          {
            columns: [
              {
                width: '15%',
                text: `DOCTOR: `,
                alignment: 'left',
                bold:true,
                fontSize:10
              },
              {
                width: '35%',
                text: `${dataInfo.doctor} `,
                alignment: 'left',
                fontSize:10
              },
              {
                width: '15%',
                text: `ENFERMERA: `,
                alignment: 'left',
                bold:true,
                fontSize:10
              },
              {
                width: '35%',
                text: `${dataInfo.enfermera} `,
                alignment: 'left',
                fontSize:10
              }
            ],
            margin: [0,5, 0, 0]
          },
          {
            columns: [
              {
                width: '20%',
                text: `TIPO CIRUGIA/EVENTO: `,
                alignment: 'left',
                bold:true,
                fontSize:8
              },
              {
                width: '30%',
                text: `${dataInfo.tipoCirugia} `,
                alignment: 'left',
                fontSize:8
              },
              {
                width: '7%',
                text: `EMISION: `,
                alignment: 'left',
                bold:true,
                fontSize:8
              },
              {
                width: '17%',
                text: `${this.conector.obtenerFechaLetra(dataInfo.fechaCreacion)} `,
                alignment: 'left',
                fontSize:7
              },
              {
                width: '9%',
                text: `IMPRESION: `,
                alignment: 'left',
                bold:true,
                fontSize:8
              },
              {
                width: '17%',
                text: `${this.conector.obtenerFechaLetra(new Date())} `,
                alignment: 'left',
                fontSize:7
              }
            ],
            margin: [0,5, 0, 10]
          },       
          {
            table: {
              widths: ['70%', '30%'],
              headerRows: 0,
              body: [
                [{ text: 'PRODUCTO', style: '' }, { text: 'CANTIDAD', style: '' }],
                ...data
              ]
            },
            margin: [0, 0, 0, 20]
          }
        ],
        styles: {
          header: {
            fontSize: 22,
            bold: true,
            alignment: 'center',
            margin: [0, 0, 0, 30]
          },
          subheader: {
            fontSize: 12,
            bold: true
          }
        }
      };
    
      pdfMake.createPdf(docDefinition).open();
    
    }

    cancelarLista(row){

      swal.fire({
        title: '¿Desea Cancelar la lista?',
        text: "Al cancelar la lista, los productos se devolveran al almacen seleccionado.",
        icon: 'error',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si',
        cancelButtonText: 'No, continuar'
      }).then((result) => {
        if (result.isConfirmed) {

      this.conector.cancelarAlmacenSalida({"idAlmacenPaciente":row.idAlmacenPaciente}).subscribe(
        (e: any)=>{
          if(e.ok){
            
            this.actualizarListaSalidas();

          }
  
  
        }
      );

    }
  })

    }

}
