import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarProductoAlmacenSecundarioComponent } from './agregar-producto-almacen-secundario.component';

describe('AgregarProductoAlmacenSecundarioComponent', () => {
  let component: AgregarProductoAlmacenSecundarioComponent;
  let fixture: ComponentFixture<AgregarProductoAlmacenSecundarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgregarProductoAlmacenSecundarioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarProductoAlmacenSecundarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
