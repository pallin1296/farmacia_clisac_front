import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AjustarProductoAlmacenSecundarioComponent } from './ajustar-producto-almacen-secundario.component';

describe('AjustarProductoAlmacenSecundarioComponent', () => {
  let component: AjustarProductoAlmacenSecundarioComponent;
  let fixture: ComponentFixture<AjustarProductoAlmacenSecundarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AjustarProductoAlmacenSecundarioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AjustarProductoAlmacenSecundarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
