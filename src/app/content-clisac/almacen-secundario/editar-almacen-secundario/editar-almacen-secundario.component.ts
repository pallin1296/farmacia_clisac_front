import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConectorApiService } from '../../../services/conector-api.service';

@Component({
  selector: 'app-editar-almacen-secundario',
  templateUrl: './editar-almacen-secundario.component.html',
  styleUrls: ['./editar-almacen-secundario.component.css']
})
export class EditarAlmacenSecundarioComponent implements OnInit {

  titulo="";
  @Input() data;
  @Output() actualizar: EventEmitter<any> = new EventEmitter();
  @ViewChild("ModalEditarAlmacen") defaultModel: NgbModal;
  cantidad: string;
  producto="";
  existente="";

  constructor(private modalService: NgbModal,
    private conector: ConectorApiService) {    
      this.data={}    
   }
   
   ngOnInit(): void {
    this.limpiar();
  }
  ngOnChanges(){
this.limpiar();
  }
  limpiar(){

    this.cantidad="";
    
  }

  open() {
    this.modalService.open(this.defaultModel, { windowClass: 'animated fadeInDown',backdrop: 'static',size: 'lg', keyboard: false, centered: true });
  }

  close() {
    this.actualizar.emit();
    this.modalService.dismissAll();
  }

  minimaCantidadAlmacen(){

   let cantidad=(document.getElementById("cantidad") as HTMLInputElement).value;
    
    let data={
      "cantidad":cantidad,
      "idalmacenproducto":this.data.idalmacenproducto
    };
    
    this.conector.editarMinimoAlmacenSecundario(data).subscribe(
      (e: any)=>{
        if(e.ok){
        
          (document.getElementById("cantidad") as HTMLInputElement).value="";
          this.close();
          
        }
  
      }
    );

  }

}
