import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarAlmacenSecundarioComponent } from './editar-almacen-secundario.component';

describe('EditarAlmacenSecundarioComponent', () => {
  let component: EditarAlmacenSecundarioComponent;
  let fixture: ComponentFixture<EditarAlmacenSecundarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditarAlmacenSecundarioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarAlmacenSecundarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
