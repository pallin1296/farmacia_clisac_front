import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlmacenSecundarioComponent } from './almacen-secundario.component';

describe('AlmacenSecundarioComponent', () => {
  let component: AlmacenSecundarioComponent;
  let fixture: ComponentFixture<AlmacenSecundarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AlmacenSecundarioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlmacenSecundarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
