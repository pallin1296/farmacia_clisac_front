import { Component, OnInit, ViewChild } from '@angular/core';
import { AgregarProductoAlmacenSecundarioComponent } from './agregar-producto-almacen-secundario/agregar-producto-almacen-secundario.component'
import { AjustarProductoAlmacenSecundarioComponent } from './ajustar-producto-almacen-secundario/ajustar-producto-almacen-secundario.component'
import { EditarAlmacenSecundarioComponent } from './editar-almacen-secundario/editar-almacen-secundario.component'
import { ConectorApiService } from '../../services/conector-api.service';
import swal from 'sweetalert2'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as pdfMake from 'pdfmake/build/pdfmake.js';
import * as pdfFonts from 'pdfmake/build/vfs_fonts.js';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
@Component({
  selector: 'app-almacen-secundario',
  templateUrl: './almacen-secundario.component.html',
  styleUrls: ['./almacen-secundario.component.css']
})
export class AlmacenSecundarioComponent implements OnInit {
  
  @ViewChild("ModalReporte") modalReporte: NgbModal;

  @ViewChild(AgregarProductoAlmacenSecundarioComponent) agregarProductoAlmacen: AgregarProductoAlmacenSecundarioComponent
  @ViewChild(AjustarProductoAlmacenSecundarioComponent) ajustarProductoAlmacen: AjustarProductoAlmacenSecundarioComponent
  @ViewChild(EditarAlmacenSecundarioComponent) editarProductoAlmacen: EditarAlmacenSecundarioComponent

  public envioModal:any;
  public envioModalAgregar:any;
  public envioModalAjustar:any;
  public opciones;
  contenido:any;
  listaAlmacen:any[]=[];
  valoralmacen:string;
  master:any;
  public boton1:boolean;
  public boton2:boolean;
  permisos=JSON.parse(localStorage.getItem('permisos'));
  mostrarLista=false;
  public busqueda:any;
  constructor(private modalService: NgbModal,private conector: ConectorApiService) {
    this.boton1=true;
    this.boton2=false;
    this.opciones="Pieza";
    this.busqueda="";
    this.envioModal={}
    this.listarAlmacen();

   }

  ngOnInit(): void {

  }

  openAgregar(){
    this.envioModalAgregar={
      "idalmacen":this.valoralmacen
    }    
    this.agregarProductoAlmacen.open();
  }
  openAjustar(){
    this.envioModalAjustar={
      "idalmacen":this.valoralmacen
    }       
    this.ajustarProductoAlmacen.open();
  }
  openEditar(row){

    this.envioModal={
      "idalmacenproducto":row.idalmacenproducto
    }

    this.editarProductoAlmacen.open();
  }
  listarAlmacen(){
    this.conector.cargandoDatos();
    this.conector.obtenerTodosAlmacenesActivos().subscribe(
      (e: any)=>{
        if(e.ok){

            if(this.permisos.almacen!="None"){
              let lista=[]
              this.mostrarLista=false;
              this.valoralmacen=this.permisos.almacen;

              e.data.forEach(element => {

                if(element.idalmacen==parseInt(this.permisos.almacen)){
                  lista.push(element);
                }
              });
              this.listaAlmacen=lista;
              this.obtenerListaAlmacen();
            }else{
              this.mostrarLista=true;
              this.listaAlmacen=e.data;   

            }
            this.conector.cerrarCargandoDatos();

        }else{
          this.conector.cerrarCargandoDatos();

        }


      }
    );

  }

async crear(){

if(this.valoralmacen!=null && this.valoralmacen!='' && this.valoralmacen!=undefined){
  console.info(this.valoralmacen);
  this.obtenerListaAlmacen();
}

}

async obtenerListaAlmacen(){

  this.conector.obtenerProductoAlmacenSecundario({"idalmacen":this.valoralmacen}).subscribe(
    (e: any)=>{
      if(e.ok){

        this.contenido=e.data
        this.master=e.data
        this.productoBusqueda();
      }else{
        this.conector.mensajeError(e.msg);
      }

    }
  );

}

productoBusqueda(){

  this.contenido=[]

  if(this.opciones=='Pieza'){
     
    this.master.forEach((x) => {

      if(this.busqueda!=""){

        if(x.producto?.toLowerCase()?.includes(this.busqueda?.toLowerCase()) && x.pieza==1){
          this.contenido.push(x)
        }
      }else{

        if(x.pieza==1){
          this.contenido.push(x)
        }
      }
  
    });

  }

  if(this.opciones=='Paquete'){
   
    this.master.forEach((x) => {
      
      if(this.busqueda!=""){
        if(x.producto?.toLowerCase()?.includes(this.busqueda?.toLowerCase()) && x.pieza==2){
          this.contenido.push(x)
        }
      }else{
        if(x.pieza==2){
          this.contenido.push(x)
        }
      }
  
    });

  }

}

openModalReporte() {
  this.modalService.open(this.modalReporte, { windowClass: 'animated fadeInDown',backdrop: 'static',size: 'sm', keyboard: false, centered: true });
}

closeModalReporte(){
  this.modalService.dismissAll();
}

imprimir(){

let infoTablas={}    
let dataPropuesta=[];
let dataSinPropuesta=[];
let totalPrecioProveedor=0.0;

  if(this.boton1){
    this.master.forEach((x) => {
      if(x.pieza==1){
        
        if(x.existente<x.minimo){
          dataPropuesta.push([
            { text: x.producto, style: '',alignment:'left',fontSize:8,fillColor:'#FCF3BE' },
            { text: parseFloat(x.existente).toFixed(2), style: '',alignment:'center',fontSize:8,fillColor:'#FCF3BE' },
            { text: parseFloat((x.minimo-x.existente).toString()).toFixed(2), style: '',alignment:'center',fontSize:8 },
            { text: parseFloat((x.precioProveedor).toString()).toFixed(2), style: '',alignment:'center',fontSize:8 },
            { text: parseFloat(((x.precioProveedor*(x.minimo-x.existente))).toString()).toFixed(2), style: '',alignment:'center',fontSize:8 }
          ]);
          totalPrecioProveedor=totalPrecioProveedor+(x.precioProveedor*(x.minimo-x.existente))
        }

      }
    });

    infoTablas={
      "listaPropuesta":dataPropuesta,
      "totalPresupuesto":parseFloat((totalPrecioProveedor).toString()).toFixed(2),
      "listaSinPropuesta":dataSinPropuesta}

      if(dataPropuesta.length!=0){
        this.crearPdf(infoTablas);

      }else{

        return swal.fire({
          title: 'Lista Vacía',
          html: "No se encontraron productos."
        });

      }
  }

  if(this.boton2){
    this.master.forEach((x) => {
      if(x.pieza==1){

        if(x.existente<x.minimo){
          dataPropuesta.push([
            { text: x.producto, style: '',alignment:'left',fontSize:8,fillColor:'#FCF3BE' },
            { text: parseFloat(x.existente).toFixed(2), style: '',alignment:'center',fontSize:8,fillColor:'#FCF3BE' },
            { text: parseFloat((x.minimo-x.existente).toString()).toFixed(2), style: '',alignment:'center',fontSize:8 },
            { text: parseFloat((x.precioProveedor).toString()).toFixed(2), style: '',alignment:'center',fontSize:8 },
            { text: parseFloat(((x.precioProveedor*(x.minimo-x.existente))).toString()).toFixed(2), style: '',alignment:'center',fontSize:8 }
          ]);
          totalPrecioProveedor=totalPrecioProveedor+(x.precioProveedor*(x.minimo-x.existente))
        }else{

          dataSinPropuesta.push([
            { text: x.producto, style: '',alignment:'left',fontSize:8,fillColor:'#FCF3BE' },
            { text: parseFloat(x.existente).toFixed(2), style: '',alignment:'center',fontSize:8,fillColor:'#FCF3BE' },
            { text: "", style: '',alignment:'center',fontSize:8 },
            { text: parseFloat((x.precioProveedor).toString()).toFixed(2), style: '',alignment:'center',fontSize:8 },
            { text: "", style: '',alignment:'center',fontSize:8 }
          ]);

        }

      }
    });
    infoTablas={
    "listaPropuesta":dataPropuesta,
    "totalPresupuesto":parseFloat((totalPrecioProveedor).toString()).toFixed(2),
    "listaSinPropuesta":dataSinPropuesta}

    if(dataPropuesta.length!=0 || dataSinPropuesta.length!=0){
      
      this.crearPdf(infoTablas);

    }else{

      return swal.fire({
        title: 'Lista Vacía',
        html: "No se encontraron productos."
      });

    }

  }

}

cambiarRadio(e){
  
  if(e=='r1'){
    this.boton1=true;
    this.boton2=false;
  }
  if(e=='r2'){
    this.boton1=false;
    this.boton2=true;
  }

}

crearPdf(reportes){
  var indice = this.listaAlmacen.findIndex(el => el.idalmacen == this.valoralmacen );
  
  let tablas=[];
  let nombre=[{
    columns: [
      {
        width: '74%',
        text: `INVENTARIO DE PRODUCTOS MINIMOS.`,
        alignment: 'left',
        bold:true,
        fontSize:8
      },
      {
        width: '9%',
        text: `IMPRESION: `,
        alignment: 'left',
        bold:true,
        fontSize:8
      },
      {
        width: '17%',
        text: `${this.conector.obtenerFechaLetra(new Date())} `,
        alignment: 'left',
        fontSize:7
      }
    ],
    margin: [0,5, 0, 10]
  },
  {
    columns: [
      {
        width: '100%',
        text: `Presupuesto`,
        style: 'subheader'
      }          
    ],
    margin: [0, 0, 0, 5]
  }]
  if(reportes['listaSinPropuesta'].length>0){
    
    nombre=[{
      columns: [
        {
          width: '74%',
          text: `INVENTARIO DE TODOS LOS PRODUCTOS.`,
          alignment: 'left',
          bold:true,
          fontSize:8
        },
        {
          width: '9%',
          text: `IMPRESION: `,
          alignment: 'left',
          bold:true,
          fontSize:8
        },
        {
          width: '17%',
          text: `${this.conector.obtenerFechaLetra(new Date())} `,
          alignment: 'right',
          fontSize:7
        }
      ],
      margin: [0,0, 0, 10]
    },{
      columns: [
        {
          width: '100%',
          text: `Presupuesto`,
          style: 'subheader'
        }          
      ],
      margin: [0, 0, 0, 5]
    }]

  }

  tablas.push(nombre);

  if(reportes['listaPropuesta'].length>0){
    
    tablas.push(
    {
      table: {
        widths: ['40%', '15%','15%','15%','15%'],
        headerRows: 0,
        body: [
          [{ text: 'PRODUCTO', style: '',fillColor:'#D6EAF8',bold:true,fontSize:10 }, 
          { text: 'EXISTENTE', style: '',alignment:'center',fillColor:'#D6EAF8',bold:true,fontSize:10 }, 
          { text: 'PROPUESTA', style: '',alignment:'center',fillColor:'#D6EAF8',bold:true,fontSize:10 }, 
          { text: 'PRECIO PROVEEDOR', style: '',alignment:'center',fillColor:'#D6EAF8',bold:true,fontSize:10 },
          { text: 'IMPORTE', style: '',alignment:'center',fillColor:'#D6EAF8',bold:true,fontSize:10 }
        ],
          
          ...reportes['listaPropuesta']
        ]
      },
      margin: [0, 0, 0, 5]
    },
    {
      columns: [
        {
          width: '70%',
          text: ``              
        },
        {
          width: '12%',
          text: `Subtotal: $`,
          bold:true,
          alignment: 'left',
          fontSize:12
        },
        {
          width: '18%',
          text: reportes['totalPresupuesto'],
          alignment: 'right',
          fontSize:12
        }
        ],
        margin: [0, 0, 0, 5]
    });

  }

  if(reportes['listaSinPropuesta'].length>0){
    
    tablas.push({
      table: {
        widths: ['40%', '15%','15%','15%','15%'],
        headerRows: 0,
        body: [
          [{ text: 'PRODUCTO', style: '',fillColor:'#D6EAF8',bold:true,fontSize:10 }, 
          { text: 'EXISTENTE', style: '',alignment:'center',fillColor:'#D6EAF8',bold:true,fontSize:10 }, 
          { text: 'PROPUESTA', style: '',alignment:'center',fillColor:'#D6EAF8',bold:true,fontSize:10 }, 
          { text: 'PRECIO PROVEEDOR', style: '',alignment:'center',fillColor:'#D6EAF8',bold:true,fontSize:10 },
          { text: 'IMPORTE', style: '',alignment:'center',fillColor:'#D6EAF8',bold:true,fontSize:10 }
        ],
          
          ...reportes['listaSinPropuesta']
        ]
      },
      margin: [0, 0, 0, 5]
    },
    {
      columns: [
        {
          width: '70%',
          text: ``              
        },
        {
          width: '12%',
          text: `Subtotal: $`,
          bold:true,
          alignment: 'left',
          fontSize:12
        },
        {
          width: '18%',
          text: ' ',
          alignment: 'right',
          fontSize:12
        }
        ],
        margin: [0, 0, 0, 5]
    },
    {
      columns: [
        {
          width: '70%',
          text: ``              
        },
        {
          width: '10%',
          text: `Total: $`,
          bold:true,
          alignment: 'left',
          fontSize:12
        },
        {
          width: '20%',
          text: ' ',
          alignment: 'right',
          fontSize:12
        }
        ],
        margin: [0, 0, 0, 5]
    });

  }

  const docDefinition = {
    pageSize: 'A4',
    content: [
      {
        text: `ALMACEN SECUNDARIO`,
        style: 'subheader',
        alignment: 'center',
        fontSize:14
      },
      {
        text: this.listaAlmacen[indice].almacen,
        style: 'subheader',
        alignment: 'center',
        fontSize:12
      },
      tablas
    ],
    styles: {
      header: {
        fontSize: 22,
        bold: true,
        alignment: 'center',
        margin: [0, 0, 0, 30]
      },
      subheader: {
        fontSize: 12,
        bold: true
      }
    }
  };

  pdfMake.createPdf(docDefinition).open();


}

}
