import { Component, ComponentFactoryResolver, OnInit, ViewChild,Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import { ModalGenerarCobroComponent } from './modal-generar-cobro/modal-generar-cobro.component';
import { ModalEntradaComponent } from './modal-entrada/modal-entrada.component';
import { ModalSalidaComponent } from './modal-salida/modal-salida.component';
import { ConectorApiService } from '../../services/conector-api.service';
import { BuscarProductoComponent } from '../generar-lista-almacen/buscar-producto/buscar-producto.component';
import { QuitarProductoAlmacenPrincipalComponent } from './quitar-producto-almacen-principal/quitar-producto-almacen-principal.component';
import swal from 'sweetalert2'
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-ventas',
  templateUrl: './ventas.component.html',
  styleUrls: ['./ventas.component.css']
})

export class VentasComponent implements OnInit {
listaVentas:any[]=[];
contenido:any[]=[];
productos:any[]=[];
indexActivate=null;
precioPublico=true;
idAlmacen=0;
codigo: string;
buscarNombre=false;

detalleVenta=[];
detalleProducto=null;

finalizarDevolucion=false;
public envioModal:any;
public motivo="";
public devolver=0;
frmDevolucion: FormGroup;
frmProducto: FormGroup;

@ViewChild(ModalGenerarCobroComponent) generarCobro:ModalGenerarCobroComponent;
@ViewChild(BuscarProductoComponent) buscarProducto:BuscarProductoComponent;
@ViewChild(QuitarProductoAlmacenPrincipalComponent) quitarProducto:QuitarProductoAlmacenPrincipalComponent;
@ViewChild(ModalEntradaComponent) generarEntrada:ModalEntradaComponent;
@ViewChild(ModalSalidaComponent) generarSalida:ModalSalidaComponent;

@ViewChild('DevolucionProducto') ModalDevolucionProducto
@ViewChild('DetalleProducto') ModalDetalleProducto

constructor (
  private modalService: NgbModal,
  private formBuilder: FormBuilder,
    private conector: ConectorApiService
  ){
    this.envioModal={}
    this.obtenerVenta();
    this.listarProducto();
    this.frmDevolucion = this.formBuilder.group({
      folio: [null, Validators.required]
    }); 
    this.frmProducto = this.formBuilder.group({
      codigo: [null, Validators.required]
    }); 
}

ngOnInit() {
   
  }

  get fData(){
    return this.frmDevolucion.controls  
  }

  get fDataProducto(){
    return this.frmProducto.controls  
  }

  editar(){
    
    this.generarCobro.open();

    //this.empleadoinfo.get('correo').setValue(null);
    //this.empleadoinfo.get('password').setValue(null);

    //this.sobreModalService.open(ModalEditar, { windowClass: 'animated fadeInDown',size:'lg',centered:true })
  }

  obtenerVenta(){
    this.conector.cargandoDatos();

    this.conector.obtenerUltimasVentas().subscribe(
      (e: any)=>{
        if(e.ok){
          this.listaVentas=e.data;
          if(this.listaVentas.length>0){
            this.contenido=e.data[this.listaVentas.length-1].detalleventa
            this.indexActivate=this.listaVentas.length-1;
          }else{
            this.indexActivate=null
            this.contenido=[]
          }

          this.conector.cerrarCargandoDatos();
          
        }else{
          this.conector.cerrarCargandoDatos();
        }
  
      }
    );
  }

  recargarVenta(){
    this.conector.cargandoDatos();

    this.conector.obtenerUltimasVentas().subscribe(
      (e: any)=>{
        if(e.ok){
            this.listaVentas=e.data;
            this.contenido=e.data[this.indexActivate].detalleventa
          this.conector.cerrarCargandoDatos();
          
        }else{
          this.conector.cerrarCargandoDatos();
        }
  
      }
    );
  }

  ventaSeleccionada(venta,index){
    this.indexActivate=index;
    this.contenido=venta.detalleventa;
  }

  nuevaVenta(){

    this.conector.cargandoDatos();
    this.conector.crearNuevaVenta().subscribe(
      (e: any)=>{
        if(e.ok){
          this.conector.cerrarCargandoDatos();  
          this.obtenerVenta()
        }else{
          this.conector.cerrarCargandoDatos();
        }
  
      }
    );

  }

  cancelarVenta(){
    
    let data={
      idventa:this.listaVentas[this.indexActivate].idventa
    }

    this.conector.cargandoDatos();

    this.conector.cancelarVenta(data).subscribe(
      (e: any)=>{
        if(e.ok){
          this.conector.cerrarCargandoDatos();  
          this.obtenerVenta()
        }else{
          this.conector.cerrarCargandoDatos();
        }
  
      }
    );

  }

  modalBuscarProducto(){
    
    this.buscarProducto.open();
  }

  modalCobro(){
    
    this.envioModal=this.listaVentas[this.indexActivate];
    this.envioModal.precioPublico=this.precioPublico;

    if(this.precioPublico){
      this.envioModal={
        importe:this.listaVentas[this.indexActivate].importepublico,
        idventa:this.listaVentas[this.indexActivate].idventa,
        precioPublico:this.precioPublico
      }
    }else{
      this.envioModal={
        importe:this.listaVentas[this.indexActivate].importecliente,
        idventa:this.listaVentas[this.indexActivate].idventa,
        precioPublico:this.precioPublico
      }
    }

    this.generarCobro.open();
  }

  async scanearProducto(){
   
    let codigo = this.codigo.trim();
    let cantidad = (document.getElementById("cantidad") as HTMLInputElement).value;
    
    let data=({
      codigo:codigo,
      cantidad:cantidad,
      idventa:this.listaVentas[this.indexActivate].idventa
    });

    this.agregarProducto(data);

  }

  async respuestaBusqueda(data){

    data.idventa=this.listaVentas[this.indexActivate].idventa
    this.agregarProducto(data)

  }

  agregarProducto(data) {
    
    if(data.codigo=="" || data.cantidad=="" || data.idventa==null){
           
      return swal.fire({
        title: 'No se genero escaner',
        html: "Faltan datos importantes."
      });
     
    }
    if(data.cantidad=="0"){
           
      return swal.fire({
        title: 'No se genero escaner',
        html: "Debe ingresar una cantidad valida."
      }); 
  }

    this.conector.agregarProductoAVentaAlmacenPrincipal(data).subscribe(
      (e: any)=>{
        if(!e.ok){
          
          (document.getElementById("codigo") as HTMLInputElement).value="";
          return swal.fire({
            title: 'No se genero escaner',
            html: e.msg
          });
          
        }else{
          this.recargarVenta()
        }

        (document.getElementById("codigo") as HTMLInputElement).value="";
        (document.getElementById("codigo") as HTMLInputElement).focus()
      }
    );

  }  

  modalQuitarProducto(row){

    this.envioModal=row;
    this.quitarProducto.open();
  }

  modalGenerarEntrada(){

    this.generarEntrada.open();

  }

  modalGenerarSalida(){

    this.generarSalida.open();

  }

  ventaCobrada(event){
    if(event.cobrado){
      this.obtenerVenta();
    }
  }

  reimprimirTicket(){

    this.conector.cargandoDatos();

    this.conector.reimprimirUltimaVenta().subscribe(
      (e: any)=>{
        if(e.ok){
         
          this.conector.ticket58mm(e.data);
          this.conector.cerrarCargandoDatos();
          
        }else{
          this.conector.cerrarCargandoDatos();
        }
  
      }
    );
  }

  openModalDevolucionProducto(){
    this.fData.folio.setValue(null);
    this.detalleVenta=[];
    this.motivo="";
    this.modalService.open(this.ModalDevolucionProducto, { windowClass: 'animated fadeInDown',size:'lg' });
  }

  cerrarModalDevolucionProducto(){
    this.modalService.dismissAll();
  }

  consultarVentaPorFolio(){
    let data={
      "folio":this.fData.folio.value
    }

    this.conector.cargandoDatos();

    this.conector.consultarVentaPorFolio(data).subscribe(
      (e: any)=>{
        if(e.ok){

          this.detalleVenta=e.data
          this.conector.cerrarCargandoDatos();
          
        }else{
          this.conector.cerrarCargandoDatos();
        }
  
      }
    );

  }

  consultarDetalleProducto(){
    let data={
      "codigo":this.fDataProducto.codigo.value
    }

    this.conector.cargandoDatos();

    this.conector.consultarProductoAlmacenPrincipal(data).subscribe(
      (e: any)=>{
        if(e.ok){

          this.detalleProducto=e.data
          this.conector.cerrarCargandoDatos();
          
        }else{
          this.detalleProducto=null
          this.conector.cerrarCargandoDatos();
        }
  
      }
    );

  }

  cambiarChecked(index){
    this.detalleVenta['productos'][index].checked=!this.detalleVenta['productos'][index].checked
    this.listaChecked();    
  }

  sumar(index){
    this.detalleVenta['productos'][index].cantidadADevolver=Number(this.detalleVenta['productos'][index].cantidadADevolver)+1;

    if(this.detalleVenta['productos'][index].cantidadADevolver>this.detalleVenta['productos'][index].devolucionDisponible){
      this.detalleVenta['productos'][index].cantidadADevolver=Number(this.detalleVenta['productos'][index].devolucionDisponible);
    }

    let listaChecked=this.detalleVenta['productos'].filter(function(producto){return producto.checked==true})
    this.cantidadDescuento(listaChecked)

  }
  restar(index){

    this.detalleVenta['productos'][index].cantidadADevolver=Number(this.detalleVenta['productos'][index].cantidadADevolver)-1;

    if(this.detalleVenta['productos'][index].cantidadADevolver<1){
      this.detalleVenta['productos'][index].cantidadADevolver=1
    }

    let listaChecked=this.detalleVenta['productos'].filter(function(producto){return producto.checked==true})
    this.cantidadDescuento(listaChecked)
  }

  listaChecked(){

    let listaChecked=this.detalleVenta['productos'].filter(function(producto){return producto.checked==true})

    if(listaChecked.length>0){
      this.finalizarDevolucion=true;
    }else{
      this.finalizarDevolucion=false;
    }

    this.cantidadDescuento(listaChecked);

  }

  validarInputDevolucion(){
    if (this.motivo==''){
      return false;
    }else{
      return true;
    }
  }

  generarDevolucion(){
    
    if(this.validarInputDevolucion()==false){
      return
    }

    let listaChecked=this.detalleVenta['productos'].filter(function(producto){return producto.checked==true})

    let data={
      idVenta:this.detalleVenta['idVenta'],
      motivo:this.motivo,
      tipoCobro:this.detalleVenta['tipoCobro'],
      productos:listaChecked
    }

    this.conector.aplicarDevolucion(data).subscribe(
      (e: any)=>{
        if(e.ok){
          console.info(e.data);
          this.conector.cerrarCargandoDatos();
          this.cerrarModalDevolucionProducto();
          let informacionDevolucion={
            "listaDevoluciones":listaChecked,
            "motivo":this.motivo,
            "encargado":e.data.encargado,
            "fechadevolucion":e.data.fechadevolucion,
            "importe":this.devolver
          }
          this.conector.devolucion58mm(informacionDevolucion);
          this.devolver=0;
        }else{
          this.conector.cerrarCargandoDatos();
          this.conector.mensajeError(e.msg);
        }
  
      }
    );

  }

  cantidadDescuento(listaChecked){
    this.devolver=0;

    listaChecked.forEach(producto => {
      console.info(Number(producto.devolucion)*Number(producto.precio));
      this.devolver=this.devolver+(Number(producto.cantidadADevolver)*Number(producto.precio))
      
    });

  }

  openModalDetalleProducto(){
    this.fDataProducto.codigo.setValue(null);
    this.detalleProducto=null;
    this.modalService.open(this.ModalDetalleProducto, { windowClass: 'animated fadeInDown',size:'lg' });
  }

  cambiarNombre(){
    this.buscarNombre=!this.buscarNombre;
  }
  listarProducto(){
    this.conector.obtenerTodosProductos().subscribe(
      (e: any)=>{
        if(e.ok){

            this.productos=e.data;   
  
        }

      }
    );

  }

}
