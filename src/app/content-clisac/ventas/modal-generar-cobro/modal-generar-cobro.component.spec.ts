import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalGenerarCobroComponent } from './modal-generar-cobro.component';

describe('ModalGenerarCobroComponent', () => {
  let component: ModalGenerarCobroComponent;
  let fixture: ComponentFixture<ModalGenerarCobroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalGenerarCobroComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalGenerarCobroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
