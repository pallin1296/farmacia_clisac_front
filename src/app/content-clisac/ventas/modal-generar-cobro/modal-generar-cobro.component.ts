import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { ConectorApiService } from '../../../services/conector-api.service';
import * as pdfMake from 'pdfmake/build/pdfmake.js';
import * as pdfFonts from 'pdfmake/build/vfs_fonts.js';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import * as JsBarcode from 'jsbarcode/bin/JsBarcode';
import swal from 'sweetalert2';
import { ProductoComponent } from 'app/content-clisac/producto/producto.component';

@Component({
  selector: 'app-modal-generar-cobro',
  templateUrl: './modal-generar-cobro.component.html',
  styleUrls: ['./modal-generar-cobro.component.css']
})
export class ModalGenerarCobroComponent implements OnInit {

  pagoinfo: FormGroup;
  @Input() data;
  @ViewChild("ModalGenerarCobro") defaultModel: NgbModal;
  @Output() actualizar: EventEmitter<any> = new EventEmitter();
  cobrado=false;
  constructor(private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private conector: ConectorApiService
    ) { }
  cambio=0.00;
  ngOnInit(): void {
    this.pagoinfo = this.formBuilder.group({
      importe: ['', null],
      tipoPago: ['', Validators.required],
      recibido: ['', Validators.required],
      otroBanco: [''],
      referencia: [''],
      banco: ['']
    }); 

  }

  ngOnChanges(){

    if(this.data.idventa!=undefined){
      this.cobrado=false;
      this.pagoinfo.get("importe").setValue(this.data.importe);
      
      this.pagoinfo.get("tipoPago").setValue("EF");
      this.pagoinfo.get("banco").setValue("BBVA México");
      this.pagoinfo.get("recibido").setValue("0")
      this.cambio=this.data.importe*-1
    }

  }

  open() {
    this.modalService.open(this.defaultModel, { windowClass: 'animated fadeInDown',backdrop: 'static',size: 'lg', keyboard: false, centered: true });
  }

  close() {
    this.actualizar.emit({cobrado:this.cobrado});
    this.modalService.dismissAll();
  }

  cambioTipoPago(){
    
    if(this.pagoinfo.get("tipoPago").value=="EF"){
      this.pagoinfo.get("recibido").setValue("0")
      this.cambio=this.data.importe*-1
    }else{
      this.pagoinfo.get("recibido").setValue(this.data.importe)
      this.cambio=0

    }

  }

  obtenerCambio(){
    
    this.cambio=this.pagoinfo.get("recibido").value-this.data.importe;

  }

  finalizarVenta(){

    let data={
      idventa:this.data.idventa,
      cambio:this.cambio,
      recibido:this.pagoinfo.get("recibido").value,
      tipoPago:this.pagoinfo.get("tipoPago").value,
      precioPublico:this.data.precioPublico,
      banco:this.pagoinfo.get("banco").value,
      otroBanco:this.pagoinfo.get("otroBanco").value,
      referencia:this.pagoinfo.get("referencia").value
    }

    this.conector.cargandoDatos();

    this.conector.finalizarVenta(data).subscribe(
      (e: any)=>{
        if(e.ok){
          this.conector.cerrarCargandoDatos();  
          this.cobrado=true;
          this.conector.ticket58mm(e.data);
          this.close();
        }else{
          this.conector.cerrarCargandoDatos();
        }
  
      }
    );


  }

  generarTicket(dataInfo){

    let tablaProductos=[]
    
    dataInfo.productos.forEach(producto => {
      
      tablaProductos.push(
          {
            columns: [
              {
                width: '100%',
                text: producto.producto,
                alignment: 'left',
                fontSize:8
              }
            ],
            margin: [0,2, 0, 0]
          },
          {
            columns: [
              {
                width: '30%',
                text: producto.codigo,
                alignment: 'left',
                fontSize:8
              },
              {
                width: '25%',
                text: producto.precio,
                alignment: 'center',
                fontSize:8
              },
              {
                width: '20%',
                text: producto.cantidad,
                alignment: 'center',
                fontSize:8
              },
              {
                width: '25%',
                text: producto.importe,
                alignment: 'right',
                fontSize:8
              }

            ],
            margin: [0,2, 0, 0]
          }
      );
    
    });
    
      const docDefinition = {
        pageSize: {
          width: 280,
          height: 'auto'
        }, 
        content: [
          {
            text: `C L I S A C`,
            fontSize:14,
            bold:true,
            alignment: 'center'
          },
          {
            text: `domicilio`,
            fontSize:12,
            alignment: 'center'
          },
          {
            text: `ciudad, estado`,
            fontSize:10,
            alignment: 'center'
          },
          {
            columns: [
              {
                width: '100%',
                text: `Tel: `,
                fontSize:10,
                alignment: 'center'
              }
            ],
            margin: [0,5, 0, 0]
          },
          {
            columns: [
              {
                columns: [
                  {
                    width: '60%',
                    text: 'Atiende: '+dataInfo.usuario,
                    alignment: 'left',
                    fontSize:6
                  }
                ],
                margin: [0,5, 0, 0]
              },
              {
                width: '40%',
                text: 'Fecha: '+dataInfo.fecha,
                alignment: 'right',
                fontSize:6
              }
            ],
            margin: [0,10, 0, 0]
          },
          {canvas: [{ type: 'line', x1: 0, y1: 5, x2: 280-2*40, y2: 5, lineWidth: 1 }]},          
          {
            columns: [
              {
                width: '100%',
                text: `Producto`,
                alignment: 'left',
                bold:true,
                fontSize:10
              }
            ],
            margin: [0,2, 0, 0]
          },
          {
            columns: [
              {
                width: '30%',
                text: `#`,
                alignment: 'left',
                bold:true,
                fontSize:10
              },
              {
                width: '25%',
                text: `Precio`,
                alignment: 'center',
                bold:true,
                fontSize:10
              },
              {
                width: '20%',
                text: `Cant.`,
                alignment: 'center',
                bold:true,
                fontSize:10
              },
              {
                width: '25%',
                text: `Importe`,
                alignment: 'right',
                bold:true,
                fontSize:10
              }

            ],
            margin: [0,2, 0, 0]
          },
          {canvas: [{ type: 'line', x1: 0, y1: 5, x2: 280-2*40, y2: 5, lineWidth: 1 }]},          
          ...tablaProductos,
          {canvas: [{ type: 'line', x1: 0, y1: 5, x2: 280-2*40, y2: 5, lineWidth: 1 }]},          
          {
            columns: [
              {
                width: '75%',
                bold:true,
                text: `Importe: `,
                fontSize:10,
                alignment: 'right'
              },
              {
                width: '25%',
                text: dataInfo.importe,
                fontSize:10,
                alignment: 'right'
              }              
            ],
            margin: [0,2, 0, 0]
          },  
          {
            columns: [
              {
                width: '75%',
                bold:true,
                text: `Recibido: `,
                fontSize:10,
                alignment: 'right'
              },
              {
                width: '25%',
                text: dataInfo.recibido,
                fontSize:10,
                alignment: 'right'
              }              
            ],
            margin: [0,2, 0, 0]
          }, 
          {
            columns: [
              {
                width: '75%',
                bold:true,
                text: `Cambio: `,
                fontSize:10,
                alignment: 'right'
              },
              {
                width: '25%',
                text: dataInfo.cambio,
                fontSize:10,
                alignment: 'right'
              }              
            ],
            margin: [0,2, 0, 0]
          }, 
          {
            columns: [
              {
                width: '50%',
                bold:true,
                text: `Tipo de Pago: `,
                fontSize:6,
                alignment: 'right'
              },
              {
                width: '50%',
                text: dataInfo.tipoPago,
                fontSize:6,
                alignment: 'left'
              }              
            ],
            margin: [10,10, 0, 0]
          }, 
          {
            columns: [
              {
                width: '100%',
                bold:true,
                text:'',
                fontSize:10,
                alignment: 'right'
              }             
            ],
            margin: [10,10, 0, 0]
          },
          {
            width: 200,
            height: 40,              
            image: this.getBarcode(dataInfo.folio),
            alignment: 'center'
          }
        ],
        styles: {
          header: {
            fontSize: 22,
            bold: true,
            alignment: 'center',
            margin: [0, 0, 0, 30]
          },
          subheader: {
            fontSize: 12,
            bold: true
          }
        }
      };
    
      pdfMake.createPdf(docDefinition).open();
    
    }

    getBarcode(text) {
      var canvas = document.createElement("canvas");
      JsBarcode(canvas, text, {format: "EAN13"});
      return canvas.toDataURL("image/png");
    }

}
