import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { ConectorApiService } from '../../../services/conector-api.service';

@Component({
  selector: 'app-modal-salida',
  templateUrl: './modal-salida.component.html',
  styleUrls: ['./modal-salida.component.css']
})
export class ModalSalidaComponent implements OnInit {

  @Input() data;
  @ViewChild("ModalSalida") defaultModel: NgbModal;
  @Output() actualizar: EventEmitter<any> = new EventEmitter();
  cobrado=false;
  frmSalida: FormGroup;

  constructor(private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private conector: ConectorApiService
    ) {
      this.frmSalida = this.formBuilder.group({
        cantidad: [null, Validators.required],
        motivo: ['', Validators.required]
      });  
     }

  ngOnInit(): void {
  }

  get fData(){
    return this.frmSalida.controls  
  }

  open() {
    this.resetForm();
    this.modalService.open(this.defaultModel, { windowClass: 'animated fadeInDown',backdrop: 'static',size: 'lg', keyboard: false, centered: true });
  }

  close() {
    this.actualizar.emit();
    this.modalService.dismissAll();
  }

  guardarSalida(){
    
    if(!this.validarFormulario()){
      return
    }

    let data={
      cantidad:this.fData.cantidad.value,
      motivo:this.fData.motivo.value,
      tipomovimiento:"Salida"
    }

    this.conector.cargandoDatos();

    this.conector.agregarMovimiento(data).subscribe(
      (e: any)=>{
        if(e.ok){

          this.conector.cerrarCargandoDatos();
          this.conector.mensajeExitoso(e.msg);
          this.close();
          
        }else{
          this.conector.cerrarCargandoDatos();
          this.conector.mensajeExitoso(e.msg);
        }
  
      }
    );

  }

  validarFormulario(){
    let validado=false;
    if(this.fData.cantidad.value!=null && this.fData.cantidad.value!='' 
    && this.fData.cantidad.value!=0 && this.fData.motivo.value!=null && this.fData.motivo.value!=''){
      validado=true;
    }

    return validado;
  }

  resetForm(){
    this.fData.cantidad.setValue(null)
    this.fData.motivo.setValue('')
  }

}
