import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuitarProductoAlmacenPrincipalComponent } from './quitar-producto-almacen-principal.component';

describe('QuitarProductoAlmacenPrincipalComponent', () => {
  let component: QuitarProductoAlmacenPrincipalComponent;
  let fixture: ComponentFixture<QuitarProductoAlmacenPrincipalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuitarProductoAlmacenPrincipalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuitarProductoAlmacenPrincipalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
