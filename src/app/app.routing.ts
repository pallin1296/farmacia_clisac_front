import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { FarmaciaService } from './services/farmacia.service';

import { VentasComponent } from '././content-clisac/ventas/ventas.component';
import { MostrarVentasActualesComponent } from '././content-clisac/mostrar-ventas-actuales/mostrar-ventas-actuales.component';
import { MostrarVentasPendientesComponent } from '././content-clisac/mostrar-ventas-pendientes/mostrar-ventas-pendientes.component';
import { ConsultarVentaComponent } from '././content-clisac/consultar-venta/consultar-venta.component';
import { ProductoComponent } from '././content-clisac/producto/producto.component';
import { CategoriaComponent } from '././content-clisac/categoria/categoria.component';
import { InventarioComponent } from '././content-clisac/inventario/inventario.component';
import { EmpleadoComponent } from '././content-clisac/empleado/empleado.component';
import { ClienteComponent } from '././content-clisac/cliente/cliente.component';
import { EmpresaComponent } from '././content-clisac/empresa/empresa.component';
import { RoleComponent } from '././content-clisac/role/role.component';
import { CorteCajaComponent } from '././content-clisac/corte-caja/corte-caja.component';
import { GenerarListaAlmacenComponent } from '././content-clisac/generar-lista-almacen/generar-lista-almacen.component';
import { ListasCreadasComponent } from '././content-clisac/listas-creadas/listas-creadas.component';
import { AlmacenPrincipalComponent } from '././content-clisac/almacen-principal/almacen-principal.component';
import { AlmacenSecundarioComponent } from '././content-clisac/almacen-secundario/almacen-secundario.component';
import { GenerarListaAlmacenSalidaComponent } from '././content-clisac/generar-lista-almacen-salida/generar-lista-almacen-salida.component';
import { SalidasAlmacenPrincipalComponent } from '././content-clisac/salidas-almacen-principal/salidas-almacen-principal.component';
import { DoctorComponent } from '././content-clisac/doctor/doctor.component';
import { TipoCirugiaComponent } from '././content-clisac/tipo-cirugia/tipo-cirugia.component';
import { AlmacenesSecundariosComponent } from '././content-clisac/almacenes-secundarios/almacenes-secundarios.component';

const routes: Routes =[
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  }, 
  {
    path: '',
    component: AdminLayoutComponent,
    canActivate: [FarmaciaService],
    children: [
          {
        path: '',
        loadChildren: './layouts/admin-layout/admin-layout.module#AdminLayoutModule'
          }
          /*,
        {
        path: 'categoria',
        component: CategoriaComponent,
        canActivate: [FarmaciaService],
        data: { role: ['categoria']}
      },
      {
      path: 'producto',
      component: ProductoComponent,
      canActivate: [FarmaciaService],
      data: { role: ['producto']}
      },
      {
      path: 'empleado',
      component: EmpleadoComponent,
      canActivate: [FarmaciaService],
      data: { role: ['empleado']}
    },
    {
    path: 'paciente',
    component: ClienteComponent,
    canActivate: [FarmaciaService],
    data: { role: ['paciente']}
  },
    {
    path: 'doctor',
    component: DoctorComponent,
    canActivate: [FarmaciaService],
    data: { role: ['doctor']}
  },
  {
  path: 'almacenPrincipal',
  component: AlmacenPrincipalComponent,
  canActivate: [FarmaciaService],
  data: { role: ['almacenprincipal']}
  },
  {
  path: 'generarListaAlmacen',
  component: GenerarListaAlmacenComponent,
  canActivate: [FarmaciaService],
  data: { role: ['generarlistaalmacen']}
  },
  {
  path: 'listasGeneradas',
  component: ListasCreadasComponent,
  canActivate: [FarmaciaService],
  data: { role: ['listasgeneradas']}
  },
  {
  path: 'almacenSecundario',
  component: AlmacenSecundarioComponent,
  canActivate: [FarmaciaService],
  data: { role: ['almacensecundario']}
  },
  {
  path: 'generarListaAlmacenSalida',
  component: GenerarListaAlmacenSalidaComponent,
  canActivate: [FarmaciaService],
  data: { role: ['generarlistaalmacensalida']}
  },
  {
  path: 'listaSalidasAlmacenSecundario',
  component: SalidasAlmacenPrincipalComponent,
  canActivate: [FarmaciaService],
  data: { role: ['listasalidasalmacensecundario']}
  },
  {
  path: 'tipoCirugiaEvento',
  component: TipoCirugiaComponent,
  canActivate: [FarmaciaService],
  data: { role: ['tipocirugiaevento']}
  },
  {
  path: 'almacenes-secundarios',
  component: AlmacenesSecundariosComponent,
  canActivate: [FarmaciaService],
  data: { role: ['almacenessecundarios']}
  },
  {
  path: 'roles',
  component: RoleComponent,
  canActivate: [FarmaciaService],
  data: { role: ['roles']}
  }*/
    ]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '**',
    redirectTo: 'login'
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes,{
       useHash: false
    })
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
